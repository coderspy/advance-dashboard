<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerSla extends Model
{
    protected $table = "customer_slas";


    
    public function customer()
    {
        return $this->belongsTo('App\SmStaff', 'customer_id', 'id');
    }
    
    public function quotationProducts()
    {
        return $this->hasMany('App\SlaServices', 'sla_id', 'id');
    }
    public function Created_by()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

  public function getServiceName(){
        return $this->belongsTo('App\Service', 'id', 'service_id');
    }
}
