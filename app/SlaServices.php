<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ServiceController;
class SlaServices extends Model
{
    protected $table="sla_services";

      public function getService(){
        return $this->belongsTo('App\Service',  'service_id','id');
      }
      public static function getDiscount($id,$discount_id){        
        $data =\DB::table('discount_assigns')
        ->where('service_id',$id)
        ->where('discount_id',$discount_id)
        ->join('discounts','discounts.id','=','discount_assigns.discount_id')
        ->where('discounts.discount_start_date','<=',date('Y-m-d'))
        ->where('discounts.discount_end_date','>=',date('Y-m-d'))
        ->select('discounts.*')
        ->first();
        return $data;
    }

    public static function getPaper($edit_id,$id){
      $data = DB::table('services')       
        ->join('service_assign_papers', 'service_assign_papers.service_id', '=', 'services.id')
        ->join('papers', 'papers.id', 'service_assign_papers.service_paper_id' ) 
        ->join('archive_documents', 'papers.id', 'archive_documents.paper_id' ) 
        ->where('services.id',$id)
        ->where('archive_documents.sla_id',$edit_id)
        ->select('services.id as rowId',  'papers.title', 'papers.id as paperId','archive_documents.id as arch_id') 
        ->first(); 
        return $data;
    }
    public static function getPrice($id){
      $data = DB::table('services') 
      ->join('service_assign_prices', 'service_assign_prices.service_id', '=', 'services.id')
      ->join('service_prices', 'service_prices.id', 'service_assign_prices.service_price_id' )
      ->where('services.id',$id)
      ->select('services.id as rowId',  'service_prices.price',  'service_prices.title','service_prices.id')
      ->first();
        return $data;
    }

    // let me check 
}
