<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SmQuotation extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\SmStaff', 'customer_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\SmSupplier', 'vendor_id', 'id');
    }

    public function quotationProducts()
    {
        return $this->hasMany('App\SmQuotationProducts', 'quotation_id', 'id');
    }

    public function Created_by()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }


    public static function productDetail($product_id, $quotation_id)
    {

        $r = DB::table('sm_quotation_services')->where('quotation_id', $quotation_id)->first(); 

        return $r;
    }

    public static function productQuantity($product_id)
    {

        $total_quantity =  SmItemReceive::where('product_id', $product_id)->sum('total_quantity');


        return $total_quantity;
    }

    public static function bid_amount($total, $discount_amount, $type){

    	if($type != ""){
    		if($type == "P"){

    			$percentage = $total / 100 * $discount_amount;
                $total = $total - $percentage;

    		}elseif($type == "A"){

    			$total = $total - $discount_amount;
    		}
    	}

    	return $total;
    }

}
