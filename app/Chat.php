<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
    ];

    public function user(){

        return $this->belongsTo(User::class);
        
    }

    protected $with = ['sender', 'receiver'];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id')->select(['id', 'username']);
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id')->select(['id', 'username']);
    }
}