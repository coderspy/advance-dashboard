<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SmNotification extends Model
{
  public static function notifications()
  {
    $user = Auth()->user();
     return SmNotification::where('user_id', $user->id)->where('role_id', Auth()->user()->role_id)->where('is_read', 0)->orderBy('created_at','desc')->get();
   // return SmNotification::where('is_read', 0)->get();
  }
}
