<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveDocuments extends Model
{
    protected $table = "archive_documents";
    

    
    public function Customer(){
        return $this->belongsTo('App\SmStaff', 'customer_id', 'id');
    }

 
    public function Service(){
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }


    public function PaperName(){
        return $this->belongsTo('App\Papers', 'paper_id', 'id');
    }
     public function getCategoryName(){
        return $this->belongsTo('App\ServiceCategory', 'category_id', 'id');
    }
}
