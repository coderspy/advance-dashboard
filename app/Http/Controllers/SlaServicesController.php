<?php

namespace App\Http\Controllers;

use App\SlaServices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlaServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SlaServices  $slaServices
     * @return \Illuminate\Http\Response
     */
    public function show(SlaServices $slaServices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SlaServices  $slaServices
     * @return \Illuminate\Http\Response
     */
    public function edit(SlaServices $slaServices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SlaServices  $slaServices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlaServices $slaServices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SlaServices  $slaServices
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlaServices $slaServices)
    {
        //
    }
}
