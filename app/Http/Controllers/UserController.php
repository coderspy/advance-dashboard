<?php

namespace App\Http\Controllers;

use App\SmUserLog;
use App\YearCheck;
use App\ApiBaseMethod;
use Soumen\Agent\Agent;
use Illuminate\Http\Request;
use PDF;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('PM');
    }
    
    public function index(){
    	return view('backEnd.systemSettings.user.user');
    }
    public function create(){
    	return view('backEnd.systemSettings.user.user_create');
    }


    public function search_user_log(Request $request){
                $request->validate([
                    'from_date' => 'required', 
                    'to_date' => 'required|after:from_date', 
                ]);
                $user_logs = SmUserLog::whereBetween('created_at', [date('Y-m-d', strtotime($request->from_date)), date('Y-m-d', strtotime($request->to_date))])
                                      ->get();          
                
                 $user_logs=SmUserLog::whereBetween('created_at', [date('Y-m-d', strtotime($request->from_date)), date('Y-m-d', strtotime($request->to_date))])
                                 
                                     ->get();
           if (ApiBaseMethod::checkUrl($request->fullUrl())) {

            return ApiBaseMethod::sendResponse($user_logs, null);
        }                           

       return view('backEnd.reports.user_log', compact('user_logs'));

        }







    public function userLog(Request $request){
    	$user_logs = SmUserLog::where('created_at', 'LIKE', '%' . YearCheck::getYear() . '%')->get();
        
        if (ApiBaseMethod::checkUrl($request->fullUrl())) {

            return ApiBaseMethod::sendResponse($user_logs, null);
        }
    	return view('backEnd.reports.user_log', compact('user_logs'));
    }


    public function user_log_pdf(){

     $current_day = date('d');
     $todays_log =SmUserLog::where('created_at', 'LIKE', '%' . YearCheck::getYear() . '%')->get();
      if (count($todays_log) != 0) {
            $todays_log;

      }
            $pdf = PDF::loadView('backEnd.all_pages.user_log_pdf',
                [
                    'todays_log' => $todays_log,
                    'current_day' => $current_day
                 
                ]
            )->setPaper('A4', 'landscape');
            return $pdf->stream('user_log.pdf');

    }
}
