<?php

namespace App\Http\Controllers;

use App\SlaTemplate;
use Illuminate\Http\Request;

class SlaTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SlaTemplate  $slaTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(SlaTemplate $slaTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SlaTemplate  $slaTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(SlaTemplate $slaTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SlaTemplate  $slaTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlaTemplate $slaTemplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SlaTemplate  $slaTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlaTemplate $slaTemplate)
    {
        //
    }
}
