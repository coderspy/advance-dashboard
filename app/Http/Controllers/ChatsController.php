<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use App\Events\ChatEvent;
use App\Events\MessageSent; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	return view('backEnd.communicate.chat');
    }

    function user(Request $res){
        return User::find($res->id);
    }

    public function allmessegae()
    {
        return Chat::orWhere('sender_id','like', '%' . Auth::id() . '%')->orWhere('receiver_id','like', '%' . Auth::id() . '%')->with('user')->get();
        return Chat::with('user')->get();
    }

    public function sendMess(Request $request)
    {
        $chat = new Chat();
        $chat->sender_id = Auth::id();
        $chat->receiver_id = $request->reciver_id;
        $chat->message = $request->message;
        $chat->save();
    	broadcast(new ChatEvent($chat))->toOthers();
        return response()->json($chat,200);
    }

}
