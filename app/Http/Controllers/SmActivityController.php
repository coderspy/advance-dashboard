<?php

namespace App\Http\Controllers;

use App\SmActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SmActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SmActivity  $smActivity
     * @return \Illuminate\Http\Response
     */
    public function show(SmActivity $smActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SmActivity  $smActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(SmActivity $smActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SmActivity  $smActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmActivity $smActivity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmActivity  $smActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmActivity $smActivity)
    {
        //
    }
}
