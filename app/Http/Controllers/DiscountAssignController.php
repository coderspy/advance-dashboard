<?php

namespace App\Http\Controllers;

use App\DiscountAssign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use App\Discount;
use App\SmNotification;
use App\ServiceCategory;
use App\Service;
use App\SmStaff;
use App\Role;
use Auth;
use DB;
class DiscountAssignController extends Controller
{



        public function assign_discount()
    {
        $assign_discount_list = DiscountAssign::get();
        $discount_list = Discount::get();
        $service_list = Service::all();
        $customer_list = SmStaff::where('active_status', 1)->where('role_id','4')->get();
        $staffs = SmStaff::where('active_status', 1)->where('role_id','3')->get();
        return view('backEnd.all_pages.assign_discount',compact('assign_discount_list','discount_list','service_list','customer_list','staffs'));
    }
        public function assign_discount_list($id)
    {
        $assign_discount_list = DiscountAssign::where('id',$id)->get();
        return view('backEnd.all_pages.discount_notification',compact('assign_discount_list'));
    }


    public function store_assign_discount(Request $request)
    {
           $request->validate([ 
            'discount' => 'required', 
            'service' => 'required', 
          
        ]); 

        DB::beginTransaction();
        $s = new DiscountAssign();
        $s->discount_id = $request->discount;
        $s->service_id = $request->service;  
        $result = $s->save(); 

        $data = User::where(['role_id'=>3,'access_status'=>1,'active_status'=>1])->get();
        foreach ($data as $key => $value) {            
            $store_notification = new SmNotification();        
            $store_notification->message        = 'Create a discount';
            $store_notification->date           = date('Y-m-d',strtotime($request->date));
            $store_notification->user_id        = $value->id;
            $store_notification->role_id        = 3;
            $store_notification->url            = route('assign_discount_list',$s->id);
            $store_notification->active_status  = 1;
            $store_notification->created_at     = date('Y-m-d h:i:s');
            $store_notification->updated_at     = date('Y-m-d h:i:s');
            $store_notification->created_by     = Auth::user()->id;
            $store_notification->updated_by     = Auth::user()->id;
            $store_notification->save();
        }
        DB::commit();
        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('assign-discount');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


    public function edit_assign_discount($id)
    {
       
        $editData= DiscountAssign::find($id);
        $data = DiscountAssign::get();
        $assign_discount_list = DiscountAssign::get();
        $discount_list = Discount::get();
        $service_list = Service::all();
       $customer_list = SmStaff::where('active_status', 1)->where('role_id','4')->get();
       $staffs = SmStaff::where('active_status', 1)->where('role_id','3')->get();
        return view('backEnd.all_pages.assign_discount', compact('data','editData','discount_list','service_list','customer_list','assign_discount_list','staffs'));
    }

    public function assign_discount_update(Request $request)
    {
         
        $request->validate([ 
            'discount' => 'required', 
            'service' => 'required', 
        ]); 

        $s = DiscountAssign::find($request->id);
       
        $s->discount_id = $request->discount;
        $s->service_id = $request->service;    
        $result = $s->save(); 

        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('assign-discount');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


      public function assign_discount_delete($id){ 
        try {
            $delete_query = DiscountAssign::destroy($id);  
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();  
        } catch (\Illuminate\Database\QueryException $e) { 

            $msg = 'This data already used in  another table. Please remove those data first';
            Toastr::error( $msg, 'Failed');
            return redirect()->back();
        

        } catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }
}
