<?php

namespace App\Http\Controllers;

use Validator;
use App\tableList;
use App\SmStaff;
use App\Role;
use App\User;
use App\ApiBaseMethod;
use App\SmHumanDepartment;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use PhpMyAdmin\MoTranslator\ReaderException;

class SmHumanDepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('PM');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departments = SmHumanDepartment::all(); 
        return view('backEnd.humanResource.human_resource_department', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function services_charges(Request $request)
    {
       
        $staffs = SmStaff::where('active_status', 1)->get();
        $roles = Role::where('active_status', '=', '1')->get();  
         return view('backEnd.all_pages.services_charges',compact('staffs', 'roles'));
    }

      public function user_archive_document(Request $request)
    {
       
        $staffs = SmStaff::where('active_status', 1)->get();
        $roles = Role::where('active_status', '=', '1')->get();  
         return view('backEnd.all_pages.user_archaive_document',compact('staffs', 'roles'));
    }
     public function user_discount_approve(Request $request)
    {
       
        $staffs = SmStaff::where('active_status', 1)->get();
        $roles = Role::where('active_status', '=', '1')->get();  
         return view('backEnd.all_pages.user_discount_approve',compact('staffs', 'roles'));
    }
   


    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => "required|unique:sm_human_departments,name"
        ]);

        if ($validator->fails()) {
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendError('Validation Error.', $validator->errors());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $department = new SmHumanDepartment();
        $department->name = $request->name;
        $result = $department->save();

        if (ApiBaseMethod::checkUrl($request->fullUrl())) {
            if ($result) {
                return ApiBaseMethod::sendResponse(null, 'Department has been created successfully');
            } else {
                return ApiBaseMethod::sendError('Something went wrong, please try again.');
            }
        } else {
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $department = SmHumanDepartment::find($id);
        $departments = SmHumanDepartment::all();

        if (ApiBaseMethod::checkUrl($request->fullUrl())) {
            $data = [];
            $data['department'] = $department->toArray();
            $data['departments'] = $departments->toArray();
            return ApiBaseMethod::sendResponse($data, null);
        }
        return view('backEnd.humanResource.human_resource_department', compact('department', 'departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => "required"
        ]);

        if ($validator->fails()) {
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendError('Validation Error.', $validator->errors());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $department = SmHumanDepartment::find($request->id);
        $department->name = $request->name;
        $result = $department->save();

        if (ApiBaseMethod::checkUrl($request->fullUrl())) {
            if ($result) {
                return ApiBaseMethod::sendResponse(null, 'Department has been updated successfully');
            } else {
                return ApiBaseMethod::sendError('Something went wrong, please try again.');
            }
        } else {
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('department');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {




        $id_key = 'department_id';

        $tables = tableList::getTableList($id_key);

        try {
            $delete_query = SmHumanDepartment::destroy($id);
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($delete_query) {
                    return ApiBaseMethod::sendResponse(null, 'Department has been deleted successfully');
                } else {
                    return ApiBaseMethod::sendError('Something went wrong, please try again.');
                }
            } else {
                if ($delete_query) {
                    Toastr::success('Operation successful', 'Success');
                    return redirect()->back();
                } else {
                    return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $msg = 'This data already used in  : ' . $tables . ' Please remove those data first';

            return redirect()->back()->with('message-danger-delete', $msg);
        } catch (\Exception $e) {
            //dd($e->getMessage(), $e->errorInfo);
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }

    }
}
