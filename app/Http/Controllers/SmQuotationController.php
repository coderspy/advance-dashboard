<?php

namespace App\Http\Controllers;
use PDF;
use App\User;
use App\SmItem;
use App\Service;
use App\SmStaff;
use App\SmSupplier;
use App\SmQuotation;
use App\SmNotification;
use App\ArchiveDocuments;
use App\SmGeneralSettings;
use App\SmQuotationProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SmQuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->role_id==1){  
        $quotations = SmQuotation::all();
        $customers = SmStaff::where('role_id',4)->get(); 
       }
     else{
        $quotations=SmQuotation::where('created_by',Auth::user()->id)->get();
        $customers = SmStaff::where('role_id',4)->get(); 
     } 
        return view('backEnd/quotations/quotations_list', compact('quotations','customers'));
    }
    
    public function created_Quotation_list($id)
    {
        $quotations=SmQuotation::where('id',$id)->get();
        $customers = SmStaff::where('role_id',4)->get(); 
        return view('backEnd.quotations.quotations_list', compact('quotations','customers'));
    }
    
    public function getServiceList(){        
        $services = Service::all();
		$searchData = [];
		foreach($services as $service){
			$searchData[] =  ['id' => $service->id, 'name' => $service->name, 'unit_price' => $service->unit_price]; 
		}

		if(!empty($searchData)){
			return json_encode($searchData);
        } 
    }
    public function getServiceListDetails(Request $request){      
        
        $searchData = Service::where('id', $request->id)->first(); 
        $id=$request->id;
        $data[0]= $serivice_with_papers = DB::table('services')
        ->join('service_assign_papers', 'service_assign_papers.service_id', '=', 'services.id')
        ->join('papers', 'papers.id', 'service_assign_papers.service_paper_id' ) 
        ->where('services.id',$id)
        ->select('services.id as rowId',  'papers.title', 'papers.id as paperId') 
        ->get(); 

        $data[1]= $serivice_with_price = DB::table('services') 
        ->join('service_assign_prices', 'service_assign_prices.service_id', '=', 'services.id')
        ->join('service_prices', 'service_prices.id', 'service_assign_prices.service_price_id' )
        ->where('services.id',$id)
        ->select('services.id as rowId',  'service_prices.price',  'service_prices.title','service_prices.id')
        ->get();

        $data[2]=$service_discount_lists = DB::table('discount_assigns')
        ->where('service_id',$request->id)
        ->join('discounts','discounts.id','=','discount_assigns.discount_id')
        ->where('discounts.discount_start_date','<=',date('Y-m-d'))
        ->where('discounts.discount_end_date','>=',date('Y-m-d'))
        ->select('discounts.*')
        ->get();
        // $data[2]=$service_discount_lists->toArray();


        return json_encode([$data]);

		if(!empty($searchData)){
			return json_encode([$searchData]);
        } 
    }


    
    public function create()
    {
        $customers  = SmStaff::where('role_id', 4)->get();
        $vendors    = SmSupplier::all();
        $items      = Service::all();
        $quotations = SmQuotation::all(); 
        $departments = SmStaff::all();
        $services    =Service::all();
       
        return view('backEnd/quotations/manage_quotations', compact('quotations','customers', 'vendors', 'items', 'departments','services'));
    }
    
 
    public function store(Request $request)
    {

       //  return $request;

        $request->validate([
            'days'         => 'required',
            'number'        => 'required',
            'date'          => 'required',  
        ]);
 
  
      
        // return $s;
        DB::beginTransaction();

       try {
             if($request->customer_type=="new"){
            $user               = new User();
            $user->access_status=  1;
            $user->created_by   =  1;
            $user->updated_by   =  1; 
            $user->role_id      =  4; 
            $user->full_name    =  $request->first_name.' '. $request->last_name;
            $user->email        = $request->email;
            $user->username     =  $request->email;
            $user->password     =  Hash::make('123456');
            $user->created_at   =  date('Y-m-d h:i:s');
            $user->save(); 

            $last_staff= SmStaff::where('role_id',4)->orderBy('id', 'DESC')->first();

            $s = new SmStaff();
            $s->user_id = $user->id;
            $s->role_id = 4;
            $s->staff_no =$last_staff->staff_no + 1 ;
            $s->designation_id = 1;
            $s->department_id = 1;
            $s->first_name = $request->first_name;
            $s->last_name = $request->last_name;
            $s->full_name = $request->first_name.' '. $request->last_name; 
            $s->email = $request->email;
            $s->gender_id = 1;
            $s->mobile = $request->mobile;
            $s->passport_number = $request->passport_number;
            $s->nid_number = $request->nid_number;
            $s->staff_photo = 'public/uploads/staff/1.png';
            $s->save();


        } else{
            $s=SmStaff::where('id',$request->customer_id)->first();
        }

            $quotation = new SmQuotation();
            
            $quotation->title               = $request->title;
            $quotation->number              = $request->number;
            $quotation->date                = date('Y-m-d', strtotime($request->date));
            $quotation->reference           = $request->reference;
            //customer_details
            
        if($request->customer_type=="new"){
            $quotation->customer_id         =  $s->id; 
            $quotation->customer_name       =  $request->first_name.' '. $request->last_name;
        }else{
            $quotation->customer_id         = $request->customer_id;
            $customer_details               = SmStaff::find($request->customer_id);
            // $quotation->customer_name       = $s->full_name;

        }
             //amount

             $quotation->days        = $request->days;
             $quotation->total_amount        = $request->Etotal;
             $quotation->paybale_amount        = $request->Ebid_amount;
             $quotation->paid_amount        = $request->paid_amount;
             if ($request->Ebid_amount==$request->paid_amount) {
                $quotation->payment_status        = 'P';
             }
             if ($request->Ebid_amount>$request->paid_amount && $request->paid_amount>0) {
                $quotation->payment_status        = 'PP';
             }
             if ($request->Ebid_amount>$request->paid_amount && $request->paid_amount==0) {
                $quotation->payment_status        = 'UP';
             }
            
            //footer note 
            $quotation->private_note        = $request->private_note;
            $quotation->public_note         = $request->public_note;
            $quotation->terms_note          = $request->terms_note;
            $quotation->footer_note         = $request->footer_note;
            $quotation->signature_person    = $request->signature_person;
            $quotation->signature_company   = $request->signature_company;

         //    return $quotation;
            
            $quotation->created_by    = Auth::user()->id;
            // return $quotation;
            $quotation->save();
            $quotation->toArray();

            $data               = SmQuotation::find($quotation->id);
            $data['note']       = '"' . $data->title . '" has been added.';
            $data['model_name'] = 'SmQuotation';
            $data['old_data']   = $data->toJson();
            $data['new_data']   = '';
            $data['action']     = 'Insert';
            $data['action_id']  = $data->id;
            $result             = SmGeneralSettings::StoreAllActivities($data);

            $i = 0; 
            if($result){
                foreach ($request->Eproducts as $product) { 
                    $quotation_product                      = new SmQuotationProducts();
                    $quotation_product->quotation_id        = $quotation->id;
                    $quotation_product->discount_id        = @$request->discount;
                    $quotation_product->service_id          = $product; 
                    $quotation_product->govt_price                 = $request->Eproducts[$i];
                    // $quotation_product->sale_unit_price          = $request->Eunit_price[$i];
                    $quotation_product->save();

                    $data               = SmQuotationProducts::find($quotation_product->id);
                    $data['note']       = '"quotation No' . $request->quotation_no . ' & Product Id ' . $data->product_id . '" has been added.';
                    $data['model_name'] = 'SmQuotationProducts';
                    $data['old_data']   = $data->toJson();
                    $data['new_data']   = '';
                    $data['action']     = 'Insert';
                    $data['action_id']  = $data->id;
                    $result             = SmGeneralSettings::StoreAllActivities($data);
                    $i++;
                }
            
            }
            
        $files = $request->file('file'); 
        if($request->hasFile('file')){   
            $paper_counter=0;
            foreach ($files as $file) {
                $fileName = md5($file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/sla/', $fileName);
                $fileName = 'public/uploads/sla/' . $fileName;

                $archive_document                  = new ArchiveDocuments();
                $archive_document->file            =  $fileName;
                $archive_document->customer_id     = $s->id;
                $archive_document->quotation_id     = $quotation->id;
                $archive_document->paper_id        = $request->file_name[$paper_counter++];
                $archive_document->save();
            }
        } 

            $store_notification = new SmNotification();        
            $store_notification->message        = 'Create a Quotation';
            $store_notification->date           = date('Y-m-d',strtotime($request->date));
            $store_notification->user_id        = 1;
            $store_notification->role_id        = 1;
            $store_notification->url            = route('created_Quotation_list',$quotation->id);
            $store_notification->active_status  = 1;
            $store_notification->created_at     = date('Y-m-d h:i:s');
            $store_notification->updated_at     = date('Y-m-d h:i:s');
            $store_notification->created_by     = Auth::user()->id;
            $store_notification->updated_by     = Auth::user()->id;
            $store_notification->save();
           

            DB::commit();
            return redirect('quotations')->with('message-success', 'quotation has been created successfully');
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-danger', 'Ops! Sorry. Operation failed. ' . $e);
        }
    }
    //end store method 


    public function show(SmQuotation $smQuotation, $id)
    {
        $quotation = SmQuotation::find($id);
        // return $quotation;
        $archive_document=ArchiveDocuments::where('quotation_id',$id)->get();

        return view('backEnd/quotations/quotationView', compact('quotation','archive_document'));
    }


    
    public function edit(SmQuotation $smQuotation, $id)
    {
        
        // $customers   = SmStaff::where('role_id', 2)->get();
        // $vendors     = SmSupplier::all();
        // $items       = SmItem::all();
        // $quotations  = SmQuotation::all();
        // $departments = SmStaff::all();

        $customers  = SmStaff::where('role_id', 4)->get();
        $vendors    = SmSupplier::all();
        $items      = Service::all();
        $quotations = SmQuotation::all(); 
        $edit        = SmQuotation::find($id);
        $customer_info        = SmStaff::find($edit->customer_id);
        $departments = SmStaff::all();
        $services    =Service::all();
        $taken_services=[];
        $service_id=[];
        $get_services_id=DB::table('sm_quotation_services')->where('quotation_id',$id)->get();
        foreach ($get_services_id as $key => $value) {
            // $service_id[]=$value->service_id;
            array_push($service_id,$value->service_id);
            $searchData = Service::where('id', $value->service_id)->first(); 
                $id=$value->service_id;
                $data[0]= $serivice_with_papers = DB::table('services')
                ->join('service_assign_papers', 'service_assign_papers.service_id', '=', 'services.id')
                ->join('papers', 'papers.id', 'service_assign_papers.service_paper_id' ) 
                ->where('services.id',$id)
                ->select('services.id as rowId',  'papers.title', 'papers.id as paperId') 
                ->get(); 

                $data[1]= $serivice_with_price = DB::table('services') 
                ->join('service_assign_prices', 'service_assign_prices.service_id', '=', 'services.id')
                ->join('service_prices', 'service_prices.id', 'service_assign_prices.service_price_id' )
                ->where('services.id',$id)
                ->select('services.id as rowId',  'service_prices.price',  'service_prices.title','service_prices.id')
                ->get();

                $data[2]=$service_discount_lists = DB::table('discount_assigns')
                ->where('service_id',$value->service_id)
                ->join('discounts','discounts.id','=','discount_assigns.discount_id')
                ->where('discounts.discount_start_date','<=',date('Y-m-d'))
                ->where('discounts.discount_end_date','>=',date('Y-m-d'))
                ->select('discounts.*')
                ->get();
                $data[3]=$value->service_id;

                $taken_services[][$value->service_id]=$data;
                // return json_encode([$data]);
        }
        // dd($taken_services);

        return view('backEnd.quotations.edit_quotations', compact('quotations','service_id', 'edit', 'customers', 'vendors', 'items', 'departments','services','customer_info','taken_services'));
    }

    
    public function Qupdate(Request $request)
    {
        $input = $request->all();
       // dd($input);
            $validator = Validator::make($input, [
                'number'       => 'required',
                'date'   => 'required',
                // 'vendors'        => 'required|not_in:0',
                'customer'        => 'required|not_in:0',
            ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        DB::beginTransaction();
        try {
            $quotation = SmQuotation::find($request->id);
            $quotation->number              = $request->number;
            $quotation->date                = date('Y-m-d', strtotime($request->date));
            $quotation->reference           = $request->reference;
            //customer_details
            $quotation->customer_id         = $request->customer;
            $customer_details               = SmStaff::find($request->customer);
            $quotation->customer_name       = $customer_details->full_name;
            //vendor_details
            // $quotation->vendor_id           = @$request->vendors;
            $vendor_details                 = SmSupplier::find($request->vendors);
            // $quotation->vendor_name         = @$vendor_details->company_name;
            //footer note 
            $quotation->private_note        = @$request->private_note;
            $quotation->public_note         = @$request->public_note;
            $quotation->terms_note          = @$request->terms_note;
            $quotation->footer_note         = @$request->footer_note;
            $quotation->signature_person    = @$request->signature_person;
            $quotation->signature_company   = @$request->signature_company;
            $quotation->total_amount        = $request->Etotal;
            $quotation->paybale_amount        = $request->Ebid_amount;
            $quotation->paid_amount        = $request->paid_amount;
            if ($request->Ebid_amount==$request->paid_amount) {
                $quotation->payment_status        = 'P';
             }
             if ($request->Ebid_amount>$request->paid_amount && $request->paid_amount>0) {
                $quotation->payment_status        = 'PP';
             }
             if ($request->Ebid_amount>$request->paid_amount && $request->paid_amount==0) {
                $quotation->payment_status        = 'UP';
             }
            $quotation->updated_by    = Auth::user()->id;
            $quotation->save();
            $quotation->toArray();

            $data               = SmQuotation::find($quotation->id);
            $data['note']       = '"' . $data->title . '" has been added.';
            $data['model_name'] = 'SmQuotation';
            $data['old_data']   = $data->toJson();
            $data['new_data']   = '';
            $data['action']     = 'Insert';
            $data['action_id']  = $data->id;
            $result             = SmGeneralSettings::StoreAllActivities($data);

            SmQuotationProducts::where('quotation_id', $quotation->id)->delete();

            $i = 0;

                foreach ($request->Eproducts as $product) {
                    $quotation_product                  = new SmQuotationProducts();
                    $quotation_product->quotation_id    = $quotation->id;
                    // $quotation_product->product_id      = $product;
                    $quotation_product->discount_id        = @$request->discount[$i];
                    // $quotation_product->qnt             = $request->quantity[$i];
                    // $quotation_product->unit_price      = $request->unit_price[$i];
                    $result                             = $quotation_product->save();
                    $data               = SmQuotationProducts::find($quotation_product->id);
                    $data['note']       = '"quotation No' . $request->quotation_no . ' & Product Id ' . $data->product_id . '" has been added.';
                    $data['model_name'] = 'SmQuotationProducts';
                    $data['old_data']   = $data->toJson();
                    $data['new_data']   = '';
                    $data['action']     = 'Insert';
                    $data['action_id']  = $data->id;
                    $result             = SmGeneralSettings::StoreAllActivities($data);

                    $i++;
                }

            DB::commit();
            return redirect('quotations')->with('message-success', 'quotation has been updated successfully');
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-danger', 'Ops! Sorry. Operation failed. ' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmQuotation  $smQuotation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, SmQuotation $smQuotation)
    {
        $result = SmQuotation::destroy($request->id);

        if ($result) {
            return redirect('quotations')->with('message-success', 'quotation has been created successfully');
        } else {
            return redirect('quotations')->with('message-danger', 'Ops! Sorry. Operation failed');
        }
    }

    public function delete($id)
    {
        try{
            $result = SmQuotation::destroy($id);

            if ($result) {
                return redirect('quotations')->with('message-success', 'quotation has been deleted successfully');
            } else {
                return redirect('quotations')->with('message-danger', 'Ops! Sorry. Operation failed');
            }
        } catch (\Illuminate\Database\QueryException $e) { 

            $msg = 'This data already used in  another table. Please remove those data first';
            Toastr::error( $msg, 'Failed');
            return redirect()->back();   

        } catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }
    public function changeStatus(Request $request,$id){
        try{
            $quotation=SmQuotation::where('id',$id)->first();
            if ($quotation->is_approved == 0) {
                $quotation->is_approved = 1;

                if ($quotation->Created_by->role_id == 3) {
                    $store_notification = new SmNotification();
                    $store_notification->message        = 'Quotation Approved';
                    $store_notification->date           = date('Y-m-d');
                    $store_notification->user_id        = $quotation->created_by;
                    $store_notification->role_id        = 3;
                    $store_notification->url            = route('created_Quotation_list',$quotation->id);
                    $store_notification->active_status  = 1;
                    $store_notification->created_at     = date('Y-m-d h:i:s');
                    $store_notification->updated_at     = date('Y-m-d h:i:s');
                    $store_notification->created_by     = Auth::user()->id;
                    $store_notification->updated_by     = Auth::user()->id;
                    $store_notification->save();
                }

            } else {
                $quotation->is_approved = 0;
            }
            $quotation->save();

          
            return redirect('quotations')->with('message-success', 'Quotation has been updated successfully');
        }catch(\Exception $e){
            return redirect('quotations')->with('message-danger', 'Ops! Sorry. Operation failed');
        }
            

    }
}
