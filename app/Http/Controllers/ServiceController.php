<?php

namespace App\Http\Controllers;

use PDF;
use App\Role;
use App\User;
use Validator;
use App\Papers;
use App\Service;
use App\SmStaff;
use App\Discount;
use App\CustomerSla;
use App\ServicePrice;
use App\ServiceCategory;
use App\ArchiveDocuments;
use App\ServiceAssignPrice;
use App\ServiceAssignPapers;
use App\SmNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
class ServiceController extends Controller
{

    /******************************* Price List  **********************/
    public function PriceList(){
        $prices = ServicePrice::all(); 
        return view('backEnd.services.price_list', compact('prices'));
    }


    // start Price List Store
      public function PriceListStore(Request $request){
        $request->validate([
            'title' => 'required | max:200',             
            'price' => 'required',             
        ]); 

        try{
            $s = new ServicePrice(); 
            $s->title = $request->title; 
            $s->price = $request->price; 
            $s->created_by = Auth::user()->id;
            $s->updated_by = Auth::user()->id;
            $result = $s->save(); 

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('services/prices-list');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            } 
        }catch (\Exception $e) { 
            return $e->getMessage();
        } 
    }

        public function PriceListEdit($id){
        
        $editData= ServicePrice::find($id);
        $prices = ServicePrice::all();
        return view('backEnd.services.price_list', compact('prices','editData'));
    }



    public function PriceListUpdate(Request $request){
        
        $request->validate([
            'title' => 'required | max:200', 
            'price' => 'required',
        ]); 
        try{
            $s = ServicePrice::find($request->id);
            $s->title = $request->title;
            $s->price = $request->price;
            $s->updated_by = Auth::user()->id;
            $result = $s->save(); 
            
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('services/prices-list');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) { 
            return $e->getMessage();
        } 

    }

    public function price_delete($id){
            try {
            $delete_query = ServicePrice::destroy($id);  
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();  

        } catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }

    }

/////////////////////////PAPER//////////////////////////////////////////////////////////////////

    public function PaperList(){
    	$category_list = Service::all();
    	$data = Papers::all();
    	return view('backEnd.services.paper_list', compact('data','category_list'));
    }
    public function ajaxSelectService(Request $request){
        $services = Service::where('category_id',$request->id)->get();
        return response()->json([$services]);
    }




    public function ajaxSelectPapers(Request $request){
    
        $r =DB::table('archive_documents')
        ->select('papers.title', 'archive_documents.file')
        ->join('papers', 'papers.id','=', 'archive_documents.paper_id')
        ->where('customer_id',$request->id)->get(); 
        return response()->json([$r]);
    }

    public function PaperListEdit($id){
        $category_list = Service::all();
        $editData= Papers::find($id);
        $data = Papers::all();
        return view('backEnd.services.paper_list', compact('data','category_list','editData'));
    }


    // start Paper List Store
      public function PaperListStore(Request $request){
        $request->validate([
            'title' => 'required | max:200',             
        ]); 

        try{
            $s = new Papers(); 
            $s->title = $request->title; 
            $s->created_by = Auth::user()->id;
            $s->updated_by = Auth::user()->id;
            $result = $s->save(); 

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('services/papers-list');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            } 
        }catch (\Exception $e) { 
            return $e->getMessage();
        } 
    }
    // end Paper List Store


    public function PaperListUpdate(Request $request){
        
        $request->validate([
            'title' => 'required | max:200' 
        ]); 
        try{
            $s = Papers::find($request->id);
            $s->title = $request->title;
            $s->updated_by = Auth::user()->id;
            $result = $s->save(); 
            
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('services/papers-list');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) { 
            return $e->getMessage();
        } 


    }

    public function papers_delete($id){
            try {
            $delete_query = Papers::destroy($id);  
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();  

        } catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }

    }



    /******************************* END Paper List  **********************/

    public function categoryView(){
    	$category_list = ServiceCategory::all();
    	$data = ServiceCategory::all();
    	return view('backEnd.services.category', compact('data','category_list'));
    }

    public function serviceView(){
    	$category_list = ServiceCategory::all();
    	$data = Service::all();
        $papers = Papers::all();
        $prices = ServicePrice::all();
    	return view('backEnd.services.service', compact('data','category_list','papers','prices'));
    }
    public function ServiceStoreNotification($id){
    	$data = Service::where('id',$id)->get();
    	return view('backEnd.services.service_nnotiication', compact('data'));
    }

 
    public function categoryEdit($id){
    	$data = ServiceCategory::all();
    	$editData= ServiceCategory::find($id);
    	return view('backEnd.services.category', compact('data','editData'));
    }



     public function CategoryDelete($id){ 
        try {
            $delete_query = ServiceCategory::destroy($id);  
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();

        } catch (\Illuminate\Database\QueryException $e) { 

            $msg = 'This data already used in  another table. Please remove those data first';
            Toastr::error( $msg, 'Failed');
            return redirect()->back();
        }  
         catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }

     public function ServiceDelete($id){ 
        try {
            $delete_query = Service::destroy($id);    
            $ServiceAssignPrice = DB::table('service_assign_prices')->where('service_id', $id)->delete();
            $ServiceAssignPapers = DB::table('service_assign_papers')->where('service_id', $id)->delete();
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();  
        } catch (\Illuminate\Database\QueryException $e) { 

            $msg = 'This data already used in  another table. Please remove those data first';
            Toastr::error( $msg, 'Failed');
            return redirect()->back();
        

        } catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }

     public function serviceEdit($id){
        $category_list = ServiceCategory::all();
        $data = Service::all();
        $prices = ServicePrice::all();
        $papers = Papers::all();
        $editData= Service::find($id);
        $editData_price=DB::table('service_assign_prices')->where('service_id', $id)->get();
        
        $price_ids = array();
        foreach ($editData_price as $row) {
            $price_ids[] = $row->service_price_id;
        }


        $editData_paper=DB::table('service_assign_papers')->where('service_id', $id)->get();
        $paper_ids = array();
        foreach ($editData_paper as $row) {
            $paper_ids[] = $row->service_paper_id;
        }

       
        return view('backEnd.services.service', compact('data','editData','category_list','prices','papers','editData_price','editData_paper', 'price_ids','paper_ids'));
    }

   public function ServiceUpdate(Request $request){
    
        $request->validate([
            'name' => 'required|max:255',  
            'category_id' => 'required', 
        ]); 
       

        DB::beginTransaction();
        try{
            $s = Service::find($request->id);
            $s->category_id = $request->category_id;  
            $s->name = $request->name;
            $s->created_by = Auth::user()->id;
            $result= $s->save(); 
            try{ 
                if (isset($request->prices)) { 
                    $delete_existing_data = ServiceAssignPrice::where('service_id',$request->id)->delete(); 
                    foreach ($request->prices as $row) {
                        $sp = new ServiceAssignPrice();
                        $sp->service_id =  $s->id;
                        $sp->service_price_id = $row;
                        $result=  $sp->save(); 
                    }
                    DB::commit();
                }
            }catch(\Exception $e){ 
                DB::rollBack();
            }
            
            try{ 
                if (isset($request->papers)) {
                    $delete_existing_data =  ServiceAssignPapers::where('service_id',$request->id)->delete();            
                    foreach ($request->papers as $row) {  
                        $paper =new ServiceAssignPapers();
                        $paper->service_id =  $s->id;
                        $paper->service_paper_id =$row; 
                        $result = $paper->save();  
                    }
                    DB::commit();
                }
            }catch(\Exception $e){ 
                DB::rollBack();
            } 
            DB::commit();

            Toastr::success('Operation successful', 'Success');
            return redirect('service-list');

        }catch(\Exception $e){ 
            DB::rollBack();
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }



   public function ServiceStore(Request $request){
          

	   	$request->validate([
		    'name' => 'required|max:255',  
		    'category_id' => 'required', 
		]); 
        DB::beginTransaction();
        $s = new Service();   
        $s->category_id = $request->category_id;  
        $s->name = $request->name;
        $s->created_by = Auth::user()->id;
        $s->save(); 
        
       
          if (isset($request->prices)) { 
             foreach ($request->prices as $row) {
                $sp =new ServiceAssignPrice();
                $sp->service_id =  $s->id;
                $sp->service_price_id = $row;
                $sp->save(); 
            }
        }
      
        if (isset($request->papers)) {
            foreach ($request->papers as $row) {  
                $paper = new ServiceAssignPapers();
                $paper->service_id =  $s->id;
                $paper->service_paper_id =$row; 
                $paper->save();  
            }
        } 

        
        $data = User::where(['role_id'=>3,'access_status'=>1,'active_status'=>1])->get();
        foreach ($data as $key => $value) {            
            $store_notification = new SmNotification();       
            $store_notification->message        = 'New Services Created';
            $store_notification->date           =  date('Y-m-d',strtotime($request->date));
            $store_notification->user_id        =  $value->id;
            $store_notification->url            =  route('ServiceStoreNotification',$s->id);
            $store_notification->role_id        =  3;
            $store_notification->active_status  = 1;
            $store_notification->created_at     = date('Y-m-d h:i:s');
            $store_notification->updated_at     = date('Y-m-d h:i:s');
            $store_notification->created_by     = Auth::user()->id;
            $store_notification->updated_by     = Auth::user()->id;
            $result= $store_notification->save();

         }
         DB::commit();
        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('service-list');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }

   public function CategoryUpdate(Request $request){

	   	$request->validate([
		    'name' => 'required|max:255',  
		]); 

        $s = ServiceCategory::find($request->id);
        $s->name = $request->name; 
        $s->updated_by = Auth::user()->id;
        $result = $s->save(); 
        
        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('services-category');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }


   public function CategoryStore(Request $request){

	   	$request->validate([
		    'name' => 'required|max:255',  
		]); 

        $s = new ServiceCategory();
        $s->name = $request->name; 
        $s->created_by = Auth::user()->id;
        $result = $s->save(); 

        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('services-category');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }

    public function pro_user_list(){

 	
        $staffs = SmStaff::where('active_status', 1)->where('role_id','3')->get();
        $roles = Role::where('active_status', '=', '1')->where('id','3')->get();
       // dd($roles);
         return view('backEnd.all_pages.pro_user_list', compact('staffs', 'roles'));
    }

     public function services_list(){
    	$category_list = ServiceCategory::all();
    	$data = Service::all();
    	return view('backEnd.all_pages.services_list', compact('data','category_list'));
    }

       public function services_list_pdf(){

        $category_list = ServiceCategory::all();
        $data = Service::all();
       if (count($category_list) != 0) {
            $category_list;

         }
      if (count($data) != 0) {
            $data;

         }
            $pdf = PDF::loadView('backEnd.all_pages.service_list_pdf',
                [
                    'category_list' => $category_list,
                    'data' => $data
                 
                ]
            )->setPaper('A4', 'landscape');
            return $pdf->stream('service_list.pdf');

    }

        public function search_services_pending_done(Request $request){
            $request->validate([
                'from_date' => 'required', 
                'to_date' => 'required|after:from_date', 
            ]); 


             if(Auth::user()->role_id==1){
        
                $pro_daily_services = CustomerSla::where('active_status', 1)
                                      ->whereBetween('created_at', [date('Y-m-d', strtotime($request->from_date)), date('Y-m-d', strtotime($request->to_date))])
                                      ->get();
             }
             else{
                
                 $pro_daily_services=CustomerSla::where('active_status', 1)
                                       ->whereBetween('created_at', [date('Y-m-d', strtotime($request->from_date)), date('Y-m-d', strtotime($request->to_date))])
                                     ->where('created_by',Auth::user()->id)
                                     ->get();

             } 

            // dd($pro_daily_services);exit;

       return view('backEnd.all_pages.pro_daily_services', compact('pro_daily_services'));

        }


        public function services_pending_done(){
        if(Auth::user()->role_id==1){     
        $pro_daily_services = CustomerSla::where('active_status', 1)->get();
       }
        else{
         $pro_daily_services=CustomerSla::where('created_by',Auth::user()->id)->where('active_status', 1)->get();
        }
      
        return view('backEnd.all_pages.pro_daily_services', compact('pro_daily_services'));
         }




         public function services_pending_done_pdf(){
         if(Auth::user()->role_id==1){ 
         $pro_daily_services = CustomerSla::where('active_status', 1)->get();
         }
         else{
         $pro_daily_services=CustomerSla::where('created_by',Auth::user()->id)->where('active_status', 1)->get();
        }
       
        if (count($pro_daily_services) != 0) {
             $pro_daily_services;

          }
     
         $pdf = PDF::loadView('backEnd.all_pages.pro_daily_services_pdf',
                [
                    'pro_daily_services' => $pro_daily_services
                       
                ]
            )->setPaper('A4', 'landscape');
            return $pdf->stream('pro_daily_services.pdf');

         }

        public function user_list(){

    
        $staffs = SmStaff::where('active_status', 1)->get();
        $roles = Role::where('active_status', '=', '1')->get();
       // dd($roles);
         return view('backEnd.all_pages.user_list', compact('staffs', 'roles'));
    }
public function ajaxDiscountCheck(Request $request){
    // return date('Y-m-d');
    $r =DB::table('discount_assigns')
        ->where('service_id',$request->id)
        ->join('discounts','discounts.id','=','discount_assigns.discount_id')
        ->where('discounts.discount_start_date','<=',date('Y-m-d'))
        ->where('discounts.discount_end_date','>=',date('Y-m-d'))
        ->select('discounts.*')
        ->get();
        return response()->json([$r]);
}
public function ajaxDiscountCalculation(Request $request){
    $r =Discount::where('id',$request->id)->first(); 
    // return $r;
    $final_grant_total=0;
        if ($r->type=='P') {
        $final_grant_total=$request->grand_total / 100 * $r->amount;
        $final_grant_total=$request->grand_total -$final_grant_total;
        } else {
            $final_grant_total=$request->grand_total - $r->amount;
        }
    
    // return $final_grant_total;
        return response()->json($final_grant_total);
}

}
