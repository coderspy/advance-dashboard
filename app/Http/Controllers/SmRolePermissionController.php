<?php

namespace App\Http\Controllers;

use App\ApiBaseMethod;
use Illuminate\Http\Request;
use App\Role;
use App\SmModuleLink;
use App\SmModule;
use App\SmRolePermission;
use App\SmStaff;

class SmRolePermissionController extends Controller
{
    public function __construct(){
        $this->middleware('PM');
    }
    
    public function assignPermission(Request $request,$id){

    	$staff_details = SmStaff::find($id);
    	$modulesRole = SmModule::where('active_status', 1)->get();
    	$role_permissions = SmRolePermission::where('staff_id', $id)->get();
    	$already_assigned = [];
    	foreach($role_permissions as $role_permission){
    		$already_assigned[] = $role_permission->module_link_id;
    	}
    	return view('backEnd.systemSettings.role.assign_role_permission', compact('staff_details', 'modulesRole', 'already_assigned'));
	}
	


    public function rolePermissionStore(Request $request){

    	SmRolePermission::where('staff_id', $request->staff_id)->delete();

    	if(isset($request->permissions)){
	    	foreach($request->permissions as $permission){
	    		$role_permission = new SmRolePermission();
	    		$role_permission->staff_id = $request->staff_id;
	    		$role_permission->module_link_id = $permission;
	    		$role_permission->save();
	    	}
	    }
        if (ApiBaseMethod::checkUrl($request->fullUrl())) {
            return ApiBaseMethod::sendResponse(null, 'Role permission has been assigned successfully');
        }
    	return redirect('role')->with('message-success-delete', 'Role permission has been assigned successfully');
    }
}
