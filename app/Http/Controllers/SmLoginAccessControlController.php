<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\SmClass;
use App\SmStudent;
use App\SmStaff; 
use App\User;

class SmLoginAccessControlController extends Controller
{
    public function loginAccessControl(){
    	$roles = Role::all();
        $classes = SmClass::all();

        return view('backEnd.systemSettings.login_access_control', compact('roles', 'classes'));
    }

    public function searchUser(Request $request){
		$request->validate([
			'role' => 'required'
		]); 

    	$role = $request->role;
		$roles = Role::all(); 
		$classes=[];
    	$staffs = SmStaff::where('active_status', 1)->where('role_id', $request->role)->get();
    	return view('backEnd.systemSettings.login_access_control', compact('staffs', 'role', 'roles', 'classes'));
    }
 
    public function loginAccessPermission(Request $request){
    	if($request->status == 'on'){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

    	
    	$user = User::find($request->id);
    	$user->access_status = $status;
    	$user->save();

    	return response()->json($request->id);
    }
}
