<?php
namespace App\Http\Controllers;
use DB;
use App\SmToDo;
use App\SmStaff;
use App\SmParent;
use App\SmHoliday;
use App\SmStudent;
use App\YearCheck;
use App\SmItemSell;
use App\SmAddIncome;
use App\SmAddExpense;
use App\SmFeesPayment;
use App\SmItemReceive;
use App\SmNoticeBoard;
use App\SmHrPayrollGenerate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('PM');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function dashboard()
    {
        $role_id = Session::get('role_id'); 
        return redirect('admin-dashboard'); 
    }



    // for display dashboard

    public function index()
    { 
        if(Auth::user() =="") 
        return redirect('login'); 
        
        $user_id = Auth()->user()->id;
        $totalStaffs = SmStaff::where('active_status', 1)->where('role_id', '!=', 1)->where('role_id', '!=', 4)->get();
        $toDoLists = SmToDo::where('complete_status', 'P')->where('created_by', $user_id)->get();
        $toDoListsCompleteds = SmToDo::where('complete_status', 'C')->where('created_by', $user_id)->get();
        $notices = SmNoticeBoard::select('*')->where('active_status', 1)->where('created_at', 'LIKE', '%' . YearCheck::getYear() . '%')->get();
        $year = YearCheck::getYear();
        $currency='$';
        return view('backEnd.dashboard', compact('currency', 'totalStaffs', 'toDoLists', 'notices', 'toDoListsCompleteds', 'year'));
    }

    public function saveToDoData(Request $request)
    {
        $toDolists = new SmToDo();
        $toDolists->todo_title = $request->todo_title;
        $toDolists->date = date('Y-m-d', strtotime($request->date));
        $toDolists->created_by = Auth()->user()->id;
        $results = $toDolists->save();

        if ($results) {
            return redirect()->back()->with('message-success', 'To Do Data added successfully');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }

    public function viewToDo($id)
    {
        $toDolists = SmToDo::where('id', $id)->first();
        return view('backEnd.dashboard.viewToDo', compact('toDolists'));
    }

    public function editToDo($id)
    {
        $editData = SmToDo::find($id);
        return view('backEnd.dashboard.editToDo', compact('editData', 'id'));
    }
    public function updateToDo(Request $request)
    {
        $to_do_id = $request->to_do_id;

        $toDolists = SmToDo::find($to_do_id);
        $toDolists->todo_title = $request->todo_title;
        $toDolists->date = date('Y-m-d', strtotime($request->date));
        $toDolists->complete_status = $request->complete_status;
        $toDolists->updated_by = Auth()->user()->id;
        $results = $toDolists->update();

        if ($results) {
            return redirect()->back()->with('message-success', 'To Do Data updated successfully');
        } else {
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }

    public function removeToDo(Request $request)
    {

        $to_do = SmToDo::find($request->id);
        $to_do->complete_status = "C";
        $to_do->save();
        $html = "";
        return response()->json('html');
    }

    public function getToDoList(Request $request)
    {
        $to_do_list = SmToDo::where('complete_status', 'C')->get();
        $datas = [];
        foreach ($to_do_list as $to_do) {
            $datas[] = array(
                'title' => $to_do->todo_title,
                'date' => date('jS M, Y', strtotime($to_do->date))
            );
        }

        return response()->json($datas);
    }

    public function viewNotice($id)
    {
        $notice = SmNoticeBoard::find($id);
        return view('backEnd.dashboard.view_notice', compact('notice'));
    }


    public function updatePassowrd()
    {
        return view('backEnd.update_password');
    }


    public function updatePassowrdStore(Request $request)
    {

        $request->validate([
            'current_password' => "required",
            'new_password'  => "required|same:confirm_password|min:6|different:current_password",
            'confirm_password'  => 'required|min:6'
        ]);


        $user = Auth::user();



        if (Hash::check($request->current_password, $user->password)) {

            $user->password = Hash::make($request->new_password);
            $result = $user->save();

            if ($result) {
                return redirect()->back()->with('message-success', 'Password has been changed successfully');
            } else {
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } else {
            return redirect()->back()->with('password-error', 'You have entered a wrong current password');
        }
    }
}
