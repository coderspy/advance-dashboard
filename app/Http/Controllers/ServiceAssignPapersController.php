<?php

namespace App\Http\Controllers;

use App\ServiceAssignPapers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceAssignPapersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceAssignPapers  $serviceAssignPapers
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceAssignPapers $serviceAssignPapers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceAssignPapers  $serviceAssignPapers
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceAssignPapers $serviceAssignPapers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceAssignPapers  $serviceAssignPapers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceAssignPapers $serviceAssignPapers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceAssignPapers  $serviceAssignPapers
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceAssignPapers $serviceAssignPapers)
    {
        //
    }
}
