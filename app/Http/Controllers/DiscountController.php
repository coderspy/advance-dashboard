<?php

namespace App\Http\Controllers;

use App\Discount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use App\ServiceCategory;
use App\Service;
use App\SmStaff;
use App\Role;

class DiscountController extends Controller
{
   
    public function index()
    {
        $data = Discount::get();
        return view('backEnd.all_pages.add_discount',compact('data'));
    }

    public function store_discount(Request $request)
    {
            $request->validate([
            'title' => 'required', 
            'type' => 'required', 
            'amount' => 'required', 
            'discount_start_date' => 'required', 
            'discount_end_date' => 'required|after:discount_start_date', 
        ]); 

        $s = new Discount();
        $s->title = $request->title;
        $s->type = $request->type;
        $s->amount = $request->amount; 
        $s->note = $request->note; 
        $s->discount_start_date =  date('Y-m-d', strtotime($request->discount_start_date)) ; 
        $s->discount_end_date =  date('Y-m-d', strtotime($request->discount_end_date)) ; 
        // return $s;
        $result = $s->save(); 

        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('discount');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }



        public function Edit_Discount($id){
        $editData= Discount::find($id);
        $data = Discount::get();
        return view('backEnd.all_pages.add_discount', compact('data','editData'));
      }

 
    public function discount_update(Request $request)
    {
        $request->validate([
            'title' => 'required', 
            'type' => 'required', 
            'amount' => 'required', 
            'discount_start_date' => 'required', 
            'discount_end_date' => 'required|after:discount_start_date', 
        ]); 

        $s = Discount::find($request->id);
        $s->title = $request->title;
        $s->type = $request->type;
        $s->amount = $request->amount; 
        $s->note = $request->note; 
        $s->discount_start_date =  date('Y-m-d', strtotime($request->discount_start_date)) ; 
        $s->discount_end_date =  date('Y-m-d', strtotime($request->discount_end_date)) ; 
        $result = $s->save(); 

        if ($result) {
            Toastr::success('Operation successful', 'Success');
            return redirect('discount');
        } else {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

      public function discount_delete($id){ 
        try {
            $delete_query = Discount::destroy($id);  
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();  
        } catch (\Illuminate\Database\QueryException $e) { 

            $msg = 'This data already used in  another table. Please remove those data first';
            Toastr::error( $msg, 'Failed');
            return redirect()->back();
        

        } catch (\Exception $e) { 
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        } 
    }
}
