<?php

namespace App\Http\Controllers;

use App\SmModuleLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SmModuleLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SmModuleLink  $smModuleLink
     * @return \Illuminate\Http\Response
     */
    public function show(SmModuleLink $smModuleLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SmModuleLink  $smModuleLink
     * @return \Illuminate\Http\Response
     */
    public function edit(SmModuleLink $smModuleLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SmModuleLink  $smModuleLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmModuleLink $smModuleLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmModuleLink  $smModuleLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmModuleLink $smModuleLink)
    {
        //
    }
}
