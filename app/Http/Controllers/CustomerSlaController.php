<?php

namespace App\Http\Controllers;

use PDF;
use File;
use App\User;
use Exception;
use App\SmItem;
use App\Service;
use App\SmStaff;
use App\SmSupplier;
use App\CustomerSla;
use App\SlaServices;
use App\SlaTemplate;
use App\SmAddIncome;
use App\SmQuotation;
use App\SmNotification;
use App\ServiceCategory;
use App\ArchiveDocuments;
use App\SmGeneralSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CustomerSlaController extends Controller
{

    public function createSlaTemplate()
    {
        $slaTemplate = SlaTemplate::all();
        return view('backEnd/sla/slaTemplate', compact('slaTemplate'));
    }

    public function storeSlaTemplate(Request $request)
    {

        $request->validate([
            'message' => 'required',
        ]);
        try {
            $s = new SlaTemplate();
            $s->message = $request->message;
            $s->active_status  = $request->active_status;
            $s->created_at     = date('Y-m-d h:i:s');
            $s->updated_at     = date('Y-m-d h:i:s');
            $s->created_by     = Auth::user()->id;
            $s->updated_by     = Auth::user()->id;
            $result = $s->save();

            if ($request->active_status == 1) {
                SlaTemplate::where('id', '!=', $s->id)->update(['active_status' => 0]);
            }
            if ($result) {
                Toastr::success('message-success', 'Sla Template has been created successfully');
                return redirect('create-sla-template');
            } else {
                Toastr::error('message-danger', 'Ops! Sorry. Operation failed');
                return redirect()->back();
            }
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Toastr::error('message-danger', 'Ops! Sorry. Operation failed');
            return redirect()->back();
        }
    }
    public function SlaTemplateUpdate(Request $request)
    {

        $request->validate([
            'message' => 'required',
        ]);
        try {
            $s = SlaTemplate::find($request->id);
            $s->message = $request->message;
            $s->active_status  = $request->active_status;
            $s->created_at     = date('Y-m-d h:i:s');
            $s->updated_at     = date('Y-m-d h:i:s');
            $s->created_by     = Auth::user()->id;
            $s->updated_by     = Auth::user()->id;
            $result = $s->save();
            if ($request->active_status == 1) {
                SlaTemplate::where('id', '!=', $s->id)->update(['active_status' => 0]);
            }

            if ($result) {
                Toastr::success('message-success', 'Sla Template has been updated successfully');
                return redirect('create-sla-template');
            } else {
                Toastr::error('Operation Failed', 'message-danger');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function SlaTemplateDelete($id)
    {
        try {
            $result = SlaTemplate::destroy($id);
            // return $id;
            if ($result) {
                return redirect('create-sla-template')->with('message-success', 'sla has been deleted successfully');
            } else {
                return redirect('create-sla-template')->with('message-danger', 'Ops! Sorry. Operation failed');
            }
        } catch (\Exception $e) {
            return redirect('create-sla-template')->with('message-danger', 'Ops! Sorry. Operation failed');
        }
    }
    public function SlaTemplateEdit($id)
    {
        $editData = SlaTemplate::find($id);
        $slaTemplate = SlaTemplate::all();

        return view('backEnd/sla/slaTemplate', compact('slaTemplate', 'editData'));
    }



    public function index()
    {
        $quotations = SmQuotation::all();
        $services    = Service::all();
        $ServiceCategory = ServiceCategory::all();
        $customers  = SmStaff::where('role_id', 4)->get();
        // $discounts=DB::table('discount_assigns')
        // ->join('discounts', 'discounts.id','=', 'discount_assigns.discount_id')
        // ->where('discount_assigns.user_id',Auth::user()->id)
        // ->where('discounts.discount_start_date','<=',date('Y-m-d'))
        // ->where('discounts.discount_end_date','>=',date('Y-m-d'))
        // ->select('discounts.title', 'discounts.type','discounts.amount','discounts.id')
        // ->get(); 
        return view('backEnd/sla/sla_add', compact('quotations', 'services', 'customers', 'ServiceCategory'));
    }

    public function createSla()
    {
        $customers  = SmStaff::where('role_id', 4)->get();
        $vendors    = SmSupplier::all();
        $items      = Service::all();
        $quotations = SmQuotation::all();
        $departments = SmStaff::all();
        $services    = Service::all();

        return view('backEnd/sla/create_sla', compact('quotations', 'customers', 'vendors', 'items', 'departments', 'services'));
    }


    public function storeSla(Request $request)
    {


        $input = $request->all();
        $validator = Validator::make($input, [
            'days'         => 'required',
            'number'        => 'required',
            'date'          => 'required',
            //'file'          => 'required|array',
        ]);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // return $s;
        DB::beginTransaction();
        try {

            if ($request->customer_type == "new") {
                $user               = new User();
                $user->access_status =  1;
                $user->created_by   =  1;
                $user->updated_by   =  1;
                $user->role_id      =  4;
                $user->full_name    =  $request->first_name . ' ' . $request->last_name;
                $user->email        = $request->email;
                $user->username     =  $request->email;
                $user->password     =  Hash::make('123456');
                $user->created_at   =  date('Y-m-d h:i:s');
                $user->save();

                $last_staff = SmStaff::where('role_id', 4)->orderBy('id', 'DESC')->first();

                $s = new SmStaff();
                $s->user_id = $user->id;
                $s->role_id = 4;
                $s->staff_no = $last_staff->staff_no + 1;
                $s->designation_id = 1;
                $s->department_id = 1;
                $s->first_name = $request->first_name;
                $s->last_name = $request->last_name;
                $s->full_name = $request->first_name . ' ' . $request->last_name;
                $s->email = $request->email;
                $s->gender_id = 1;
                $s->mobile = $request->mobile;
                $s->passport_number = $request->passport_number;
                $s->nid_number = $request->nid_number;
                $s->staff_photo = 'public/uploads/staff/1.png';
                $s->save();
            } else {
                $s = SmStaff::where('id', $request->customer_id)->first();
            }

            $quotation = new CustomerSla();

            $quotation->title               = $request->title;
            $quotation->number              = $request->number;
            $quotation->date                = date('Y-m-d', strtotime($request->date));
            $quotation->reference           = $request->reference;
            //customer_details

            if ($request->customer_type == "new") {
                $quotation->customer_id         =  $s->id;
                $quotation->customer_name       =  $request->first_name . ' ' . $request->last_name;
            } else {
                $quotation->customer_id         = $request->customer_id;
                $customer_details               = SmStaff::find($request->customer_id);
                // $quotation->customer_name       = $s->full_name;

            }
            //amount


            $quotation->days        = $request->days;
            $quotation->total_amount        = $request->Etotal;
            $quotation->paybale_amount        = $request->Ebid_amount;
            $quotation->paid_amount        = $request->paid_amount;
            if ($request->Ebid_amount == $request->paid_amount) {
                $quotation->payment_status        = 'P';
            }
            if ($request->Ebid_amount > $request->paid_amount && $request->paid_amount > 0) {
                $quotation->payment_status        = 'PP';
            }
            if ($request->Ebid_amount > $request->paid_amount && $request->paid_amount == 0) {
                $quotation->payment_status        = 'UP';
            }

            //footer note 
            $quotation->private_note        = $request->private_note;
            $quotation->public_note         = $request->public_note;
            $quotation->terms_note          = $request->terms_note;
            $quotation->footer_note         = $request->footer_note;
            $quotation->signature_person    = $request->signature_person;
            $quotation->signature_company   = $request->signature_company;

            //    return $quotation;

            $quotation->created_by    = Auth::user()->id;
            $quotation->save();
            $quotation->toArray();

            $data               = CustomerSla::find($quotation->id);
            $data['note']       = '"' . $data->title . '" has been added.';
            $data['model_name'] = 'SmQuotation';
            $data['old_data']   = $data->toJson();
            $data['new_data']   = '';
            $data['action']     = 'Insert';
            $data['action_id']  = $data->id;
            $result             = SmGeneralSettings::StoreAllActivities($data);

            $i = 0;
            if ($result) {
                foreach ($request->Eproducts as $product) {
                    $quotation_product                      = new SlaServices();
                    $quotation_product->sla_id        = $quotation->id;
                    $quotation_product->discount_id        = $request->discount;
                    $quotation_product->service_id          = $product;
                    $quotation_product->govt_price                 = $request->Eproducts[$i];
                    // $quotation_product->sale_unit_price          = $request->Eunit_price[$i];
                    $quotation_product->save();

                    $data               = SlaServices::find($quotation_product->id);
                    $data['note']       = '"quotation No' . $request->quotation_no . ' & Product Id ' . $data->product_id . '" has been added.';
                    $data['model_name'] = 'SmQuotationProducts';
                    $data['old_data']   = $data->toJson();
                    $data['new_data']   = '';
                    $data['action']     = 'Insert';
                    $data['action_id']  = $data->id;
                    $result             = SmGeneralSettings::StoreAllActivities($data);
                    $i++;
                }
            }

            $files = $request->file('file');
            if ($request->hasFile('file')) {
                $paper_counter = 0;
                foreach ($files as $file) {
                    $fileName = md5($file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
                    $file->move('public/uploads/sla/', $fileName);
                    $fileName = 'public/uploads/sla/' . $fileName;

                    $archive_document                  = new ArchiveDocuments();
                    $archive_document->file            =  $fileName;
                    $archive_document->customer_id     = $s->id;
                    $archive_document->sla_id     = $quotation->id;
                    $archive_document->paper_id        = $request->file_name[$paper_counter++];
                    $archive_document->save();
                }
            }
                $store_notification = new SmNotification();
                $store_notification->message        = 'Create a SLA';
                $store_notification->date           = date('Y-m-d', strtotime($request->date));
                $store_notification->user_id        = 1;
                $store_notification->role_id        = 1;
                $store_notification->url            = route('created_sla_list',$quotation->id);
                $store_notification->active_status  = 1;
                $store_notification->created_at     = date('Y-m-d h:i:s');
                $store_notification->updated_at     = date('Y-m-d h:i:s');
                $store_notification->created_by     = Auth::user()->id;
                $store_notification->updated_by     = Auth::user()->id;
                $store_notification->save();

            if ($request->paid_amount > 0) {
                $income = new SmAddIncome();
                $income->sla_id = $quotation->id;
                $income->income_head_id = 1;
                $income->account_id = 1;
                $income->payment_method_id = 1;
                $income->amount = $request->paid_amount;
                $income->date = date('Y-m-d', strtotime($request->created_at));
                $income->save();
            }



            DB::commit();
            return redirect('sla-list')->with('message-success', 'Sla has been created successfully');
        } catch (\Exception $e) {
            return $e;
            DB::rollback();
            return redirect()->back()->with('message-danger', 'Ops! Sorry. Operation failed. ' . $e);
        }
    }


    public function slaStore(Request $request)
    {
        //    return $request;
        try {


            if ($request->customer_type == "new") {
                $user               = new User();
                $user->access_status =  1;
                $user->created_by   =  1;
                $user->updated_by   =  1;
                $user->role_id      =  4;
                $user->full_name    =  $request->first_name . ' ' . $request->last_name;
                $user->email        = $request->email;
                $user->username     =  $request->email;
                $user->password     =  Hash::make('123456');
                $user->created_at   =  date('Y-m-d h:i:s');
                $user->save();

                $last_staff = SmStaff::where('role_id', 4)->orderBy('id', 'DESC')->first();

                $s = new SmStaff();
                $s->user_id = $user->id;
                $s->role_id = 4;
                $s->staff_no = $last_staff->staff_no + 1;
                $s->designation_id = 1;
                $s->department_id = 1;
                $s->first_name = $request->first_name;
                $s->last_name = $request->last_name;
                $s->full_name = $request->first_name . ' ' . $request->last_name;
                $s->email = $request->email;
                $s->gender_id = 1;
                $s->mobile = $request->mobile;
                $s->passport_number = $request->passport_number;
                $s->nid_number = $request->nid_number;
                $s->staff_photo = 'public/uploads/staff/1.png';
                $s->save();
            }



            $store_sla = new CustomerSla();
            // $store_sla->service_id     = 1;
            $store_sla->number         = $request->number;
            $store_sla->date           =  date('Y-m-d', strtotime($request->date));
            $store_sla->customer_type  = $request->customer_type;
            $store_sla->days           = $request->days;
           // $store_sla->description    = $request->description;
            $store_sla->reference      = $request->reference;

            if ($request->customer_type == "new") {
                $customer_ID         =  $s->id;
            } else {
                $customer_ID            =  $request->customer_id;
            }
            $store_sla->customer_name = '';
            $store_sla->customer_id         = $customer_ID;

            // return $request->paid_amount;

            $store_sla->days        = $request->days;
            $store_sla->total_amount        = $request->Etotal;
            $store_sla->paybale_amount        = $request->Ebid_amount;
            $store_sla->paid_amount        = $request->paid_amount;
            if ($request->Ebid_amount == $request->paid_amount) {
                $store_sla->payment_status        = 'P';
            }
            if ($request->Ebid_amount > $request->paid_amount && $request->paid_amount > 0) {
                $store_sla->payment_status        = 'PP';
            }
            if ($request->Ebid_amount > $request->paid_amount && $request->paid_amount == 0) {
                $store_sla->payment_status        = 'UP';
            }

            //footer note 
            $store_sla->private_note        = $request->private_note;
            $store_sla->public_note         = $request->public_note;
            $store_sla->terms_note          = $request->terms_note;
            $store_sla->footer_note         = $request->footer_note;
            $store_sla->signature_person    = $request->signature_person;
            $store_sla->signature_company   = $request->signature_company;


            $store_sla->created_by    = Auth::user()->id;

            $result = $store_sla->save();

            $services = $request->Eproducts;
            //    return $services;
            foreach ($request->Eproducts as $Sid) {
                $sla_services = new SlaServices();
                $sla_services->service_id = $Sid;
                $sla_services->sla_id = $store_sla->id;
                $sla_services->save();
            }


            $files = $request->file('file');
            if ($request->hasFile('file')) {
                $paper_counter = 0;
                foreach ($files as $file) {
                    $fileName = md5($file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
                    $file->move('public/uploads/sla/', $fileName);
                    $fileName = 'public/uploads/sla/' . $fileName;

                    $s                  = new ArchiveDocuments();
                    $s->file            =  $fileName;
                    $s->customer_id     = $customer_ID;
                    $s->paper_id        = $request->file_name[$paper_counter++];
                    $s->save();
                }
            }

            $store_notification = new SmNotification();

            $store_notification->message        = 'Create a SLA';
            $store_notification->date           = date('Y-m-d', strtotime($request->date));
            $store_notification->user_id        = Auth::user()->id;
            $store_notification->role_id        = 3;
            $store_notification->url            = url('sla-list');
            $store_notification->active_status  = 1;
            $store_notification->created_at     = date('Y-m-d h:i:s');
            $store_notification->updated_at     = date('Y-m-d h:i:s');
            $store_notification->created_by     = Auth::user()->id;
            $store_notification->updated_by     = Auth::user()->id;
            $store_notification->save();

            return redirect('sla-list')->with('message-success', 'SLA has been created successfully');
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }


    public function sla_list()
    {
        if (Auth::user()->role_id == 1) {
            $slas = CustomerSla::all();
        } else {
            $slas = CustomerSla::where('created_by', Auth::user()->id)->get();
        }

        return view('backEnd/sla/sla_list', compact('slas'));
    }

    function created_sla_list($id){
            $slas = CustomerSla::where('id', $id)->get();
             return view('backEnd.sla.sla_list', compact('slas'));
    }

    public function show($id)
    {
        $sla = CustomerSla::find($id);
        return view('backEnd/sla/slaView', compact('sla'));
    }
    public function slaPayment($id)
    {
        $sla = CustomerSla::find($id);
        return view('backEnd/sla/slaPayment', compact('sla'));
    }
    public function slaPaymentReceive(Request $request)
    {

        // return $request;
        $input = $request->all();
        $validator = Validator::make($input, [
            'due' => 'required|numeric',
            'payment' => 'required|numeric|max:due'
        ]);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $sla = CustomerSla::findOrfail($request->id);
            $new_payment = $sla->paid_amount + $request->payment;
            $sla->paid_amount = $new_payment;

            if ($sla->paybale_amount == $new_payment) {
                $sla->payment_status        = 'P';
            }
            if ($sla->paybale_amount > $new_payment && $new_payment > 0) {
                $sla->payment_status        = 'PP';
            }
            if ($sla->paybale_amount > $new_payment && $new_payment == 0) {
                $sla->payment_status        = 'UP';
            }
            $sla->save();
            return redirect('sla-list')->with('message-success', 'Payment Received Successfully');
        } catch (\Esception $e) {
            return redirect('sla-list')->with('message-danger', 'Ops! Sorry. Operation failed');
        }
    }

    public function slaDelete($id)
    {
        try {
            $result = CustomerSla::destroy($id);
            // return $id;
            if ($result) {
                return redirect('sla-list')->with('message-success', 'sla has been deleted successfully');
            } else {
                return redirect('sla-list')->with('message-danger', 'Ops! Sorry. Operation failed');
            }
        } catch (\Illuminate\Database\QueryException $e) { 

            $msg = 'This data already used in  another table. Please remove those data first';
            Toastr::error( $msg, 'Failed');
            return redirect()->back();
        
        } catch (\Exception $e) {
            return redirect('sla-list')->with('message-danger', 'Ops! Sorry. Operation failed');
        }
    }
    public function edit($id)
    {
        $edit = CustomerSla::find($id);
        $customers  = SmStaff::where('role_id', 4)->get();
        $vendors    = SmSupplier::all();
        $items      = Service::all();
        $quotations = SmQuotation::all();
        $departments = SmStaff::all();
        $services    = Service::all();

        return view('backEnd.sla.sla_edit', compact('edit', 'quotations', 'customers', 'vendors', 'items', 'departments', 'services'));
    }


    public function slaUpdate(Request $request)
    {


        $input = $request->all();
        $validator = Validator::make($input, [
            'days'         => 'required',
            'number'        => 'required',
            'date'          => 'required',
            //'file'          => 'nullable|array',
        ]);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // return $s;
        DB::beginTransaction();
        try {

            if ($request->customer_type == "new") {
                $user               = new User();
                $user->access_status =  1;
                $user->created_by   =  1;
                $user->updated_by   =  1;
                $user->role_id      =  4;
                $user->full_name    =  $request->first_name . ' ' . $request->last_name;
                $user->email        = $request->email;
                $user->username     =  $request->email;
                $user->password     =  Hash::make('123456');
                $user->created_at   =  date('Y-m-d h:i:s');
                $user->save();

                $last_staff = SmStaff::where('role_id', 4)->orderBy('id', 'DESC')->first();

                $s = new SmStaff();
                $s->user_id = $user->id;
                $s->role_id = 4;
                $s->staff_no = $last_staff->staff_no + 1;
                $s->designation_id = 1;
                $s->department_id = 1;
                $s->first_name = $request->first_name;
                $s->last_name = $request->last_name;
                $s->full_name = $request->first_name . ' ' . $request->last_name;
                $s->email = $request->email;
                $s->gender_id = 1;
                $s->mobile = $request->mobile;
                $s->passport_number = $request->passport_number;
                $s->nid_number = $request->nid_number;
                $s->staff_photo = 'public/uploads/staff/1.png';
                $s->save();
            } else {
                $s = SmStaff::where('id', $request->customer_id)->first();
            }

            $quotation = CustomerSla::findOrFail($request->id);

            $quotation->title               = $request->title;
            $quotation->number              = $request->number;
            $quotation->date                = date('Y-m-d', strtotime($request->date));
            $quotation->reference           = $request->reference;
            //customer_details

            if ($request->customer_type == "new") {
                $quotation->customer_id         =  $s->id;
                $quotation->customer_name       =  $request->first_name . ' ' . $request->last_name;
            } else {
                $quotation->customer_id         = $request->customer_id;
                $customer_details               = SmStaff::find($request->customer_id);
                // $quotation->customer_name       = $s->full_name;

            }
            //amount


            $quotation->days        = $request->days;
            $quotation->total_amount        = $request->Etotal;
            $quotation->paybale_amount        = $request->Ebid_amount;
            $quotation->paid_amount        = $request->paid_amount;
            if ($request->Ebid_amount == $request->paid_amount) {
                $quotation->payment_status        = 'P';
            }
            if ($request->Ebid_amount > $request->paid_amount && $request->paid_amount > 0) {
                $quotation->payment_status        = 'PP';
            }
            if ($request->Ebid_amount > $request->paid_amount && $request->paid_amount == 0) {
                $quotation->payment_status        = 'UP';
            }

            //footer note 
            $quotation->private_note        = $request->private_note;
            $quotation->public_note         = $request->public_note;
            $quotation->terms_note          = $request->terms_note;
            $quotation->footer_note         = $request->footer_note;
            $quotation->signature_person    = $request->signature_person;
            $quotation->signature_company   = $request->signature_company;

            //    return $quotation;

            $quotation->created_by    = Auth::user()->id;
            $quotation->save();
            $quotation->toArray();

            $data               = CustomerSla::find($quotation->id);
            $data['note']       = '"' . $data->title . '" has been added.';
            $data['model_name'] = 'SmQuotation';
            $data['old_data']   = $data->toJson();
            $data['new_data']   = '';
            $data['action']     = 'Insert';
            $data['action_id']  = $data->id;
            $result             = SmGeneralSettings::StoreAllActivities($data);

            $i = 0;
            if ($result) {
                foreach ($request->Eproducts as $product) {
                    SlaServices::where('sla_id', $request->id)->delete();
                    $quotation_product                      = new SlaServices();
                    $quotation_product->sla_id        = $quotation->id;
                    $quotation_product->discount_id        = $request->discount[$i];
                    $quotation_product->service_id          = $product;
                    $quotation_product->govt_price                 = $request->Eproducts[$i];
                    // $quotation_product->sale_unit_price          = $request->Eunit_price[$i];
                    $quotation_product->save();

                    $data               = SlaServices::find($quotation_product->id);
                    $data['note']       = '"quotation No' . $request->quotation_no . ' & Product Id ' . $data->product_id . '" has been added.';
                    $data['model_name'] = 'SmQuotationProducts';
                    $data['old_data']   = $data->toJson();
                    $data['new_data']   = '';
                    $data['action']     = 'Insert';
                    $data['action_id']  = $data->id;
                    $result             = SmGeneralSettings::StoreAllActivities($data);
                    $i++;
                }
            }

            $files = $request->file('file');
            if ($request->hasFile('file')) {
                $paper_counter = 0;
                foreach ($files as $file) {
                    ArchiveDocuments::where('sla_id', $request->id)->delete();
                    $fileName = md5($file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
                    $file->move('public/uploads/sla/', $fileName);
                    $fileName = 'public/uploads/sla/' . $fileName;

                    $archive_document                  = new ArchiveDocuments();
                    $archive_document->file            =  $fileName;
                    $archive_document->customer_id     = $s->id;
                    $archive_document->sla_id     = $quotation->id;
                    $archive_document->paper_id        = $request->file_name[$paper_counter++];
                    $archive_document->save();
                }
            }
            $store_notification = new SmNotification();

            // $store_notification->message        = 'Create a SLA';
            // $store_notification->date           = date('Y-m-d', strtotime($request->date));
            // $store_notification->user_id        = Auth::user()->id;
            // $store_notification->role_id        = 3;
            // $store_notification->url            = url('sla-list');
            // $store_notification->active_status  = 1;
            // $store_notification->created_at     = date('Y-m-d h:i:s');
            // $store_notification->updated_at     = date('Y-m-d h:i:s');
            // $store_notification->created_by     = Auth::user()->id;
            // $store_notification->updated_by     = Auth::user()->id;
            // $store_notification->save();

            if (Auth::user()->role_id == 3) {
                $store_notification = new SmNotification();
                $store_notification->message        = 'Updated a SLA';
                $store_notification->date           = date('Y-m-d', strtotime($request->date));
                $store_notification->user_id        = 1;
                $store_notification->role_id        = 1;
                $store_notification->url            = route('created_sla_list',$quotation->id);
                $store_notification->active_status  = 1;
                $store_notification->created_at     = date('Y-m-d h:i:s');
                $store_notification->updated_at     = date('Y-m-d h:i:s');
                $store_notification->created_by     = Auth::user()->id;
                $store_notification->updated_by     = Auth::user()->id;
                $store_notification->save();
            }

            if ($request->paid_amount > 0) {

                $income = SmAddIncome::updateOrCreate(['sla_id' => $quotation->id]);
                $income->income_head_id = 1;
                $income->sla_id = $quotation->id;
                $income->account_id = 1;
                $income->payment_method_id = 1;
                $income->amount = $request->paid_amount;
                $income->date = date('Y-m-d', strtotime($request->created_at));
                $income->save();
            }


            DB::commit();
            return redirect('sla-list')->with('message-success', 'Sla has been updated successfully');
        } catch (\Exception $e) {
            // return $e;
            DB::rollback();
            return redirect()->back()->with('message-danger', 'Ops! Sorry. Operation failed. ' . $e);
        }
    }






    public function changeStatus(Request $request, $id)
    {
        try {
            $sla = CustomerSla::where('id', $id)->first();
            if ($sla->is_approved == 0) {
                $sla->is_approved = 1;
                if ($sla->Created_by->role_id == 3) {
                    $store_notification = new SmNotification();
                    $store_notification->message        = 'Sla Approved';
                    $store_notification->date           = date('Y-m-d');
                    $store_notification->user_id        = $sla->created_by;
                    $store_notification->role_id        = 3;
                    $store_notification->url            = route('created_sla_list',$sla->id);
                    $store_notification->active_status  = 1;
                    $store_notification->created_at     = date('Y-m-d h:i:s');
                    $store_notification->updated_at     = date('Y-m-d h:i:s');
                    $store_notification->created_by     = Auth::user()->id;
                    $store_notification->updated_by     = Auth::user()->id;
                    $store_notification->save();
                }
            } else {
                $sla->is_approved = 0;
            }
            $sla->save();


            return redirect('sla-list')->with('message-success', 'Sla has been updated successfully');
        } catch (\Exception $e) {
            return redirect('sla-list')->with('message-danger', 'Ops! Sorry. Operation failed');
        }
    }
}
