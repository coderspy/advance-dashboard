<?php

namespace App\Http\Controllers;

use App\ArchiveDocuments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArchiveDocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArchiveDocuments  $archiveDocuments
     * @return \Illuminate\Http\Response
     */
    public function show(ArchiveDocuments $archiveDocuments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArchiveDocuments  $archiveDocuments
     * @return \Illuminate\Http\Response
     */
    public function edit(ArchiveDocuments $archiveDocuments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ArchiveDocuments  $archiveDocuments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArchiveDocuments $archiveDocuments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArchiveDocuments  $archiveDocuments
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArchiveDocuments $archiveDocuments)
    {
        //
    }
}
