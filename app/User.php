<?php

namespace App;

use App\Message;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable; 

class User extends Authenticatable
{
    use Notifiable;
    public static $item ="23876323";  //23876323 //22014245

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
 
    public function staff(){
        return $this->belongsTo('App\SmStaff', 'id', 'user_id');
    }

    
    public static function NumberToBangladeshiTakaFormat($amount = 0)
    {

        $tmp      = explode(".", $amount); // for float or double values
        $strMoney = "";
        $amount   = $tmp[0];
        $strMoney .= substr($amount, -3, 3);
        $amount = substr($amount, 0, -3);
        while (strlen($amount) > 0) {
            $strMoney = substr($amount, -2, 2) . "," . $strMoney;
            $amount   = substr($amount, 0, -2);
        }

        if (isset($tmp[1])) // if float and double add the decimal digits here.
        {
            return $strMoney . "." . $tmp[1];
        } else {
            return $strMoney . '.00';
        }

    }

/*     public function messages()
    {
    return $this->hasMany(Message::class);
    } */

    public function messages()
    {
        return $this->hasMany(Chat::class, 'receiver_id');
    }
}
