<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Papers extends Model
{
   
    public function getServicesName(){
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }
}
