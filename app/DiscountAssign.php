<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountAssign extends Model
{
    public function getDiscountName(){
        return $this->belongsTo('App\Discount', 'discount_id', 'id');
    }
}
