<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SmQuotationProducts extends Model
{

    protected $table = "sm_quotation_services";

    public function product(){
    	return $this->belongsTo('App\Service', 'service_id', 'id');
    }

    public static function getPaper($edit_id,$id){
        $data = DB::table('services')       
          ->join('service_assign_papers', 'service_assign_papers.service_id', '=', 'services.id')
          ->join('papers', 'papers.id', 'service_assign_papers.service_paper_id' ) 
          ->join('archive_documents', 'papers.id', 'archive_documents.paper_id' ) 
          ->where('services.id',$id)
          ->where('archive_documents.quotation_id',$edit_id)
          ->select('services.id as rowId',  'papers.title', 'papers.id as paperId','archive_documents.id as arch_id') 
          ->first(); 
          return $data;
      }

    
}
