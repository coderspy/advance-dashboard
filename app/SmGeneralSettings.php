<?php  
namespace App; 
use App\SmClass;
use App\SmSection;
use App\SmStudent;
use App\SmSubject;
use App\YearCheck;
use App\SmExamType;
use App\SmLanguage;
use App\SmMarksGrade;
use App\SmResultStore;
use App\SmDateFormat;  
use App\SmAssignSubject; 
use App\SmTemporaryMeritlist;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr; 
use Illuminate\Database\Eloquent\Model;

class SmGeneralSettings extends Model
{
    

    public static function StoreAllActivities($data)
    {

        $s = new SmActivity();
        $s->note            =   $data['note'];
        $s->model_name      =   $data['model_name'];
        $s->old_data        =   $data['old_data'];
        $s->new_data        =   $data['new_data'];
        $s->action          =   $data['action'];
        $s->action_id       =   $data['action_id'];
        $s->author_id       =   Auth::user()->id;
        $s->author_mode     =   'users';
        $r = $s->save();
        return $r;
    }

    

    public function languages()
    {
        return $this->belongsTo('App\SmLanguage', 'language_id', 'id');
    }

    public function dateFormats()
    {
        return $this->belongsTo('App\SmDateFormat', 'date_format_id', 'id');
    }
    public static function getLanguageList()
    {
        $languages = SmLanguage::all();
        return $languages;
    }

    public static function value()
    {
        $value = SmGeneralSettings::first();
        return $value->system_purchase_code;
    }

    public static function SUCCESS($redirect_specific_message = null)
    {
        if ($redirect_specific_message) {
            Toastr::success($redirect_specific_message, 'Success');
        } else {
            Toastr::success('Operation successful', 'Success');
        }
        return;
    }
    public static function ERROR($redirect_specific_message = null)
    {
        if ($redirect_specific_message) {
            Toastr::error($redirect_specific_message, 'Failed');
        } else {
            Toastr::error('Operation Failed', 'Failed');
        }
        return;
    }


    public function timeZone()
    {
        return $this->belongsTo('App\SmTimeZone', 'time_zone_id', 'id');
    }



    ///DateConvater
    public static function DateConvater($input_date)
    {
        $generalSetting = SmGeneralSettings::find(1);


        $system_date_foramt = SmDateFormat::find($generalSetting->date_format_id);
        $DATE_FORMAT =  $system_date_foramt->format;
        echo date_format(date_create($input_date), $DATE_FORMAT);
    }




 

}
