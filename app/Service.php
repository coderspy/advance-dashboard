<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function getCategoryName(){
        return $this->belongsTo('App\ServiceCategory', 'category_id', 'id');
    }
    public function getDiscountAssign(){
        return $this->hasMany('App\DiscountAssign','service_id', 'id');
    }
      
   

}
