<?php

namespace App\Console\Commands;

use App\CustomerSla;
use App\SmQuotation;
use App\SmNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class Notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notiication description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d', strtotime('30 days'));
        $sla = CustomerSla::where('date', '=', $date)->get();
        foreach ($sla as $key => $value) {
            $store_notification = new SmNotification();
            $store_notification->message        = 'SLA date will expire '.App\SmGeneralSettings::DateConvater($value->date);
            $store_notification->date           = date('Y-m-d', strtotime($request->date));
            $store_notification->user_id        = $value->created_by;
            $store_notification->role_id        = $value->Created_by->role_id;
            $store_notification->url            = route('created_sla_list',$value->id);
            $store_notification->active_status  = 1;
            $store_notification->created_at     = date('Y-m-d h:i:s');
            $store_notification->updated_at     = date('Y-m-d h:i:s');
            $store_notification->created_by     = Auth::user()->id;
            $store_notification->updated_by     = Auth::user()->id;
            $store_notification->save();
        }
        $sla = SmQuotation::where('date', '=', $date)->get();
        foreach ($sla as $key => $value) {
            $store_notification = new SmNotification();
            $store_notification->message        = 'Quotation date will expire '.App\SmGeneralSettings::DateConvater($value->date);
            $store_notification->date           = date('Y-m-d', strtotime($request->date));
            $store_notification->user_id        = $value->created_by;
            $store_notification->role_id        = $value->Created_by->role_id;
            $store_notification->url            = route('created_Quotation_list',$value->id);
            $store_notification->active_status  = 1;
            $store_notification->created_at     = date('Y-m-d h:i:s');
            $store_notification->updated_at     = date('Y-m-d h:i:s');
            $store_notification->created_by     = Auth::user()->id;
            $store_notification->updated_by     = Auth::user()->id;
            $store_notification->save();
        }
    }
}
