<?php
    if (Auth::user() == "") {
        header('location:' . url('/login'));
        exit();
    }

    $staffs = App\SmStaff::where('user_id', Auth::user()->id )->first();
        $modules = [];
        $module_links = [];

        $permissions = App\SmRolePermission::where('staff_id',$staffs->id)->get();
        foreach ($permissions as $permission) {
            $module_links[] = $permission->module_link_id;
            $modules[] = $permission->moduleLink->module_id;
        }
?>



<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/login')}}">
            <img src="{{asset($generalSetting->logo)}}" alt="">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>

    <ul class="list-unstyled components"> 
        {{-- only superadmin can show this menus --}}
        @if(Auth::user()->role_id==1) 
            <li>
                <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">

                    <span class="flaticon-speedometer"></span>
                    dashboard
                </a>
            </li> 
            <li>
                <a href="#services" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Services
                </a>
                <ul class="collapse list-unstyled" id="services"> 
                    <li>
                        <a href="{{route('categoryView')}}">Category</a>
                    </li> 
                       <li>
                        <a href="{{url('services/papers-list')}}">Papers List</a>
                    </li>
                    <li>
                        <a href="{{url('services/prices-list')}}">Price List</a>
                    </li>
                    <li>
                        <a href="{{url('service-list')}}">Create Services</a>
                    </li>
                 
                </ul>
            </li>

            <li>
                <a href="#subMenuHumanResource" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Human Resource
                </a>
                <ul class="collapse list-unstyled" id="subMenuHumanResource"> 
                        {{-- <li>
                            <a href="{{url('services-charges')}}">Services  Charges</a>
                        </li> --}}
                        <li><a href="{{url('department')}}">Department</a></li>
                        <li><a href="{{url('designation')}}">Designation</a></li>  
                        <li><a href="{{route('staff_directory')}}">Users </a></li>   
                        <li><a href="{{route('role')}}">role</a></li>
                        <li><a href="{{url('customer-directory')}}">Customers</a></li>    
                               
                </ul>
            </li> 
            
            <li>
                <a href="#subMenuDiscount" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle"> <span class="flaticon-consultation"></span>  Discount
                </a>
                <ul class="collapse list-unstyled" id="subMenuDiscount"> 
                    <li><a href="{{url('discount')}}"> Create Discount</a></li> 
                    <li><a href="{{url('assign-discount')}}"> Assign Discounts</a></li>  
                    
                </ul>
            </li> 
            
            <li>
                <a href="#subMenuSla" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle"> <span class="flaticon-consultation"></span>  SLA
                </a>
                <ul class="collapse list-unstyled" id="subMenuSla"> 
                    <li><a href="{{url('create-sla-template')}}">SLA & Quotation Template</a></li> 
                    <li><a href="{{url('create-sla')}}">Create SLA</a></li> 
                    <li><a href="{{url('sla-list')}}"> SLA List</a></li>  
                    
                </ul>
            </li> 

            <li>
                <a href="#subMenuQuotation" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-book"></span> Quotations  </a>
                <ul class="collapse list-unstyled" id="subMenuQuotation"> 
                    <li><a href="{{url('quotations/create')}}">New Quotations</a></li> 
                    <li><a href="{{url('quotations')}}">Quotations List</a></li> 
                </ul>
            </li>
  
            <li>
                <a href="#subMenuAccount" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-accounting"></span>
                    @lang('lang.accounts')
                </a>
                <ul class="collapse list-unstyled" id="subMenuAccount"> 
                        <li>
                            <a href="{{url('chart-of-account')}}"> @lang('lang.chart_of_account')</a>
                        </li> 
                        <li>
                            <a href="{{route('payment_method')}}"> @lang('lang.payment_method')</a>
                        </li> 
                        <li>
                            <a href="{{url('bank-account')}}"> @lang('lang.bank_account')</a>
                        </li> 
                        <li>
                            <a href="{{route('add_income')}}"> @lang('lang.income')</a>
                        </li> 
                        <li>
                            <a href="{{route('profit')}}"> @lang('lang.profit')</a>
                        </li> 
                        <li>
                            <a href="{{url('add-expense')}}"> @lang('lang.expense')</a>
                        </li> 
                        <li>
                            <a href="{{route('search_account')}}"> @lang('lang.search')</a>
                        </li> 
                    
                </ul>
            </li> 




            {{-- Communication Menu  --}}
            <li>
                <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-email"></span>
                    Communication
                </a>
                <ul class="collapse list-unstyled" id="subMenuCommunicate"> 
                    
                    <li><a href="{{url('notice-list')}}">Notice Board</a></li> 
                    <li><a href="{{url('send-email-sms-view')}}">Send Email</a></li> 
                    <li><a href="{{url('email-sms-log')}}">Email Logs</a></li>  
                    <li> 
                        <a href="{{route('user.chats')}}">
                            <span class="ti-user"></span>Chatting
                        </a> 
                    </li>

                </ul>
            </li>  
            {{--end Communication Menu  --}}




            <li>
                <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-analysis"></span> Reports
                </a>
                <ul class="collapse list-unstyled" id="subMenusystemReports"> 
                    <li><a href="{{url('reports/pro-user-list')}}">PRO Users</a></li>  
                 {{--    <li><a href="{{url('reports/user-list')}}">Staffs</a></li>   --}}
                    <li><a href="{{url('reports/services-list')}}">Services</a></li>  
                    <li><a href="{{url('reports/daily-services-report')}}">Daily Services Report</a></li>      
                    <li><a href="{{url('reports/customer-monthly-services')}}">All Services Report</a></li> 
                    <li><a href="{{url('reports/services-pending-done')}}">services pending/done</a></li>  
                    <li><a href="{{route('user_log')}}">user log</a></li>
                </ul>
            </li>
            
            
            <li>
                <a href="#subMenusystemSettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-settings"></span> System Settings
                </a>
                <ul class="collapse list-unstyled" id="subMenusystemSettings">
                    <li><a href="{{url('general-settings')}}"> General Settings</a></li>
                    <li><a href="{{url('email-settings')}}">Email Settings</a></li> 
                  {{--   <li><a href="{{url('login-access-control')}}">Login Permission</a></li> --}}
                    <li> <a href="{{route('base_group')}}">Base Group</a></li>
                    <li><a href="{{route('base_setup')}}">Base Setup</a></li>  
                    <li><a href="{{url('backup-settings')}}">Backup Settings</a></li>
                     <li>
                        <a href="{{url('sms-settings')}}">@lang('lang.sms_settings')</a>
                    </li>
                </ul>
            </li> 

            {{-- start frontEnd Settings --}}
            <li>
                <a href="#subMenufrontEndSettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-software"></span> Web Settings
                </a>
                <ul class="collapse list-unstyled" id="subMenufrontEndSettings">
                    <li><a href="{{url('admin-home-page')}}"> Home Page</a></li>
                    <li><a href="{{url('news')}}">News list</a></li>
                    <li><a href="{{url('news-category')}}">News Category</a></li>
                    <li><a href="{{url('testimonial')}}">Testimonial</a></li> 
                    <li><a href="{{url('contact-page')}}">Contact</a></li>
                    <li><a href="{{url('contact-message')}}">contact message</a></li>
                    <li><a href="{{url('about-page')}}"> about us</a></li>
                    <li><a href="{{url('custom-links')}}"> custom links</a></li>
                </ul>
            </li>
            {{--End start frontEnd Settings --}}
 

 
            @endif

            @if(Auth::user()->role_id==2) 
            <li>
                <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">

                    <span class="flaticon-speedometer"></span>
                    dashboard 
                </a>
            </li> 
            @endif 

            {{-- only pro user can see this menu  --}}
            @if(Auth::user()->role_id==3)
              @if(in_array(1, $modules)) 
                @if(in_array(1, $module_links)) 

            <li>
                <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">

                    <span class="flaticon-speedometer"></span>
                    dashboard 
                </a>
            </li> 
             @endif  
            <li>
                <a href="#subMenuHumanResource" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Human Resource
                </a>
                <ul class="collapse list-unstyled" id="subMenuHumanResource">  
                @if(in_array(2, $module_links))      
                    <li>
                        <a href="{{url('add-customer')}}"> Add Customer</a>
                    </li> 
                @endif    
                </ul>
            </li> 
            @endif
          @if(in_array(2, $modules))
            <li>
                <a href="#ServicesMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Services
                </a>
                <ul class="collapse list-unstyled" id="ServicesMenu"> 
             
                    @if(in_array(3, $module_links))
                        <li><a href="{{url('create-sla-template')}}">SLA Template</a></li> 
                    @endif 
                    
                      @if(in_array(3, $module_links)) <li><a href="{{url('create-sla')}}">Create SLA</a></li>  @endif 

                      @if(in_array(4, $module_links)) <li><a href="{{url('sla-list')}}"> SLA List</a></li>  @endif   
                   
                     @if(in_array(5, $module_links)) <li><a href="{{url('quotations/create')}}">New Quotations</a></li>  @endif 
                       @if(in_array(6, $module_links)) <li><a href="{{url('quotations')}}">Quotations List</a></li>   @endif 
                </ul>
            </li> 
             @endif

             @if(in_array(3, $modules))
             <li>
                <a href="#NotificationMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Notifications
                </a>
                <ul class="collapse list-unstyled" id="NotificationMenu"> 
               
                  @if(in_array(7, $module_links)) 
                     <li>
                        <a href="{{url('sms-settings')}}">Sms Notification</a>
                    </li>  
                  @endif      
                </ul>
            </li> 
          @endif
            
            @if(in_array(4, $modules))
              <li>
                <a href="#ReportMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Report
                </a>
                <ul class="collapse list-unstyled" id="ReportMenu">  
                    @if(in_array(8, $module_links))
                        <li>
                            <a href="{{url('reports/daily-services-report')}}">Daily Services Report</a>
                        </li> 
                    @endif
                    @if(in_array(9, $module_links))
                     <li>
                        <a href="{{url('reports/services-pending-done')}}">Services Pending/Done</a>
                    </li>  
                    @endif
                    @if(in_array(10, $module_links))
                     <li>
                        <a href="{{url('reports/customer-monthly-services')}}">Montly Services Report</a>
                    </li>  
                    @endif  
                </ul>
            </li> 
            @endif

            @if(in_array(5, $modules))

             <li>
                <a href="#ServicesMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Communication
                </a>
                <ul class="collapse list-unstyled" id="ServicesMenu">  
                    @if(in_array(11, $module_links))


                    <li> 
                        <a href="{{route('user.chats')}}">
                            <span class="ti-user"></span>Chatting
                        </a> 
                    </li> 

                    @endif 

                </ul>
            </li> 
            @endif 


            @endif 
    </ul>
</nav>
