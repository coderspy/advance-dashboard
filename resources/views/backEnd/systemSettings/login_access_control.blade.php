@extends('backEnd.master')
@section('mainContent')
<style type="text/css">
    #selectStaffsDiv, .forStudentWrapper{
        display: none;
    }
    .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background: linear-gradient(90deg, #7c32ff 0%, #c738d8 51%, #7c32ff 100%);
}

input:focus + .slider {
  box-shadow: 0 0 1px linear-gradient(90deg, #7c32ff 0%, #c738d8 51%, #7c32ff 100%);
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Login Permission</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.system_settings')</a>
                <a href="#">Login Permission</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">@lang('lang.select_criteria')</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'login-access-control', 'enctype' => 'multipart/form-data', 'method' => 'POST']) }}
                        
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-lg-6 mb-30">
                                    @if(session()->has('message-success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message-success') }}
                                    </div>
                                    @elseif(session()->has('message-danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('message-danger') }}
                                    </div>
                                    @endif 
                                </div>

                                    <div class="col-lg-12 mb-30">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" id="member_type" >
                                            <option data-display=" @lang('lang.select_role') *" value="">@lang('lang.select_role') *</option>
                                            @foreach($roles as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option> 
                                            @endforeach
                                        </select>
                                        @if ($errors->has('role'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('role') }}</strong>
                                            </span>
                                        @endif 
                                    </div> 



                                    <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">

                                </div>

                                <div class="col-lg-12 mt-20 text-right">
                                    <button type="submit" class="primary-btn small fix-gr-bg">
                                        <span class="ti-search pr-2"></span>
                                        @lang('lang.search')
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div> 
        @if(isset($staffs))
            <div class="row mt-40">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 no-gutters">
                            <div class="main-title">
                                <h3 class="mb-0">Permission List</h3>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID NO</th>
                                        <th>@lang('lang.name')</th>
                                        <th>@lang('lang.role')</th>
                                        <th>@lang('lang.department')</th>
                                        <th>@lang('lang.description')</th>
                                        <th>@lang('lang.mobile')</th>
                                        <th>@lang('lang.email')</th>
                                        <th>Login Permission</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($staffs as $value)
                                    <tr id="{{$value->user_id}}">
                                        <input type="hidden" id="id" value="{{$value->user_id}}">
                                                <input type="hidden" id="role" value="{{$role}}">
                                        <td>{{$value->staff_no}}</td>
                                        <td>{{$value->first_name}}&nbsp;{{$value->last_name}}</td>
                                        <td>{{!empty($value->roles->name)?$value->roles->name:''}}</td>
                                        <td>{{$value->departments !=""?$value->departments->name:""}}</td>
                                        <td>{{$value->designations !=""?$value->designations->title:""}}</td>
                                        <td>{{$value->mobile}}</td>
                                        <td>{{$value->email}}</td>
                                        <td>
                                            <label class="switch">
                                                <input type="checkbox" class="switch-input" {{$value->staff_user->access_status == 0? '':'checked'}}>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif 
    </div> 
</section>



@endsection
