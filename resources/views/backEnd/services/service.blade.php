@extends('backEnd.master')
@section('mainContent')
@php $settings =\App\SmGeneralSettings::find(1); @endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> Services</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Services</a>
                <a href="#">Services List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        @if(isset($editData)) 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('service-list')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div> 
        @endif

        <div class="row"> 
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    Edit
                                @else
                                    Add
                                @endif
                                    Service
                            </h3>
                        </div>

                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'service-list-update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <input type="hidden" name="id" value="{{$editData->id}}">
                        @else 
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'service-list-store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }} 
                        @endif
                       
                        <div class="white-box">
                            <div class="add-visitor"> 
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id" id="">
                                                <option data-display="Select Category *" value="">Select Category *</option>
                                                @foreach($category_list as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('category_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" name="name" autocomplete="off" value="{{isset($editData)? $editData->name:old('name')}}">
                                            <label>Service Name<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                    <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                          <label>Select Price<span>*</span></label> <br> 
                                            @foreach($prices as $row)
                                            <div class="">
                                                @if(isset($price_ids))
                                                <input type="checkbox" id="row{{$row->id}}" class="common-checkbox form-control{{ $errors->has('row') ? ' is-invalid' : '' }}"  name="prices[]" value="{{$row->id}}" {{in_array($row->id, $price_ids)? 'checked': ''}}>
                                                <label for="row{{$row->id}}"> {{$row->title}} - {{@$row->price}}</label>
                                                @else
                                                
                                                <input type="checkbox" id="row{{$row->id}}" class="common-checkbox form-control{{ $errors->has('row') ? ' is-invalid' : '' }}" name="prices[]" value="{{$row->id}}"> 
                                                <label for="row{{$row->id}}">{{@$row->title}} - {{@$row->price}}</label> 
                                                @endif
                                            </div>
                                            @endforeach 
                                        </div>
                                        
                                    </div>
                                </div>
                                 <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                          <label>Select Required Papers<span>*</span></label> <br>
                                          
                                          @foreach($papers as $row2)
                                          <div class="">
                                              @if(isset($price_ids))
                                                <input type="checkbox" id="row2{{$row2->id}}" class="common-checkbox form-control{{ $errors->has('row2') ? ' is-invalid' : '' }}"  name="papers[]" value="{{$row2->id}}" {{in_array($row2->id, $paper_ids)? 'checked': ''}}>
                                                <label for="row2{{$row2->id}}"> {{$row2->title}}</label>
                                              @else 
                                                <input type="checkbox" id="row2{{$row2->id}}" class="common-checkbox form-control{{ $errors->has('row2') ? ' is-invalid' : '' }}" name="papers[]" value="{{$row2->id}}"> 
                                                <label for="row2{{$row2->id}}">{{@$row2->title}} </label> 
                                              @endif
                                          </div>
                                          @endforeach   
                                        </div>
                                    </div>
                                </div>
                                  
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="">
                                            <span class="ti-check"></span>
                                            {{isset($editData)? 'update':'save'}} Service
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Service List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Service Category</th>
                                    <th>Service Name</th>
                                    <th>Price ({{@$settings->currency_symbol}})</th>
                                    <th>Papers</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $count=1; @endphp 
                                @foreach($data as $editData)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{@$editData->getCategoryName->name}}</td>
                                    <td>{{@$editData->name}}</td>
                                    <td>
                                        @php 
                                            $prices= DB::table('service_assign_prices')
                                            ->join('service_prices', 'service_prices.id', '=', 'service_assign_prices.service_price_id')
                                            ->where('service_assign_prices.service_id',$editData->id)->get();
                                            $total_price = 0;
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.' ['.$price->price .']<br>'; 
                                                $total_price+=$price->price;
                                                $count++;
                                            }

                                        @endphp
                                        <hr>
                                      <b>Total Price {{@$settings->currency_symbol}} {{number_format((float)$total_price, 2, '.', '')}}</b>
                                    </td>
                                    <td>
                                        
                                        @php 
                                            $prices= DB::table('service_assign_papers')
                                            ->join('papers', 'papers.id', '=', 'service_assign_papers.service_paper_id')
                                            ->where('service_assign_papers.service_id',$editData->id)->get();
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.'<br>'; 
                                                $count++;
                                            }

                                        @endphp
                                        </td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                Select
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                               
                                               <a class="dropdown-item" href="{{url('service-list-edit', [$editData->id])}}">Edit</a>
                                             
                                               <a class="dropdown-item" data-toggle="modal" data-target="#DeleteService{{$editData->id}}"
                                                    href="#">Delete </a>
                                            
                                                </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteService{{$editData->id}}" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete  Item</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to delete ? </h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
                                                     {{ Form::open(['url' => 'service-list-delete/'.$editData->id, 'method' => 'GET', 'enctype' => 'multipart/form-data']) }}
                                                    <button class="primary-btn fix-gr-bg" type="submit">DELETE</button>
                                                     {{ Form::close() }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
