@extends('backEnd.master')
@section('mainContent')
@php $settings =\App\SmGeneralSettings::find(1); @endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> Services</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Services</a>
                <a href="#">Services List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        @if(isset($editData)) 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('service-list')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div> 
        @endif

        <div class="row">

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Service List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Service Category</th>
                                    <th>Service Name</th>
                                    <th>Price ({{@$settings->currency_symbol}})</th>
                                    <th>Papers</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $count=1; @endphp 
                                @foreach($data as $editData)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{@$editData->getCategoryName->name}}</td>
                                    <td>{{@$editData->name}}</td>
                                    <td>
                                        @php 
                                            $prices= DB::table('service_assign_prices')
                                            ->join('service_prices', 'service_prices.id', '=', 'service_assign_prices.service_price_id')
                                            ->where('service_assign_prices.service_id',$editData->id)->get();
                                            $total_price = 0;
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.' ['.$price->price .']<br>'; 
                                                $total_price+=$price->price;
                                                $count++;
                                            }

                                        @endphp
                                        <hr>
                                      <b>Total Price {{@$settings->currency_symbol}} {{number_format((float)$total_price, 2, '.', '')}}</b>
                                    </td>
                                    <td>
                                        
                                        @php 
                                            $prices= DB::table('service_assign_papers')
                                            ->join('papers', 'papers.id', '=', 'service_assign_papers.service_paper_id')
                                            ->where('service_assign_papers.service_id',$editData->id)->get();
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.'<br>'; 
                                                $count++;
                                            }

                                        @endphp
                                        </td>
                                </tr>
                                
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
