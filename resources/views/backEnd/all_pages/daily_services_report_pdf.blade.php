<!DOCTYPE html>
<html lang="en">
<head>
  <title>Todays Customer Services Report</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
 table,th,tr,td{
     font-size: 11px !important;
     padding: 0px !important;
     text-align: center !important;
 }
 
</style>
<body>
 

@php 
    $generalSetting= App\SmGeneralSettings::find(1); 
    if(!empty($generalSetting)){
        $school_name =$generalSetting->school_name;
        $site_title =$generalSetting->site_title;
        $school_code =$generalSetting->school_code;
        $address =$generalSetting->address;
        $phone =$generalSetting->phone; 
    } 
@endphp
<div class="container-fluid"> 
                    
                    <table  cellspacing="0" width="100%">
                        <tr>
                            <td> 
                              
                            </td>
                            <td> 
                                  <img class="logo-img" style="width:50px;height: 50px; " src="{{ url('/')}}/{{$generalSetting->logo }}" alt=""> 
                                <h3 style="font-size:22px !important" class="text-white"> {{isset($school_name)?$school_name:'Office Management ERP'}} </h3> 
                                <p style="font-size:18px !important" class="text-white mb-0"> {{isset($address)?$address:'Office Management'}} </p> 
                                
                          </td>
                        </tr>
                    </table>

                    <hr>
           
                <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                      <thead>
                         
                        <tr>
                            <th>SL</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>SLA Id</th>
                                    <th>Customer Id</th>
                                    <th>Customer</th>   
                                    <th>Services</th>
                                    <th>Total Services Price</th>      
                                    <th>Services Paid Amount</th>      
                                    <th>Payable Amount</th>  
                           
                              </tr>
                            </thead>
                           <tbody>
                                @php $count=1; @endphp 
                                @foreach($documents as $row)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>
                                        @php 
                                         $users= DB::table('users')->where('id',$row->created_by)->first();
                                         echo $users->full_name;
                                        @endphp
                                       
                                    </td>
                                    <td>{{date('jS M, Y', strtotime($row->created_at))}}</td>
                                    <td>{{@$row->id}}</td>
                                    <td>{{@$row->customer_id}}</td>
                                    <td>{{@$row->full_name}}</td>       
                                  
                                    <td>
                                        @php 
                                        $sla_services= DB::table('sla_services')->where('sla_id',$row->id)->get();
                                        foreach($sla_services as $services){
                                            $service_details=DB::table('services')->where('id',$services->service_id)->first();
                                          
                                            echo $service_details->name .'<br>';
    
                                           }
                                        
                                        @endphp      
                                    </td>
                                    <td>{{App\User::NumberToBangladeshiTakaFormat($row->total_amount)}}</td>         
                                    <td>{{App\User::NumberToBangladeshiTakaFormat($row->paid_amount)}}</td>         
                                    <td>{{App\User::NumberToBangladeshiTakaFormat($row->paybale_amount)}}</td>         
                                </tr>
                                @endforeach
                            </tbody>
                </table>
            </div>  
         </body>
        </html>
    

