<!DOCTYPE html>
<html lang="en">
<head>
  <title>Services List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
 table,th,tr,td{
     font-size: 11px !important;
     padding: 0px !important;
     text-align: center !important;
 }
 
</style>
<body>
 

@php 
    $generalSetting= App\SmGeneralSettings::find(1); 
    if(!empty($generalSetting)){
        $school_name =$generalSetting->school_name;
        $site_title =$generalSetting->site_title;
        $school_code =$generalSetting->school_code;
        $address =$generalSetting->address;
        $phone =$generalSetting->phone; 
    } 
@endphp
<div class="container-fluid"> 
                    
                    <table  cellspacing="0" width="100%">
                        <tr>
                            <td> 
                              
                            </td>
                            <td> 
                                  <img class="logo-img" style="width:50px;height: 50px; " src="{{ url('/')}}/{{$generalSetting->logo }}" alt=""> 
                                <h3 style="font-size:22px !important" class="text-white"> {{isset($school_name)?$school_name:'Office Management ERP'}} </h3> 
                                <p style="font-size:18px !important" class="text-white mb-0"> {{isset($address)?$address:'Office Management'}} </p> 
                                
                          </td>
                        </tr>
                    </table>

                    <hr>
           
                <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                      <thead>
                         
                        <tr>
                            <th>Sl</th>
                           <th>Service Category</th>
                           <th>Service Name</th>
                           <th>Service Prices</th>
                           <th>Service Papers</th>
                           
                        </tr>
                     </thead>
                       <tbody>
                            @php $count=1; @endphp 
                                @foreach($data as $editData)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{@$editData->getCategoryName->name}}</td>
                                    <td>{{@$editData->name}}</td>
                                    <td>
                                        @php 
                                            $prices= DB::table('service_assign_prices')
                                            ->join('service_prices', 'service_prices.id', '=', 'service_assign_prices.service_price_id')
                                            ->where('service_assign_prices.service_id',$editData->id)->get();
                                            $total_price = 0;
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.' ['.$price->price .']<br>'; 
                                                $total_price+=$price->price;
                                                $count++;
                                            }

                                        @endphp
                                        <hr>
                                      <b>Total Price {{@$settings->currency_symbol}} {{number_format((float)$total_price, 2, '.', '')}}</b>
                                    </td>
                                    <td>   
                                        @php 
                                            $prices= DB::table('service_assign_papers')
                                            ->join('papers', 'papers.id', '=', 'service_assign_papers.service_paper_id')
                                            ->where('service_assign_papers.service_id',$editData->id)->get();
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.'<br>'; 
                                                $count++;
                                            }

                                        @endphp
                                        </td>
                                    <td>
                                  </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>  
         </body>
        </html>
    

