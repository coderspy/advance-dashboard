@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Archive</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Customer</a>
                <a href="#">Archive List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        @if(isset($editData)) 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('service-list')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div> 
        @endif

        <div class="row"> 
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    Edit
                                @else
                                    Add
                                @endif
                                    Archive
                            </h3>
                        </div>

                      @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'Archive-update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <input type="hidden" name="id" value="{{$editData->id}}">
                        @else 
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'customer-archive-store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }} 
                        @endif
              
                     <div class="white-box">


                         @if ($errors->any())
                             @foreach ($errors->all() as $error)
                                 <div>{{$error}}</div>
                             @endforeach
                         @endif

 
                            <div class="add-visitor">

                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                              <select class="niceSelect w-100 bb form-control{{ $errors->has('customer_id') ? ' is-invalid' : '' }}" name="customer_id" id="">
                                                <option data-display="Select Type *" value="">Select Type *</option>
                                              
                                              <option value="{{@$staffs->id}}" selected>{{@$staffs->full_name}}</option> 
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('fees_type') ? ' is-invalid' : '' }}" name="category_id" id="SelectSeviceCategory">
                                            <option data-display="select Service Category" value="">@lang('lang.select') @lang('lang.name')</option>
                                            @foreach($category_list as $value)
                                                 <option value="{{$value->id}}" {{isset($edit)? !empty($edit->id)? $edit->id==$value->id ? 'selected':'':'':''}}>{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="row mt-40">
                                    <div class="col-lg-12"  id="serviceSelecttDiv"> 
                                     
                                        <select class="w-100 bb niceSelect form-control{{ $errors->has('section') ? ' is-invalid' : '' }}" id="serviceSelect" name="service_id">
                                            <option data-display="@lang('lang.select_section') *" value="">@lang('lang.select_section') *</option>
                                        </select>
                                        @if ($errors->has('section'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('section') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>



                                
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control{{ $errors->has('file_title') ? ' is-invalid' : '' }}" type="text" maxlength="200"  name="file_title" autocomplete="off" value="{{isset($editData)? $editData->file_title:old('file_title')}}" required>
                                            <label>Document Title <span>*</span></label> 
                                            <span class="focus-border"></span>
                                            @if ($errors->has('file_title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('file_title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                
                                
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                         
                                        <div class="row no-gutters input-right-icon">
                                            <div class="col">
                                                <div class="input-effect">
                                                    <input class="primary-input" type="text" id="placeholderGuardiansName" placeholder="Upload Documents"  readonly="">
                                                    <span class="focus-border"></span>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button class="primary-btn-small-input" type="button">
                                                    <label class="primary-btn small fix-gr-bg" for="file">@lang('lang.browse')</label>
                                                    <input type="file" class="d-none" name="file" id="file">
                                                </button>
                                            </div>
                                        </div> 
                                    </div>
                                </div>




           
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="">
                                            <span class="ti-check"></span>
                                            {{isset($editData)? 'update':'save'}} Archive
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Archive List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Service Category</th>
                                    <th>Service</th>
                                    <th>File Title</th>
                                    <th>File</th> 
                                    <th>Download</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $count=1; @endphp 
                                @foreach($documents as $row)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{@$row->getCategoryName->name}}</td>
                                    <td>{{@$row->Service->name}}</td>
                                    <td>{{@$row->file_title}}</td>
                                    <td> 
                                    @php 
                                        $file = explode('.',$row->file );
                                    @endphp 
                                        @if($file[1]=="png" || $file[1]== "PNG" || $file[1]=="jpg" || $file[1]== "JPG" || $file[1]=="jpeg" || $file[1]== "JPEG" )
                                        <img src="{{asset($row->file)}}" alt="" class="img img-fluid" style="width:100px;">
                                        @elseif($file[1]=="pdf" || $file[1]== "PDF" || $file[1]=="docx" || $file[1]== "DOCX" || $file[1]=="doc" || $file[1]== "DOC" )
                                   
                                        <img src="{{asset('public/uploads/file/file.png')}}" alt="" class="img img-fluid" style="width:100px;">
                                        @endif


                                    </td>  
                                    <td>
                                        <a href="{{asset($row->file)}}" class="primary-btn fix-gr-bg small"> Download </a>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                Select
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                               
                                               <a class="dropdown-item" href="{{url('edit-Archive', [$row->id])}}">Edit</a> 
                                             
                                               <a class="dropdown-item" data-toggle="modal" data-target="#DeleteService{{$row->id}}"
                                                    href="#">Delete </a>
                                            
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteService{{$row->id}}" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete  Item</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to delete ? </h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
                                                     {{ Form::open(['url' => 'Archive-delete/'.$row->id, 'method' => 'GET', 'enctype' => 'multipart/form-data']) }}
                                                    <button class="primary-btn fix-gr-bg" type="submit">DELETE</button>
                                                     {{ Form::close() }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
