<!DOCTYPE html>
<html lang="en">
<head>
  <title>User Log</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
 table,th,tr,td{
     font-size: 11px !important;
     padding: 0px !important;
     text-align: center !important;
 }
 
</style>
<body>
 

@php 
    $generalSetting= App\SmGeneralSettings::find(1); 
    if(!empty($generalSetting)){
        $school_name =$generalSetting->school_name;
        $site_title =$generalSetting->site_title;
        $school_code =$generalSetting->school_code;
        $address =$generalSetting->address;
        $phone =$generalSetting->phone; 
    } 
@endphp
<div class="container-fluid"> 
                    
                    <table  cellspacing="0" width="100%">
                        <tr>
                            <td> 
                              
                            </td>
                            <td> 
                                  <img class="logo-img" style="width:50px;height: 50px; " src="{{ url('/')}}/{{$generalSetting->logo }}" alt=""> 
                                <h3 style="font-size:22px !important" class="text-white"> {{isset($school_name)?$school_name:'Office Management ERP'}} </h3> 
                                <p style="font-size:18px !important" class="text-white mb-0"> {{isset($address)?$address:'Office Management'}} </p> 
                                
                          </td>
                        </tr>
                    </table>

                    <hr>
           
                <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                      <thead>
                         
                        <tr>
                            <th>User</th>
                            <th>Role</th>
                            <th>Ip Address</th>
                            <th>Mac Address</th>
                            <th>Login Time</th>
                            <th>User Agent</th>
                           
                        </tr>
                     </thead>
                      <tbody>
                        @foreach($todays_log as $user_log)
                           
                        <tr>
                            <td>{{$user_log->user!=""?$user_log->user->username:""}}</td>
                            <td>{{$user_log->role!=""?$user_log->role->name:""}}</td>
                            <td>{{$user_log->ip_address}}</td>
                            <td>{{$user_log->mac_address}}</td>
                            <td  data-sort="{{strtotime($user_log->created_at)}}" >
                                {{$user_log->created_at != ""? App\SmGeneralSettings::DateConvater($user_log->created_at):''}}

                            </td>
                            <td>{{$user_log->user_agent}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>  
         </body>
        </html>
    

