<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pro Daily Services</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
 table,th,tr,td{
     font-size: 11px !important;
     padding: 0px !important;
     text-align: center !important;
 }
 
</style>
<body>
 
@php 
    $generalSetting= App\SmGeneralSettings::find(1); 
    if(!empty($generalSetting)){
        $school_name =$generalSetting->school_name;
        $site_title =$generalSetting->site_title;
        $school_code =$generalSetting->school_code;
        $address =$generalSetting->address;
        $phone =$generalSetting->phone; 
    } 
@endphp
               <div class="container-fluid">               
                    <table  cellspacing="0" width="100%">
                        <tr>
                            <td> 
                              
                            </td>
                            <td> 
                              <img class="logo-img" style="width:50px;height: 50px; " src="{{ url('/')}}/{{$generalSetting->logo }}" alt=""> 
                            <h3 style="font-size:22px !important" class="text-white"> {{isset($school_name)?$school_name:'Office Management ERP'}} </h3> 
                            <p style="font-size:18px !important" class="text-white mb-0"> {{isset($address)?$address:'Office Management'}} </p> 
                            
                          </td>
                        </tr>
                    </table>
                    <hr>
                <table class="table table-bordered table-striped" cellspacing="0" width="100%">
                      <thead>
                         
                        <tr>
                            <th>Sl </th>
                            <th>Pro Id</th>
                            <th>Pro user</th>
                            <th>Date</th>
                            <th>Services Approved</th>
                            <th>Services Pending</th>     
                        </tr>
                       </thead>
                        <tbody>
                                @php $count =1; @endphp
                                @foreach($pro_daily_services as $value)
                                <tr>
                                    <td >{{$count++}}</td>
                                    <td>
                                         @php
                                            $pro= DB::table('sm_staffs')->where('user_id',$value->created_by)->first();
                                            echo $pro->staff_no;
                                          @endphp
                                    </td>
                                    <td >
                                       @php
                                            $pro= DB::table('sm_staffs')->where('user_id',$value->created_by)->first();
                                            echo $pro->full_name;
                                          @endphp

                                    </td>
                                    <td >{{date('jS M, Y', strtotime($value->date))}}</td>
 
                                    <td> 
                                        @if($value->is_approved==1) 
                                        <a style="color: darkgreen;">Approved</a>
                                        @else
                                         <a style="color: blue;">Null</a>
                                        @endif
                                    
                                    </td> 
                                      <td> 
                                        @if($value->is_approved==0) 
                                        <a style="color:red ;">Pending</a>
                                         @else
                                         <p style="color: blue;">Null</p>
                                        @endif
                                    
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                      </div>  
                   </body>
                  </html>
    

