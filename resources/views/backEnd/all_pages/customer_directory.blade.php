@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Customer Directory</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Human Resource</a>
                <a href="#">Customer Directory</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">Select Criteria </h3>
                </div>
            </div> 

            <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg">
                <a href="{{url('add-customer')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                   Add Customer 
                </a>
            </div> 
        </div>
        <div class="row">
            <div class="col-lg-12"> 
              @if(session()->has('message-success'))
                  <div class="alert alert-success">
                  {{ session()->get('message-success') }}
                  </div>
                  @elseif(session()->has('message-danger'))
                  <div class="alert alert-danger">
                      {{ session()->get('message-danger') }}
                  </div>
              @endif
              </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'search-customer', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <div class="row">    
                            <div class="col-lg-4 mt-30-md">
                               <div class="col-lg-12">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" placeholder="Search by Customer id" name="staff_no">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                           </div>
                            <div class="col-lg-4 mt-30-md">
                               <div class="col-lg-12">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" placeholder="Search by Name" name="staff_name">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                           </div>
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg">
                                <span class="ti-search pr-2"></span>
                                Search
                            </button>
                        </div>
                    </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>
 <div class="row mt-40">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Customer List</h3>
                    </div>
                </div>
            </div>

         <div class="row">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Customer ID</th>
                                <th>image</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Archive</th>
                                <th>action</th>
                            </tr>
                        </thead>

                        <tbody>
                             @php $count=1; @endphp 
                            @foreach($staffs as $value)
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$value->staff_no}}</td>
                                <td><img src="{{asset($value->staff_photo)}}" alt="profile image" class="img Img-50 img-thumbnail"></td> 
                                <td>{{$value->first_name}}&nbsp;{{$value->last_name}}</td>
                                <td>{{$value->mobile}}</td>
                                <td>{{$value->email}}</td>
                                <td>
                                <a href="{{url('/customer/archive',$value->id)}}" class="primary-btn small fix-gr-bg"> Archive </a>
                                </td>

                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                           Select
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="{{url('view-customer', $value->id)}}">view</a>
                                          
                                            <a class="dropdown-item" href="{{url('edit-customer', $value->id)}}">edit</a>
                                           
                                            <a class="dropdown-item modalLink" title="Delete Staff" data-modal-size="modal-md" href="{{url('delete-Customer', $value->id)}}">delete</a>
                                            
                                       
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
