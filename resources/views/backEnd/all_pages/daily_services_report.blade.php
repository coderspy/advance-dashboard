@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Services</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Customer</a>
                <a href="#">Todays Customers Services</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
       
        <div class="row">  
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Customers Services</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                     <div class="pull-right" style="margin-left: 22px; margin-top: 7px;">
                <a href="{{url('/daily-services-report-pdf')}}" class="btn btn-info">Pdf</a>
                <a class="btn btn-info" href="{{ URL::previous() }}">back</a>
               </div>
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>SLA Id</th>
                                    <th>Customer Id</th>
                                    <th>Customer</th>   
                                    <th>Services</th>
                                    <th>Total Services Price</th> 
                                     <th>Payable Amount</th>      
                                    <th>Services Paid Amount</th>      
                                        
                                </tr>
                            </thead>

                            <tbody>
                                @php $count=1; @endphp 
                                @foreach($documents as $row)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>
                                        @php 
                                         $users= DB::table('users')->where('id',$row->created_by)->first();
                                         echo $users->full_name;
                                        @endphp
                                       
                                    </td>
                                    <td>{{date('jS M, Y', strtotime($row->created_at))}}</td>
                                    <td>{{@$row->id}}</td>
                                    <td>{{@$row->customer_id}}</td>
                                    <td>{{@$row->full_name}}</td>       
                                  
                                    <td>
                                        @php 
                                        $sla_services= DB::table('sla_services')->where('sla_id',$row->id)->get();
                                        foreach($sla_services as $services){
                                            $service_details=DB::table('services')->where('id',$services->service_id)->first();
                                          
                                            echo $service_details->name .'<br>';
    
                                           }
                                        
                                        @endphp      
                                    </td>
                                    <td>{{App\User::NumberToBangladeshiTakaFormat($row->total_amount)}}</td>
                                    <td>{{App\User::NumberToBangladeshiTakaFormat($row->paybale_amount)}}</td>           
                                    <td>{{App\User::NumberToBangladeshiTakaFormat($row->paid_amount)}}</td>         
                                          
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
