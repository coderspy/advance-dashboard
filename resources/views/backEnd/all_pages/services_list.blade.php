@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Services list</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Reports</a>
                <a href="#">Services list</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12"> 
              @if(session()->has('message-success'))
                  <div class="alert alert-success">
                  {{ session()->get('message-success') }}
                  </div>
                  @elseif(session()->has('message-danger'))
                  <div class="alert alert-danger">
                      {{ session()->get('message-danger') }}
                  </div>
              @endif
              </div>
        </div>
 
 <div class="row mt-40">
        <div class="col-lg-12">
            <div class="row">
                        <div class="col-lg-6 no-gutters">
                            <div class="main-title">
                                <h3 class="mb-0">Services List</h3>
                            </div>
                        </div>
                    </div>

         <div class="row">
             <div class="pull-right" style="margin-left: 22px; margin-top: 7px;">
                <a href="{{url('/services-list-pdf/')}}" class="btn btn-info">Pdf</a>
                <a class="btn btn-info" href="{{ URL::previous() }}">back</a>
            </div>
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                 <th>Sl</th>
                                 <th>Service Category</th>
                                 <th>Service Name</th>
                                 <th>Service Prices</th>
                                 <th>Service Papers</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $count=1; @endphp 
                                @foreach($data as $editData)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{@$editData->getCategoryName->name}}</td>
                                    <td>{{@$editData->name}}</td>
                                    <td>
                                        @php 
                                            $prices= DB::table('service_assign_prices')
                                            ->join('service_prices', 'service_prices.id', '=', 'service_assign_prices.service_price_id')
                                            ->where('service_assign_prices.service_id',$editData->id)->get();
                                            $total_price = 0;
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.' ['.$price->price .']<br>'; 
                                                $total_price+=$price->price;
                                                $count++;
                                            }

                                        @endphp
                                        <hr>
                                      <b>Total Price {{@$settings->currency_symbol}} {{number_format((float)$total_price, 2, '.', '')}}</b>
                                    </td>
                                    <td>   
                                        @php 
                                            $prices= DB::table('service_assign_papers')
                                            ->join('papers', 'papers.id', '=', 'service_assign_papers.service_paper_id')
                                            ->where('service_assign_papers.service_id',$editData->id)->get();
                                            $count=1;
                                            foreach($prices as $price){
                                                echo $count.'. '.$price->title.'<br>'; 
                                                $count++;
                                            }

                                        @endphp
                                        </td>
                                    <td>
                                  </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
