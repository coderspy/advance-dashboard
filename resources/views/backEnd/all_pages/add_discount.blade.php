@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Discount</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Discount</a>
                <a href="#">Discount List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        @if(isset($editData)) 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('service-list')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div> 
        @endif

        <div class="row"> 
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    edit
                                @else
                                    add
                                @endif
                                    Discount
                            </h3>
                        </div>

                      @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'discount-update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <input type="hidden" name="id" value="{{$editData->id}}">
                        @else 
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'store-discount', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }} 
                        @endif
              
                     <div class="white-box">
                            <div class="add-visitor">
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" type="text" name="title" autocomplete="off" value="{{isset($editData)? $editData->title:old('title')}}">
                                            <label>Title Name<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                              <select class="niceSelect w-100 bb form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="">
                                                <option data-display="Select Type *" value="">Select Type *</option>
                                               
                                                <option {{ isset($editData)? @$editData->type == 'P'? 'selected': '':''}} value="P">Percentage</option>
                                                <option {{ isset($editData)? @$editData->type == 'F'? 'selected': '':''}} value="F">Fixed amount</option>
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" type="text" name="note" autocomplete="off" value="{{isset($editData)? $editData->note:old('note')}}">
                                            <label>Note<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            @if ($errors->has('note'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('note') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control{{$errors->has('amount') ? ' is-invalid' : '' }}" type="text" name="amount" autocomplete="off" value="{{isset($editData)? $editData->amount:old('amount')}}">
                                            <label>Amount<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            @if ($errors->has('amount'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="no-gutters input-right-icon">
                                            <div class="col-lg-12"> 
                                                <div class="input-effect">
                                                    <input class="primary-input date form-control{{ $errors->has('discount_start_date') ? ' is-invalid' : '' }}" id="discount_start_date" type="text"
                                                    name="discount_start_date" value="{{isset($editData->discount_start_date)? $editData->discount_start_date: date('m/d/Y')}}">
                                                    <span class="focus-border"></span>
                                                    <label>Discount Start Date<span>*</span> </label>
                                                    @if ($errors->has('discount_start_date'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('discount_start_date') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button class="" type="button">
                                                    <i class="ti-calendar" id="discount_start_date"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                         <div class="row mt-40">
                             <div class="col-lg-12">
                                 
                            <div class="no-gutters input-right-icon">
                                <div class="col-lg-12"> 
                                    <div class="input-effect">
                                        <input class="primary-input date form-control{{ $errors->has('discount_end_date') ? ' is-invalid' : '' }}" id="discount_end_date" type="text"
                                        name="discount_end_date" value="{{isset($editData->discount_end_date)? $editData->discount_end_date: date('m/d/Y')}}">
                                        <span class="focus-border"></span>
                                        <label>Discount End Date<span>*</span> </label>
                                        @if ($errors->has('discount_end_date'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('discount_end_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="" type="button">
                                        <i class="ti-calendar" id="discount_end_date"></i>
                                    </button>
                                </div>
                            </div>
                             </div>
                        </div>          
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="">
                                            <span class="ti-check"></span>
                                            {{isset($editData)? 'update':'save'}} Discount
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Discount List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Discount Title</th>
                                    <th>Discount Type</th>
                                    <th>Amount</th>
                                    <th>Note</th>
                                    <th>Discount Start date</th>
                                    <th>Discount end date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php $count=1; @endphp 
                                @foreach($data as $editData)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{$editData->title}}</td>
                                    <td>
                                         @if($editData->type=='F')
                                            Fixed Price
                                               
                                         @elseif($editData->type=='P')
                                            Percentage
                                         @endif
                                   </td>
                                    <td>{{number_format(@$editData->amount,2,'.', '')}}</td>
                                    <td>{{$editData->note}}</td>
                                    <td>{{$editData->discount_start_date}}</td>
                                    <td>{{$editData->discount_end_date}}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                Select
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                               
                                               <a class="dropdown-item" href="{{url('edit-discount', [$editData->id])}}">Edit</a>
                                             
                                               <a class="dropdown-item" data-toggle="modal" data-target="#DeleteService{{$editData->id}}"
                                                    href="#">Delete </a>
                                            
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteService{{$editData->id}}" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete  Item</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to delete ? </h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
                                                     {{ Form::open(['url' => 'discount-delete/'.$editData->id, 'method' => 'GET', 'enctype' => 'multipart/form-data']) }}
                                                    <button class="primary-btn fix-gr-bg" type="submit">DELETE</button>
                                                     {{ Form::close() }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
