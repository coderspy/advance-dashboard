@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Customer Archive</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">Dashboard</a>
                <a href="#">Customer</a>
                <a href="#">Customer Archive Document</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        @if(isset($editData)) 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('add-customer')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div> 
        @endif

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Customer Archive List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Customer No</th>
                                    <th>Customer Name</th>
                                    <th>Service Category</th>
                                    <th>Service Name</th>
                                    <th>Document Title</th> 
                                    <th>File Download</th> 
                                   
                                </tr>
                            </thead>

                            <tbody>ArchiveDocuments
                                @php $count=1; @endphp 
                                @foreach($documents as $row)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{$row->staff_no}}</td>
                                    <td>{{@$row->first_name}}</td>
                                    <td>{{@$row->getCategoryName->name}}</td>
                                    <td>{{@$row->Service->name}}</td>
                                    <td>{{@$row->file_title}}</td>
                                    <td> 
                                    @php 
                                        $file = explode('.',$row->file );
                                    @endphp 
                                        @if($file[1]=="png" || $file[1]== "PNG" || $file[1]=="jpg" || $file[1]== "JPG" || $file[1]=="jpeg" || $file[1]== "JPEG" )
                                        <img src="{{asset($row->file)}}" alt="" class="img img-fluid" style="width:100px;">
                                        @elseif($file[1]=="pdf" || $file[1]== "PDF" || $file[1]=="docx" || $file[1]== "DOCX" || $file[1]=="doc" || $file[1]== "DOC" )
                                   
                                        <img src="{{asset('public/uploads/file/file.png')}}" alt="" class="img img-fluid" style="width:100px;">
                                        @endif


                                    </td>  
                                    <td>
                                        <a href="{{asset($row->file)}}" class="primary-btn fix-gr-bg small"> Download </a>
                                    </td>
                                </tr>
                           
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
