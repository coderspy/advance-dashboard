@extends('backEnd.master')
@section('mainContent') 

<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Reports/Pro Daily Services</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="{{url('quotations')}}" class="active">Pro Daily Services</a> 
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
                      <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'search-services-pending-done', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <div class="row"> 

                  <div class="col-lg-4">
                        <div class="no-gutters input-right-icon">
                            <div class="col">
                                <div class="input-effect">
                                    <input class="primary-input date form-control{{ $errors->has('from_date') ? ' is-invalid' : '' }}" id="from_date" type="text"
                                     name="from_date" value="{{date('m/d/Y')}}">
                                    <span class="focus-border"></span>
                                    <label>From Date<span style="color:red;">*</span> </label>
                                    @if ($errors->has('from_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('from_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-auto">
                                <button class="" type="button">
                                    <i class="ti-calendar" id="from_date"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="no-gutters input-right-icon">
                            <div class="col">
                                <div class="input-effect">
                                    <input class="primary-input date form-control{{ $errors->has('to_date') ? ' is-invalid' : '' }}" id="to_date" type="text"
                                     name="to_date" value="{{date('m/d/Y')}}">
                                    <span class="focus-border"></span>
                                    <label>To Date<span style="color:red;">*</span> </label>
                                    @if ($errors->has('to_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('to_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-auto">
                                <button class="" type="button">
                                    <i class="ti-calendar" id="to_date"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg">
                                <span class="ti-search pr-2"></span>
                                Search
                            </button>
                        </div>
                    </div>
            {{ Form::close() }}
            </div>
        </div>
    </div>
        <div class="row">
         <div class="col-lg-12">
              {{--   <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Pro Daily Services</h3>
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-lg-12">
                    <div class="" style="margin-left: 22px; margin-top: 7px;">
                    <a href="{{url('/services-pending-done-pdf/')}}" class="btn btn-info">Pdf</a>
                    <a class="btn btn-info" href="{{ URL::previous() }}">back</a>
                     </div>
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                               @if(session()->has('message-success') != "" ||
                                session()->get('message-danger') != "")
                                <tr>
                                    <td colspan="11">
                                         @if(session()->has('message-success'))
                                          <div class="alert alert-success">
                                              {{ session()->get('message-success') }}
                                          </div>
                                        @elseif(session()->has('message-danger'))
                                          <div class="alert alert-danger">
                                              {{ session()->get('message-danger') }}
                                          </div>
                                        @endif
                                    </td>
                                </tr>
                                 @endif 
                                <tr>
                                    <th>Sl </th>
                                    <th>Pro Id</th>
                                    <th>Pro user</th>
                                    <th>Date</th>
                                    <th>Services Approved</th>
                                    <th>Services Pending</th>
                                      
                                </tr>
                            </thead>
                            <tbody>
                                @php $count =1; @endphp
                                @foreach($pro_daily_services as $value)
                                <tr>
                                    <td >{{$count++}}</td>
                                    <td>
                                         @php
                                            $pro= DB::table('sm_staffs')->where('user_id',$value->created_by)->first();
                                            echo $pro->staff_no;
                                          @endphp
                                    </td>
                                    <td >
                                       @php
                                            $pro= DB::table('sm_staffs')->where('user_id',$value->created_by)->first();
                                            echo $pro->full_name;
                                          @endphp

                                    </td>
                                    <td >{{date('jS M, Y', strtotime($value->date))}}</td>
 
                                    <td> 
                                        @if($value->is_approved == 1) 
                                        <a style="color: darkgreen;">Approved</a>
                                        @else
                                         <a style="color: blue;">Null</a>
                                        @endif
                                    
                                    </td> 
                                      <td> 
                                        @if($value->is_approved == 0) 
                                        <a style="color:red ;">Pending</a>
                                         @else
                                         <p style="color: blue;">Null</p>
                                        @endif
                                    
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
