@extends('backEnd.master')
@section('mainContent') 
<link rel="stylesheet" href="{{asset('public/backEnd/css/sla.css')}}">
@php
 
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; }
    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo;
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
@endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Edit quotation</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">quotation</a>
                <a href="#" class="active">Edit quotation</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($edit))
                                   Edit
                                @else
                                   Add
                                @endif
                                  quotation
                            </h3>
                        </div>
                  
                        {{ Form::open(['class' => 'form-horizontal', 'files' => 'true', 'url' => 'quotations/update', 'method' => 'POST', 'id'=>'tender-create-form','enctype' => 'multipart/form-data' ]) }}                   
                       @csrf
                    <input type="text" hidden name="id" value="{{ @$edit->id }}">
                        <div class="white-box">   

                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                <p class="text-danger">{{$error}}</p>
                                @endforeach
                            @endif

                            {{-- hoyese --}}

                            <div class="add-visitor">
                                <div class="row ">
                                    <div class="col-lg-3">
                                        <div class="invoice-details-left">
                                            <div class="mb-20">  <img src="{{asset($logo)}}" class="tender-create-logo w-25" > </div>
                                            <div class="business-info">
                                                <h3>{{$generalSetting->company_name}}</h3>
                                                <p class="textWrap">{{$generalSetting->address}}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="row">
        
                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('days') ? ' is-invalid' : '' }}" type="number" min="1" max="365" name="days" autocomplete="off" id="days"
                                                    value="{{isset($edit)? !empty($edit->days)? $edit->days : old('days'):''}}">
                                                    <label>Service Durration Days<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('days'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('days') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div> 

                                            {{-- <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" type="text" name="title" autocomplete="off" id="title" value="{{isset($edit)? !empty($edit->title)? $edit->title : old('title'): old('title')}}">
                                                    <label>SLA Title<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('title'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('title') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" type="text" name="number" autocomplete="off" id="number" value="{{isset($edit)? !empty($edit->number)? $edit->number : old('number'): Auth::user()->id.''.time()}}">
                                                    <label>Quotation Number<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="input-effect">
                                                            @php
                                                            $value = date('m/d/Y');
                                                            if(isset($edit) && !empty($edit->date) ){  $value = date('m/d/Y', strtotime($edit->date));   }
                                                            else{ if(!empty(old('date'))){ $value = old('date');   }else{  $value = date('m/d/Y');   } }
                                                            @endphp
                                                            <input class="primary-input date" id="date" type="text" name="date" value="{{$value}}">
                                                            <label>Quotation date</label>
                                                            <span class="focus-border"></span>
                                                            @if ($errors->has('date'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('date') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="" type="button">
                                                            <i class="ti-calendar" id="end-date-icon"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('reference') ? ' is-invalid' : '' }}" type="text" name="reference" autocomplete="off"     value="{{isset($edit)? !empty($edit->reference)? $edit->reference : old('reference'): old('reference')}}" id="reference">
                                                    <label>@lang('lang.reference') <span></span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('reference'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('reference') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            {{-- <div class="col-lg-3 mt-20 mb-10">
                                                <select class="niceSelect w-100 bb form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" name="category" id="SelectSeviceCategory">
                                                    <option data-display="Select Category *" value="">Select Category *</option>
                                                    @foreach($ServiceCategory as $value)
                                                         <option value="{{$value->id}}" {{isset($edit)? !empty($edit->id)? $edit->id==$value->id ? 'selected':'':'':''}}>{{$value->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category'))
                                                <span class="invalid-feedback invalid-select" role="alert">
                                                    <strong>{{ $errors->first('category') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            
                                            <div class="col-lg-3  mt-20 mb-10" id="serviceSelecttDiv">
                                                <select class="w-100 bb niceSelect form-control{{ $errors->has('service') ? ' is-invalid' : '' }}" id="serviceSelect" name="service">
                                                    <option data-display="Select Service *" value="">Select Service *</option>
                                                </select>
                                                @if ($errors->has('service'))
                                                <span class="invalid-feedback invalid-select" role="alert">
                                                    <strong>{{ $errors->first('service') }}</strong>
                                                </span>
                                                @endif
                                            </div> --}}
                                            
                                            {{-- Select --}}
                                            <div class="col-lg-8 mt-20 mb-10">
                                                <div class="col-lg-12 d-flex mt-10">
                                                   
                                                <input type="text" name="customer" hidden value="{{ @$customer_info->id }}">
                                                <table class="">
                                                    <tr>
                                                        <td style="width:30%">Name</td>
                                                        <td>{{@$customer_info->full_name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td>{{@$customer_info->email}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Phone</td>
                                                        <td>{{@$customer_info->mobile}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Passport Number</td>
                                                        <td>{{@$customer_info->passport_number}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>NID Number</td>
                                                        <td>{{@$customer_info->nid_number}}</td>
                                                    </tr>
                                                </table>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-3 mt-20 mb-10 d-none">
                                                <div class="col-lg-12 d-flex mt-10">
                                                    <p class="text-uppercase fw-500 mb-10">Type *</p>
                                                    <div class="d-flex radio-btn-flex ml-40">
                                                        <div class="mr-30">
                                                            <input type="radio" name="customer_type" id="relationFather" value="new" class="common-radio relationButton newc" {{old('customer_type') == "new"? 'checked': ''}}>
                                                            <label for="relationFather">New</label>
                                                        </div>
                                                        <div class="mr-30">
                                                            <input type="radio" name="customer_type" id="relationMother" value="old" class="common-radio relationButton oldc" {{old('customer_type') == "old"? 'checked': ''}}>
                                                            <label for="relationMother">Old</label>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            {{-- customer --}}
                                            <div class="exCustomer row" style="display:none">
                                                <div class="col-lg-4 mt-20 mb-10 ">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('customer') ? ' is-invalid' : '' }}" name="customer_id" id="SelectCustomerID">
                                                        <option data-display="@lang('lang.select') @lang('lang.customer') *" value="" >@lang('lang.select') @lang('lang.customer') *</option>
                                                        @foreach($customers as $value)
                                                             <option value="{{$value->id}}"  {{isset($edit)? !empty($edit->customer_id)? $edit->customer_id==$value->id ? 'selected':'':'':''}} >
                                                                {{$value->full_name}}
                                                                - [{{@$value->nid_number}}]
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('customer_id'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('customer') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>  
                                                {{-- end customer --}}
                                                <div class="col-lg-8"> 
                                                    <div class="paper_list" id="paper_list">
                                                        <h4 class="mt-20" id="avaliable_papers"></h4>
                                                        <ul>    
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                                                          
                                        <div class="row" id="newCustomer" style="display:none">
                                            <div class="col-lg-12 mt-10">
                                                <h4 class="text-center">New Customer </h4>
                                            </div>
                                            
 
                                            <div class="col-lg-4 mt-20">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control {{$errors->has('first_name') ? 'is-invalid' : ' '}}" type="text"  name="first_name" value="{{old('first_name')}}">
                                                    <span class="focus-border"></span>
                                                    <label>First Name <span>*</span> </label>
                                                    @if ($errors->has('first_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div> 
                                            <div class="col-lg-4 mt-20">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" type="text"  name="last_name" value="{{old('last_name')}}">
                                                    <span class="focus-border"></span>
                                                    <label>Last Name <span>*</span> </label>
                                                    @if ($errors->has('last_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email"  name="email" value="{{old('email')}}">
                                                    <span class="focus-border"></span>
                                                    <label>Email <span>*</span> </label>
                                                    @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" type="number"  name="mobile" value="{{old('mobile')}}">
                                                    <span class="focus-border"></span>
                                                    <label>Mobile <span>*</span> </label>
                                                    @if ($errors->has('mobile'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('mobile') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('passport_number') ? ' is-invalid' : '' }}" type="text"  name="passport_number" value="{{old('passport_number')}}">
                                                    <span class="focus-border"></span>
                                                    <label>Passport Number <span>*</span> </label>
                                                    @if ($errors->has('passport_number'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('passport_number') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('nid_number') ? ' is-invalid' : '' }}" type="text"  name="nid_number" value="{{old('nid_number')}}">
                                                    <span class="focus-border"></span>
                                                    <label>NID Number <span>*</span> </label>
                                                    @if ($errors->has('nid_number'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('nid_number') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>
 
                               {{--  <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="4" name="description">{{isset($edit)? !empty($edit->description)?$edit->description:'':old('description')}}</textarea>
                                                <label>@lang('lang.description') <span></span></label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div> --}}
 

                             
                                <!-- ***************** equipment-table ************************ ******** -->
                                <div class="equipment comon-status row mt-25 d-block">
                                    <div class="col-lg-12 text-right">
                                        <button type="button" class="primary-btn small fix-gr-bg" id="addRowEquipment">
                                            <span class="ti-plus pr-2"></span> Add New Service
                                        </button>
                                    </div>
                                    <input type="hidden" id="add_product_counter" >
                                    <table class="display school-table school-table-style without-box-shadow" cellspacing="0" width="100%" id="equipment-table">
                                        <thead>
                                            <tr>
                                                <th class="item_name">Service Name</th>   
                                                <th>Papers</th>
                                                <th>Price ({{$currency_symbol}})</th>
                                                <th>Subtotal  ({{$currency_symbol}})</th>
                                                <th>Discount</th>
                                                <th>Total ({{$currency_symbol}})</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            @foreach ($edit->quotationProducts as $key => $single_service)
                                            <tr>
                                                <td>
                                                    <div class="input-effect">
                                                        <select class="niceSelect w-100 bb form-control" name="Eproducts[]" id="Ereceived_product">
                                                            <option data-display="Select Service *" value="none">Select Service *</option>
                                                            @foreach($items as $key=>$value)
                                                            <option value="{{$value->id}}" {{isset($single_service)?  $single_service->service_id == $value->id ? 'selected':'':''}}>{{$value->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="focus-border"></span>
                                                </div>
                                                </td>  
                                                <td> 
                                                    <div class="sla_service_papers_list" id="sla_service_papers_list">
                                                        <div class="row no-gutters input-right-icon">
                                                            <div class="col">
                                                                <div class="input-effect">
                                                                    @php
                                                                        $paper = \App\SmQuotationProducts::getPaper($edit->id,$single_service->service_id);
                                                                    @endphp
                                                                    <input type="hidden" class="d-block" name="file_name[]" value="{{ @$paper->paperId}}">
                                                                    <input class="primary-input" type="text" name="file_title[]" id="placeholderGuardiansName" placeholder="{{ @$paper->title }} " value="{{ @$paper->title }}" readonly="">
                                                                    <span class="focus-border"></span>
                                                                </div>
                                                            </div>
                                                             <div class="col-auto">
                                                                <button class="primary-btn-small-input text-align-right" type="button">
                                                                    <label class="primary-btn small fix-gr-bg" for="file{{$key}}">@</label>
                                                                    <input type="file" class="d-none" name="file[]" value="{{ @$paper->title }}" id="file{{$key}}">
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td> 
                                                    <div class="sla_service_price_list text-right" id="sla_service_price_list">
                                                        @php
                                                                $price = \App\SlaServices::getPrice($single_service->service_id);
                                                        @endphp
                                                        <h4> {{ @$price->title }} {{ @$price->price }}</h4>
                                                    </div>
                                                </td> 
                                                <td>
                                                    <div class="total_price text-center" id="total_price"><h4 class="text-center" id="setTotalPrice">{{ @$price->price }}</h4></div> 
                                                </td>
                                                <td>
                                                    <div class="input-effect" id="CustomerDiscountDiv">
                                                        <select class="niceSelect w-72 bb form-control" name="discount[]" id="CustomerDiscount">
                                                            <option data-display="Select Discount" value="0">Select Discount</option>

                                                            @if(@$single_service->discount_id)
                                                            @php
                                                              $discount =\App\SlaServices::getDiscount($single_service->service_id,$single_service->discount_id);
                                                            @endphp
                                                             <option value="{{@$single_service->discount_id}}" selected >{{ isset($discount) ? $discount->title : '' }}</option>
                                                             @else

                                                             @foreach ($single_service->product->getDiscountAssign as $item)
                                                                 <option value="{{@$item->getDiscountName->id}}" >{{ isset($item->getDiscountName) ? $item->getDiscountName->title : '' }}</option>
                                                             @endforeach
                                                           @endif
                                                           
                                                        </select>
                                                        <span class="focus-border"></span>
                                                </div>
                                                </td>
                                                <td>
                                                    <input type="text" value="{{ @$price->price }}"  class="service_payable_price" name="service_payable_price[]" id="service_total_price" style="border:none; background-color:transparent">
                                                </td>
                                                <td>
                                                        {{-- x --}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr> 
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><input class="primary-input form-control d-none"
                                                        type="number" step="any" id="Etotal" name="Etotal" autocomplete="off" readonly="true" value="0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr> 
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                               
                                                <td>
                                                    {{-- <div class="d-flex radio-btn-flex ml-40">
                                                        <div class="mr-30">
                                                            <input type="radio" name="Ediscount_type" id="ErelationFather" value="P" class="common-radio relationButton">
                                                            <label for="ErelationFather" class="pl-4">%</label>
                                                        </div>
                                                        <div class="mr-30">
                                                            <input type="radio" name="Ediscount_type" id="ErelationMother" value="A" class="common-radio relationButton" checked="checked">
                                                            <label for="ErelationMother" class="pl-4">@lang('lang.fixed')</label>
                                                        </div>
                                                    </div> --}}
                                                </td>
                                            </tr>
                                            <tr> 
                                                
                                                <td></td>
                                                <td colspan="4" style="text-align:right">Grand Total:</td>
                                                <td>
                                                    <input class="primary-input form-control" value="{{ @$price->price }}" type="number" step="any" id="Ebid_amount" name="Ebid_amount" autocomplete="off" readonly="true"> 
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td colspan="4" style="text-align:right"></td>
                                                <td>
                                                    <input class="primary-input form-control " value="{{ @$price->price }}" type="number" step="any" name="paid_amount"  id="paid_amount">
                                                </td>    <td>
                                                    <div class="d-flex radio-btn-flex ml-40 ">
                                                        <div class="mr-30 ">
                                                            <input type="radio" name="pament_type" id="full_paid" value="F" class="common-radio relationButton" >
                                                            <label for="full_paid" class="pl-4">Full</label>
                                                        </div>
                                                        <div class="mr-30 ">
                                                            <input type="radio" name="pament_type" id="Partial" value="P" class="common-radio relationButton">
                                                            <label for="Partial" class="pl-4">Partial</label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tfoot>
                                    </table>

                                </div> 
                                
                                    
                                <div class="row mt-40">
                                    <div class="col-lg-12">
                                        <div class="student-details invoice-student-details">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item text-center">
                                                        <a class="nav-link active" href="#publicNotes" role="tab" data-toggle="tab">@lang('lang.Public') @lang('lang.notes')</a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#privateNotes" role="tab" data-toggle="tab">@lang('lang.Private') @lang('lang.notes')</a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#terms" role="tab" data-toggle="tab">@lang('lang.Terms')</a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#footer" role="tab" data-toggle="tab">@lang('lang.Footer')</a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#signature" role="tab" data-toggle="tab">@lang('lang.signature')</a>
                                                    </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content invoice-tab-content">
                                                <!-- Start Profile Tab -->
                                                <div role="tabpanel" class="tab-pane fade  show active" id="publicNotes">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="public_note"> {{ @$edit->public_note }}</textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="privateNotes">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="private_note"> {{ @$edit->private_note }}</textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="terms">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="terms_note"> {{ @$edit->terms_note}}</textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="footer">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="footer_note"> {{ @$edit->footer_note }}</textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="signature">
                                                    <input type="text" name="signature_person" value="{{ @$edit->signature_person }}" class="primary-input form-control" placeholder="person name">
                                                    <input type="text" name="signature_company" value="{{ @$edit->signature_company }}" class="primary-input form-control" placeholder="company name">
                                                </div>
                                                <!-- End Profile Tab -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-40">
                                    <div class="col-lg-12 text-right">
                                        <button type="submit" class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            @if(isset($edit))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                            Quotation
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    
</script>
@endsection
