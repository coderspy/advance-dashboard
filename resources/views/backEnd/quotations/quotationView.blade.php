@extends('backEnd.master')
@section('mainContent') 
<style>
   table,
   thead,
   th,
   tr,
   td {
       color: #415094 !important;
       -webkit-print-color-adjust: exact;
   }

   @media print {
       body {
           -webkit-print-color-adjust: exact;
       }
   }

   .purchaseInvoice {
       padding: 0px 25px !important;
   }

   .TotalAmount {
       background: linear-gradient(90deg, #7c32ff 0%, #c738d8 51%, #7c32ff 100%);
       padding: 5px 0px;
       color: white;
       letter-spacing: 2px;
       margin-top: 10px;
   }

   .quotation_view_table {
       width: 100%;
   }

   .quotation_view_50 {
       width: 50%;
   }

   .quotation_view_60 {
       width: 50%;
   }

   .quotation_view_table_tr_td {
       width: 50%;
       vertical-align: top;
   }

   .quotation_view_table_tr_td_h2 {
       background: #e9ecef !important;
       padding: 5px;
   }

   .quotation_view_table_tr_img {
       min-width: 160px;
       max-width: 160px;
       height: auto;
   }

   .shipment_qorkorder {
       width: 60%;
       padding-right: 10%;
       vertical-align: top;
   }

   .shipment_qorkorder_td {
       width: 20%;
       vertical-align: top;
   }
   </style>
@php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
   
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; } 

    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo; 
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
@endphp 
<link rel="stylesheet" href="{{ asset('/public/css/quotationView.css') }}">
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.quotation') @lang('lang.details')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.quotation') </a>
                <a href="#">@lang('lang.quotation') @lang('lang.details')</a>
            </div>
        </div>
    </div>
</section>


<section class="admin-visitor-area">
<div class="container-fluid p-0">
    <div class="row">
            <div class="offset-lg-2 col-lg-8">
                <div class="white-box">
                   <div class="row mt-40">
                        <div class="col-lg-12"> 

                            <div class="row" id="purchaseInvoice">
                                <div class="container-fluid">
                                    <div class="row mb-20">
                                        <div class="col-lg-12">
                                            <table class="quotation_view_table"   style="width:100%">
                                                <tr>
                                                    <td class="quotation_view_table_tr_td"> 
                                                        <div class="col-lg-12 ">
                                                            <img src="{{asset($logo)}}"  class="quotation_view_table_tr_img" style="width:200px !important">
                                                            <div class="business-info text-left">
                                                                <h3 class="mt-10 primary-color">{{$generalSetting->company_name}}</h3>
                                                                <p class="mt-0 primary-color" class="quotation_view_50">{{$generalSetting->address}}</p>
                                                                <p class="mt-0 primary-color" class="quotation_view_50">{{date('Y-m-d')}} </p>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    {{-- {{dd($quotation)}} --}}
                                                    <td class="quotation_view_50 p-0" class="primary-color"> 
                                                        <div class="col-lg-12 ">
                                                            <div class="invoice-details-right">
                                                                <h2 class="text-uppercase text-center quotation_view_table_tr_td_h2" >@lang('lang.quotation') @lang('lang.details')</h2>                                                
                                                               {{--  <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">@lang('lang.quotation') @lang('lang.title'):</p>
                                                                    <p class="text-left  primary-color">{{$quotation->title}}</p>
                                                                </div> --}}
                                                                <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">@lang('lang.quotation') @lang('lang.no_'):</p>
                                                                    <p class="text-left  primary-color">{{$quotation->number}}</p>
                                                                </div>
                                                                <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">@lang('lang.quotation') @lang('lang.date'):</p>
                                                                    <p class="text-left  primary-color">{{date('jS M, Y', strtotime($quotation->date))}}</p>
                                                                </div>
                                                                <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">@lang('lang.quotation') @lang('lang.reference')</p>
                                                                    <p class="text-left  primary-color">{{$quotation->reference}}</p>
                                                                </div> 
                                                                <h2 class="text-uppercase text-center TotalAmount" >@lang('lang.total') @lang('lang.amount') {{$currency_symbol}}{{$quotation->paybale_amount}} </h2> 
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>  


                                    <hr>
             

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="quotation_view_table">
                                                <tr>
                                                    <td class="quotation_view_50">
                                                                    
                                                         @if(!empty($quotation->customer ))  
                                                        <div class="col-lg-12 ">
                                                            <div class=" primary-color">
                                                                <h5 class="primary-color">@lang('lang.Bill_To'):</h5>
                                                            </div>

                                                            <div class=" primary-color">
                                                                <h5 class="primary-color">{{$quotation->customer != ""? $quotation->customer->full_name : ''}}</h5>
                                                                <p class="primary-color quotation_view_60" >{{$quotation->customer != ""? $quotation->customer->current_address : ''}}</p>
                                                            </div>
                                                        </div>
                                                        @endif

                                                    </td>
                                                    <td class="quotation_view_50">
                                                         @if(!empty($quotation->vendor_id ))     
                                                        <div class="col-lg-12 ">
                                                            <div class=" primary-color">
                                                                <h5 class="primary-color">@lang('lang.vendor'):</h5>
                                                            </div>

                                                            <div class=" primary-color"> 
                                                                <h5 class="primary-color"> Vendor Name</h5>
                                                                <p class="primary-color quotation_view_60" > Vendor Address</p> 
                                                            </div>
                                                        </div>
                                                        @endif

                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td colspan="2">
                                                        <div class="col-lg-12 ">
                                                            <?php $tem=App\SlaTemplate::where('active_status',1)->first(); ?>
                                                            <p class="primary-color mt-40"><?=$tem->message;?></p>
                                                            <p class="primary-color mt-40"> {{$quotation->description}}   </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="col-lg-12 ">
                                                            <p class="primary-color mt-40"> {{$quotation->public_note}}   </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                            

                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row mt-30 mb-50">
                                        <div class="col-lg-12">
                                            <table class="d-table table-responsive custom-table" cellspacing="0" width="100%" >
                                                <thead>
                                                    <tr>
                                                        <th class="primary-color text-left">@lang('lang.sl')</th>  
                                                        <th class="primary-color text-left">Service Name</th>   
                                                        <th class="primary-color text-right">@lang('lang.price') ({{$currency_symbol}})</th> 
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                @php $grand_total = 0; $sub_total = 0; $total_subtotal=0; $count=1; @endphp
                                                {{-- {{dd($quotation->quotationProducts)}} --}}
                                                @foreach($quotation->quotationProducts as $value)

                                                @php     
                                                    $productDetail = App\SmQuotation::productDetail($value->product_id,$quotation->id);  
                                                    $total_subtotal = $total_subtotal+   $value->sale_unit_price;

                                                @endphp

                                                <tr>
                                                <td>{{$count++}}</td>
                                                    <td class="primary-color text-left">
                                                        @php 
                                                        $q= DB::table('services')->where('id',$value->service_id )->first();
                                                        @endphp
                                                        {{@$q->name}}
                                                        @php
                                                            $service_prices=DB::table('service_assign_prices')
                                                            ->join('service_prices','service_prices.id','=','service_assign_prices.service_price_id')
                                                            ->where('service_assign_prices.service_id',$value->service_id)
                                                            ->get();
                                                            // dd($service_prices);
                                                            $total_service_price=0;
                                                        @endphp
                                                       
                                                        <ul>
                                                            @foreach ($service_prices as $service_price)
                                                            <li style=" text-decoration: none">
                                                                {{$service_price->title}} [{{$service_price->price}}]
                                                            </li>
                                                            @php
                                                                 $total_service_price+=$service_price->price
                                                            @endphp
                                                            @endforeach
                                                           
                                                        </ul>
                                                    
                                                    </td>  
                                                    <td class="primary-color text-right">{{$currency_symbol}}
                                                      {{$quotation->paybale_amount}}</td>  
                                                </tr>
                                                @endforeach
                                                    <tr>   
                                                        <td></td>
                                                        <td class="fw-600 primary-color text-right">@lang('lang.sub') @lang('lang.total') ({{$currency_symbol}}) </td>
                                                        <td class="fw-600 primary-color text-right">
                                                            {{App\User::NumberToBangladeshiTakaFormat( $quotation->paybale_amount)}}
                                                        </td>
                                                    </tr>
                                                    <tr>   
                                                        <td></td>
                                                        {{-- <td class="fw-600 primary-color text-right">Discount ({{$quotation->discount_type != ""? ($quotation->discount_type == "P"? ' %': ' fixed'):'' }})</td> --}}
                                                        <td class="fw-600 primary-color text-right">Paid  ({{$currency_symbol}})</td>
                                                        <td class="fw-600 primary-color text-right">
                                                        {{$quotation->paid_amount != ""?  App\User::NumberToBangladeshiTakaFormat($quotation->paid_amount): "0.00" }}  
                                                        </td>
                                                    </tr>
                                                    <tr>   
                                                        <td></td>
                                                        <td class="fw-600 primary-color text-right">Due  ({{$currency_symbol}})</td>
                                                        <td class="fw-600 primary-color text-right">
                                                            {{ App\User::NumberToBangladeshiTakaFormat($quotation->paybale_amount-$quotation->paid_amount) }} 
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
<h3>
    Required Papers: 
    @php
        $papers_title=[];
   
    foreach($quotation->quotationProducts as $value){
        $service_paper=DB::table('service_assign_papers')->where('service_id',$value->service_id)->get();
        foreach ($service_paper as $key => $paper) {
            $paper_info=DB::table('papers')->where('id',$paper->service_paper_id)->first();
           array_push($papers_title, $paper_info->title);
        }
    }
        echo implode(",",array_unique($papers_title));
    @endphp
</h3>
 
                <div class="row mb-20">
                    <div class="col-lg-12">
                        @if(!empty($quotation->public_note))
                        <h4>Note</h4>
                        <p class="primary-color">{{$quotation->public_note}}</p>
                        <hr>
                        @endif
                    </div>
                </div> 

                <div class="row mb-20">
                    <div class="col-lg-12">
                        @if(!empty($quotation->terms_note))
                        <h4>Terms</h4>
                        <p class="primary-color">{{$quotation->terms_note}}</p>
                        <hr>
                        @endif
                    </div>
                </div> 


                <div class="row mb-20 mt-40">
                    <div class="col-lg-12">
                        <table style="width:100%">
                            <tr>
                                <td style="width:80%"></td>
                                <td style="width:20%">
                                    <hr>
                                    <p>Signature</p>
                                    @if(!empty($quotation->signature_person))
                                    <p>{{@$quotation->signature_person}}<br>{{@$quotation->signature_company}}</p>
                                    @endif
                                </td>
                            </tr>

                        </table>

                    </div> 
                </div>
                <div class="row mt-40">
                    <div class="col-lg-12">
                        <hr>
                        @if(!empty($quotation->footer_note))
                        <p class="primary-color" style="text-align: justify;font-size: 11px; bottom:0">{{$quotation->footer_note}}</p>
                        @endif
                    </div>
                </div> 




 
                                    <div class="row mt-40">
                                        <div class="col-lg-12 text-center">
                                            <button class="primary-btn fix-gr-bg" onclick="javascript:printDiv('purchaseInvoice')" id="printButton">@lang('lang.print')</button>
                                        </div>
                                      </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection








  









