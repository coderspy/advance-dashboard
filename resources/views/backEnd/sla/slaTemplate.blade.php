@extends('backEnd.master')
@section('mainContent') 
@php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; } 

    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo; 
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
@endphp 
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>SLA & Quotation Template</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="{{url('create-sla')}}" class="">SLA & Quotation</a> 
                <a href="{{url('create-sla-template')}}" class="active">SLA & Quotation Template</a> 
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        @if(isset($editData))
       {{--  <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('create-sla')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')  @lang('lang.new')
                </a>
            </div>
        </div>  --}}
        @endif
        
        <div class="row">
            <div class="col-lg-12"> 

                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    Edit
                                @else
                                    Add
                                @endif
                                    SLA & Quotation Template
                            </h3>
                        </div>

                      @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'sla-template-update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        <input type="hidden" name="id" value="{{$editData->id}}">
                        @else 
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'store-sla-template', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }} 
                        @endif
              
                     <div class="white-box">
                            <div class="add-visitor">
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" rows="10" name="message" >{{isset($editData)? $editData->message:old('message')}}
                                            </textarea>
                                            <label>Template<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            @if ($errors->has('message'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('message') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <span class="text-primary">[name], [present_address], [created_at],  [email], [phone],[system_name]</span>
                                    </div>
                                </div>        

                            <div class="row mt-40">
                                <div class="col-lg-12 d-flex">
                                    <p class="text-uppercase fw-500 mb-10">Is Default SLA & Quotation Template *</p>
                                    <div class="d-flex radio-btn-flex ml-40">
                                        <div class="mr-30">
                                            <input type="radio" name="active_status" id="relationFather" value="1" class="common-radio relationButton" {{old('relationButton') == 1? 'checked': ''}}>
                                            <label for="relationFather">@lang('lang.yes')</label>
                                        </div>
                                        <div class="mr-30">
                                            <input type="radio" name="active_status" id="relationMother" value="0" class="common-radio relationButton" {{old('relationButton') == 0? 'checked': 'checked'}}>
                                            <label for="relationMother">@lang('lang.no')</label>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            {{isset($editData)? 'update':'create'}} SLA & Quotation Template
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-40">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">SLA & Quotation Template List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                 
                                <tr>
                                    <th>Sl</th>
                                    <th>message</th> 
                                    <th>created at</th>
                                    <th>Status</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count =1; @endphp
                                @foreach($slaTemplate as $value)
                                  <tr>
                                    <td >{{$count++}}</td>
                                    <td >{{$value->message}}</td> 
                                    <td >{{date('jS M, Y', strtotime($value->created_at))}}</td>  
                                    <td>
                                        @if(@$value->active_status==1)
                                            <button class="primary-btn fix-gr-bg small">Active</button>
                                        @else
                                            <button class="primary-btn small bg-danger text-white border-0">Inactive</button>
                                        @endif


                                    </td>
                                  
                                    <td >
                                        <a class="primary-btn small bg-success text-white border-0" href="{{url('sla-template-edit', [$value->id])}}"> <span class="ti-pencil"> </span></a>
                                        <a class="primary-btn small bg-warning text-white border-0" href="{{url('sla-template-delete', [$value->id])}}"> <span class="ti-close"> </span></a> 
                                    </td>
                                </tr>
                                  <div class="modal fade admin-query" id="deletequotations{{$value->id}}" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">@lang('lang.delete') SLA</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal">@lang('lang.cancel')
                                                        </button>

                                                        <a href="{{url('sla-template-delete', [$value->id])}}"
                                                           class="primary-btn fix-gr-bg">@lang('lang.delete')</a>

                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
     $(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    });</script>
@endsection
