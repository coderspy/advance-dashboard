@extends('backEnd.master')
@section('mainContent') 
@php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; } 

    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
   
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo; 
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
@endphp 
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>SLA List</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="{{url('quotations')}}" class="active">SLA List</a> 
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
         
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('create-sla')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')  @lang('lang.new')
                </a>
            </div>
        </div> 
        
        <div class="row">

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">SLA List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                               @if(session()->has('message-success') != "" ||
                                session()->get('message-danger') != "")
                                <tr>
                                    <td colspan="11">
                                         @if(session()->has('message-success'))
                                          <div class="alert alert-success">
                                              {{ session()->get('message-success') }}
                                          </div>
                                        @elseif(session()->has('message-danger'))
                                          <div class="alert alert-danger">
                                              {{ session()->get('message-danger') }}
                                          </div>
                                        @endif
                                    </td>
                                </tr>
                                 @endif 
                                <tr>
                                    <th>Sl</th>
                                    <th>Number</th>
                                    <th>Reference</th>
                                    <th>Created Date</th>
                                    <th>Expire Alert Date</th>
                                    <th>Customer No</th>
                                    <th>Customer name</th>
                                    <th>Service</th>
                                    <th>@lang('lang.payment') @lang('lang.status')</th>
                                    <th>Paybale amount ({{$currency_symbol}})</th> 
                                    <th>Paid amount ({{$currency_symbol}})</th> 

                                    <th>status</th>
                                    
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count =1; @endphp
                                @foreach($slas as $value)
                                  <tr>
                                    <td >{{$count++}}</td>
                                    <td >{{$value->number}}</td>
                                    <td >{{$value->reference}}</td> 
                                    <td >{{date('jS M, Y', strtotime($value->created_at))}}</td>
                                    <td >{{date('jS M, Y', strtotime($value->date))}}</td>
                                    <td >{{$value->customer->staff_no}}</td> 
                                    <td >{{$value->customer->first_name}}</td>

                                    <td >

                                     @php 
                                        $sla_services= DB::table('sla_services')->where('sla_id',$value->id)->get();
                                        foreach($sla_services as $services){
                                            $service_details=DB::table('services')->where('id',$services->service_id)->first();
                                          
                                            echo $service_details->name .'<br>';
    
                                           }
                                        
                                        @endphp

                                    </td> 
                                    <td >{{$value->payment_status}}</td> 
                                    <td >{{App\User::NumberToBangladeshiTakaFormat($value->paybale_amount)}}</td>  
                                    <td >{{$value->paid_amount}}</td>  

                                  @if(Auth::user()->role_id==1)   

                                    

                                    <td> 
                                        @if($value->is_approved==0) 
                                    <a data-toggle="modal"   href="#" data-target="#approveStatus{{$value->id}}" class="primary-btn small tr-bg">Pending</a>
                                        @else
                                    <a data-toggle="modal"   href="#" data-target="#approveStatus{{$value->id}}" class="primary-btn fix-gr-bg small">Approved</a>
                                        @endif
                                    
                                    </td>

                                @else
                                    <td> 
                                        @if($value->is_approved==0) 
                                          <a style="color:red;">Pending</a>
                                        @else
                                         <a style="color: darkgreen;">Approved</a>
                                        @endif
                                    
                                    </td>
                                @endif

                                    <td >
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                @lang('lang.select')
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">  
                                                <a class="dropdown-item" href="{{url('sla-list-view', [$value->id])}}">@lang('lang.view')</a> 
                                                <a class="dropdown-item" href="{{url('sla-edit', [$value->id])}}">@lang('lang.edit')</a> 

                                            @php
                                                $due=$value->paybale_amount-$value->paid_amount;
                                            @endphp
                                              @if($value->is_approved==1) 
                                                @if ($due>0)
                                                    <a class="dropdown-item" href="{{url('sla-payment', [$value->id])}}">Payment</a> 

                                                @endif
                                                @endif

                                                <a class="dropdown-item" data-toggle="modal" data-target="#deletequotations{{$value->id}}"  href="#">@lang('lang.delete') </a> 
                                           
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                  <div class="modal fade admin-query" id="deletequotations{{$value->id}}" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">@lang('lang.delete') SLA</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal">@lang('lang.cancel')
                                                        </button>

                                                        <a href="{{url('sla-list-delete', [$value->id])}}"
                                                           class="primary-btn fix-gr-bg">@lang('lang.delete')</a>

                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="modal fade admin-query" id="approveStatus{{$value->id}}" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    @if (@$value->is_approved==0)
                                                        Make Approve
                                                    @else
                                                        Make Pending
                                                    @endif
                                                    
                                                  SLA</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to 
                                                        @if (@$value->is_approved==0)
                                                        Make Approve
                                                    @else
                                                        Make Pending
                                                    @endif</h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal">@lang('lang.cancel')
                                                        </button>
                                                            @php
                                                            if (@$value->is_approved==0) {
                                                                $status=' Make Approve';
                                                            } else {
                                                            $status='Make Pending';
                                                            }

                                                                
                                                            @endphp
                                                        <a href="{{url('sla/approve', [$value->id])}}"
                                                        class="primary-btn fix-gr-bg">{{$status}}</a>

                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
     $(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    });</script>
@endsection
