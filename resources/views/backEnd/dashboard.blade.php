@extends('backEnd.master')


@section('mainContent')

@php  $setting = App\SmGeneralSettings::find(1); if(!empty($setting->currency_symbol)){ $currency = $setting->currency_symbol; }else{ $currency = '$'; } @endphp
<style>
p{
    text-transform: capitalize;
}
</style>
<section class="mb-40">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6">
                <div class="main-title">
                    <h3 class="mb-0">Welcome To {{Auth::user()->full_name}} </h3>
                </div>
            </div>
            <div class="col-lg-6 text-right">
                <div class="main-title">
                    <form name="Tick">
                        
                    <span class="mb-0"  style="    background: none; border: none;  font-size: 18px;  color: #405093; font-weight: 500;"> {{date('jS M, Y')}},  </span>
                        <input type="text" size="11" name="Clock" style="    background: none; border: none;  font-size: 18px;  color: #405093; font-weight: 500;">
                    </form>

                        <script> 
                        function show(){
                        var Digital=new Date()
                        var hours=Digital.getHours()
                        var minutes=Digital.getMinutes()
                        var seconds=Digital.getSeconds()
                        var dn="AM" 
                        if (hours>12){
                            dn="PM"
                            hours=hours-12
                        }
                        if (hours==0)
                            hours=12
                        if (minutes<=9)
                            minutes="0"+minutes
                        if (seconds<=9)
                        seconds="0"+seconds
                        document.Tick.Clock.value=hours+":"+minutes+":"
                        +seconds+" "+dn
                        setTimeout("show()",1000)
                        }
                        show()
                        </script>   
                </div>
            </div>
            <hr>
        </div> 
 @if(Auth::user()->role_id==1)   
      <div class="row"> 
        <div class="col-lg-3 col-md-6">
            <a href="{{route('staff_directory')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Inactive User </h3>
                            <p class="mb-0">total Inactive User</p>
                        </div>
                        <?php
                           $Inactive = DB::table('sm_staffs')->where('active_status', '=', 0)->get();
                              ?>
                           <h1 class="gradient-color2">{{$Inactive->count()}}</h1>   
                    </div>
                </div>
            </a>
        </div>
    <div class="col-lg-3 col-md-6">
        <a href="{{route('staff_directory')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Active User </h3>
                        <p class="mb-0">total Active User</p>
                    </div>
                     <?php
                       $active =DB::table('sm_staffs')->where('active_status', '=', 1)->get();
                       ?>
                    <h1 class="gradient-color2">{{$active->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
      <div class="col-lg-3 col-md-6">
        <a href="{{url('reports/services-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Services</h3>
                        <p class="mb-0">Total services</p>
                    </div>
                     <?php
                       $services = DB::table('services')->get();
                       ?> 
                    <h1 class="gradient-color2">{{$services->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="{{url('sla-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Pending SLA </h3>
                        <p class="mb-0">total Pending SLA</p>
                    </div>
                     <?php
                       $Pending = DB::table('customer_slas')->where('is_approved', '=', 0)->get();
                       ?> 
                    <h1 class="gradient-color2">{{$Pending->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="{{url('sla-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Approved SLA </h3>
                        <p class="mb-0">total Approved SLA</p>
                    </div>
                     <?php
                       $Approved = DB::table('customer_slas')->where('is_approved', '=', 1)->get();
                       ?> 
                    <h1 class="gradient-color2">{{$Approved->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
  
    <div class="col-lg-3 col-md-6">
        <a href="{{url('quotations')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Quotation</h3>
                        <p class="mb-0">Total Quotations</p>
                    </div>
                     <?php
                       $Quatation = DB::table('sm_quotations')->where('active_status', '=', 1)->get();
                       ?>
                    <h1 class="gradient-color2">{{$Quatation->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-6">
        <a href="{{url('reports/pro-user-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Pro User</h3>
                        <p class="mb-0">Total Pro User</p>
                    </div>
                     <?php
                       $pro_user = DB::table('sm_staffs')->where('role_id', '=', 3)->get();
                       ?>
                    <h1 class="gradient-color2">{{$pro_user->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
    
    <div class="col-lg-3 col-md-6">
        <a href="{{url('customer-directory')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Customer</h3>
                        <p class="mb-0">Total Customer</p>
                    </div>
                    <?php
                       $customer = DB::table('sm_staffs')->where('role_id', '=', 4)->where('active_status', '=', 1)->get();
                       ?> 
                    <h1 class="gradient-color2">{{$customer->count()}}</h1>
                    
                </div>
            </div>
        </a>
    </div>
     </div>
      @endif
    @if(Auth::user()->role_id==3)  
      <div class="row"> 
        <div class="col-lg-3 col-md-6">
        <a href="{{url('customer-directory')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Active Customer</h3>
                        <p class="mb-0">total Customer</p>
                    </div>
                     <?php
                       $customer_pro =DB::table('sm_staffs')->where('active_status', '=', 1)->where('role_id', '=', 4)->get();
                       ?>
                    <h1 class="gradient-color2">{{$customer_pro->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-6">
        <a href="{{url('reports/services-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Services</h3>
                        <p class="mb-0">Total services</p>
                    </div>
                     <?php
                       $services = DB::table('services')->get();
                       ?> 
                    <h1 class="gradient-color2">{{$services->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="{{url('sla-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Approved SLA </h3>
                        <p class="mb-0">total Approved SLA</p>
                    </div>
                     <?php
                       $Approved_individual_pro = DB::table('customer_slas')->where('is_approved', '=', 1)->where('created_by', '=', Auth::user())->get();
                       ?> 
                    <h1 class="gradient-color2">{{$Approved_individual_pro->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
        <div class="col-lg-3 col-md-6">
        <a href="{{url('sla-list')}}" class="d-block">
            <div class="white-box single-summery">
                <div class="d-flex justify-content-between">
                    <div>
                        <h3>Pending SLA </h3>
                        <p class="mb-0">total Pending SLA</p>
                    </div>
                     <?php
                       $Approved_individual_pro = DB::table('customer_slas')->where('is_approved', '=', 0)->where('created_by', '=', Auth::user())->get();
                       ?> 
                    <h1 class="gradient-color2">{{$Approved_individual_pro->count()}}
                    </h1>
                </div>
            </div>
        </a>
    </div>
  </div>
  @endif
</section>
<section class="mt-50">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title">
                    <h3 class="mb-30">Notice board </h3>
                </div>
            </div>

            <div class="col-lg-12">
                <table class="school-table-style w-100">
                    <thead>
                        <tr>
                            <th>date </th>
                            <th>title </th>
                            <th>actions </th>
                        </tr>
                    </thead>

                    <tbody>
                      <?php $role_id = Auth()->user()->role_id; ?>
                      <?php if (isset($notices)) {
                            foreach ($notices as $notice) {
                                $inform_to = explode(',', $notice->inform_to);
                                if (in_array($role_id, $inform_to)) {
                                    ?>
                                <tr>
                                    <td> {{$notice->publish_on != ""? App\SmGeneralSettings::DateConvater($notice->publish_on):''}}

                                    </td>
                                    <td>{{$notice->notice_title}}</td>
                                    <td>
                                    <a href="{{url('view/notice/'.$notice->id)}}" title="View notice"  class="primary-btn small tr-bg modalLink" data-modal-size="modal-lg">view</a>
                                    </td>
                                </tr>
                                    <?php 
                                }
                            }
                        }

                        ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
 <section class="mt-50">
    <div class="container-fluid p-0">
        <div class="row">  
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@lang('lang.calendar')</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class='common-calendar'>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
@php $events=[]; @endphp 
<script type="text/javascript">
    /*-------------------------------------------------------------------------------
       Full Calendar Js 
    -------------------------------------------------------------------------------*/
    if ($('.common-calendar').length) {
        $('.common-calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            height: 650,
            events: <?php echo json_encode($events);?> ,
        });
    }


</script>

 
@endsection

@section('script')
<script type="text/javascript">


 


    const monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

  

</script>

<?php


$events = array(); 
?>
<script type="text/javascript">
    /*-------------------------------------------------------------------------------
       Full Calendar Js 
    -------------------------------------------------------------------------------*/
    if ($('.common-calendar').length) {
        $('.common-calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            height: 650,
            events: <?php echo json_encode($events);?> ,
        });
    }


</script>

@endsection 