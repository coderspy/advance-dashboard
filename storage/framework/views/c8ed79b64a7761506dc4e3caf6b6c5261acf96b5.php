<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> Assign Discount</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>">Dashboard</a>
                <a href="#">Assign Discount</a>
                <a href="#">Assign Discount List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        <?php if(isset($editData)): ?> 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="<?php echo e(url('assign-discount')); ?>" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    <?php echo app('translator')->getFromJson('lang.add'); ?>
                </a>
            </div>
        </div> 
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Discount List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Discount Item</th>
                                    <th>Amount (BDT)</th>
                                    <th>Type</th>
                                    <th>Services</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $count=1; ?> 
                                <?php $__currentLoopData = $assign_discount_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $editData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($count++); ?></td>                                   
                                    <td>
                                        <?php
                                        $discount = DB::table('discounts')->select('*')
                                                ->where('id', $editData->discount_id)
                                                ->first();
                                        if (!empty($discount)) {
                                            echo @$discount->title;
                                        }
                                        ?> 
                                         
                                   </td>
                                   <td><?php echo e(number_format(@$discount->amount, 2, '.', '')); ?></td>
                                   <td><?php if(@$discount->type=="F"): ?> Fixed <?php else: ?> Percentage <?php endif; ?></td>
                                    <td>
                                            <?php
                                        $service_name = DB::table('services')->select('name')
                                                ->where('id', $editData->service_id)
                                                ->first();
                                        if (!empty($service_name)) {
                                            echo $service_name->name;
                                        }
                                        ?> 


                                        
                                    </td>
                                </tr>
                                
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>