<?php $__env->startSection('mainContent'); ?> 
<?php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; } 

    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo; 
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
?> 
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>SLA & Quotation Template</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->getFromJson('lang.dashboard'); ?></a>
                <a href="<?php echo e(url('create-sla')); ?>" class="">SLA & Quotation</a> 
                <a href="<?php echo e(url('create-sla-template')); ?>" class="active">SLA & Quotation Template</a> 
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        <?php if(isset($editData)): ?>
       
        <?php endif; ?>
        
        <div class="row">
            <div class="col-lg-12"> 

                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30"><?php if(isset($editData)): ?>
                                    Edit
                                <?php else: ?>
                                    Add
                                <?php endif; ?>
                                    SLA & Quotation Template
                            </h3>
                        </div>

                      <?php if(isset($editData)): ?>
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'sla-template-update', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                        <input type="hidden" name="id" value="<?php echo e($editData->id); ?>">
                        <?php else: ?> 
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'store-sla-template', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?> 
                        <?php endif; ?>
              
                     <div class="white-box">
                            <div class="add-visitor">
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control<?php echo e($errors->has('message') ? ' is-invalid' : ''); ?>" rows="10" name="message" ><?php echo e(isset($editData)? $editData->message:old('message')); ?>

                                            </textarea>
                                            <label>Template<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('message')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('message')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                        <span class="text-primary">[name], [present_address], [created_at],  [email], [phone],[system_name]</span>
                                    </div>
                                </div>        

                            <div class="row mt-40">
                                <div class="col-lg-12 d-flex">
                                    <p class="text-uppercase fw-500 mb-10">Is Default SLA & Quotation Template *</p>
                                    <div class="d-flex radio-btn-flex ml-40">
                                        <div class="mr-30">
                                            <input type="radio" name="active_status" id="relationFather" value="1" class="common-radio relationButton" <?php echo e(old('relationButton') == 1? 'checked': ''); ?>>
                                            <label for="relationFather"><?php echo app('translator')->getFromJson('lang.yes'); ?></label>
                                        </div>
                                        <div class="mr-30">
                                            <input type="radio" name="active_status" id="relationMother" value="0" class="common-radio relationButton" <?php echo e(old('relationButton') == 0? 'checked': 'checked'); ?>>
                                            <label for="relationMother"><?php echo app('translator')->getFromJson('lang.no'); ?></label>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            <?php echo e(isset($editData)? 'update':'create'); ?> SLA & Quotation Template
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-40">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">SLA & Quotation Template List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                 
                                <tr>
                                    <th>Sl</th>
                                    <th>message</th> 
                                    <th>created at</th>
                                    <th>Status</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count =1; ?>
                                <?php $__currentLoopData = $slaTemplate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <tr>
                                    <td ><?php echo e($count++); ?></td>
                                    <td ><?php echo e($value->message); ?></td> 
                                    <td ><?php echo e(date('jS M, Y', strtotime($value->created_at))); ?></td>  
                                    <td>
                                        <?php if(@$value->active_status==1): ?>
                                            <button class="primary-btn fix-gr-bg small">Active</button>
                                        <?php else: ?>
                                            <button class="primary-btn small bg-danger text-white border-0">Inactive</button>
                                        <?php endif; ?>


                                    </td>
                                  
                                    <td >
                                        <a class="primary-btn small bg-success text-white border-0" href="<?php echo e(url('sla-template-edit', [$value->id])); ?>"> <span class="ti-pencil"> </span></a>
                                        <a class="primary-btn small bg-warning text-white border-0" href="<?php echo e(url('sla-template-delete', [$value->id])); ?>"> <span class="ti-close"> </span></a> 
                                    </td>
                                </tr>
                                  <div class="modal fade admin-query" id="deletequotations<?php echo e($value->id); ?>" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title"><?php echo app('translator')->getFromJson('lang.delete'); ?> SLA</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4><?php echo app('translator')->getFromJson('lang.are_you_sure_to_delete'); ?></h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal"><?php echo app('translator')->getFromJson('lang.cancel'); ?>
                                                        </button>

                                                        <a href="<?php echo e(url('sla-template-delete', [$value->id])); ?>"
                                                           class="primary-btn fix-gr-bg"><?php echo app('translator')->getFromJson('lang.delete'); ?></a>

                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
     $(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    });</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>