<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Discount</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>">Dashboard</a>
                <a href="#">Discount</a>
                <a href="#">Discount List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        <?php if(isset($editData)): ?> 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="<?php echo e(url('service-list')); ?>" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    <?php echo app('translator')->getFromJson('lang.add'); ?>
                </a>
            </div>
        </div> 
        <?php endif; ?>

        <div class="row"> 
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30"><?php if(isset($editData)): ?>
                                    edit
                                <?php else: ?>
                                    add
                                <?php endif; ?>
                                    Discount
                            </h3>
                        </div>

                      <?php if(isset($editData)): ?>
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'discount-update', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                        <input type="hidden" name="id" value="<?php echo e($editData->id); ?>">
                        <?php else: ?> 
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'store-discount', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?> 
                        <?php endif; ?>
              
                     <div class="white-box">
                            <div class="add-visitor">
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>" type="text" name="title" autocomplete="off" value="<?php echo e(isset($editData)? $editData->title:old('title')); ?>">
                                            <label>Title Name<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('title')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('title')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                              <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('type') ? ' is-invalid' : ''); ?>" name="type" id="">
                                                <option data-display="Select Type *" value="">Select Type *</option>
                                               
                                                <option <?php echo e(isset($editData)? @$editData->type == 'P'? 'selected': '':''); ?> value="P">Percentage</option>
                                                <option <?php echo e(isset($editData)? @$editData->type == 'F'? 'selected': '':''); ?> value="F">Fixed amount</option>
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control<?php echo e($errors->has('note') ? ' is-invalid' : ''); ?>" type="text" name="note" autocomplete="off" value="<?php echo e(isset($editData)? $editData->note:old('note')); ?>">
                                            <label>Note<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('note')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('note')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <input class="primary-input form-control<?php echo e($errors->has('amount') ? ' is-invalid' : ''); ?>" type="text" name="amount" autocomplete="off" value="<?php echo e(isset($editData)? $editData->amount:old('amount')); ?>">
                                            <label>Amount<span>*</span></label> 
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('amount')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('amount')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="no-gutters input-right-icon">
                                            <div class="col-lg-12"> 
                                                <div class="input-effect">
                                                    <input class="primary-input date form-control<?php echo e($errors->has('discount_start_date') ? ' is-invalid' : ''); ?>" id="discount_start_date" type="text"
                                                    name="discount_start_date" value="<?php echo e(isset($editData->discount_start_date)? $editData->discount_start_date: date('m/d/Y')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>Discount Start Date<span>*</span> </label>
                                                    <?php if($errors->has('discount_start_date')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('discount_start_date')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button class="" type="button">
                                                    <i class="ti-calendar" id="discount_start_date"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                         <div class="row mt-40">
                             <div class="col-lg-12">
                                 
                            <div class="no-gutters input-right-icon">
                                <div class="col-lg-12"> 
                                    <div class="input-effect">
                                        <input class="primary-input date form-control<?php echo e($errors->has('discount_end_date') ? ' is-invalid' : ''); ?>" id="discount_end_date" type="text"
                                        name="discount_end_date" value="<?php echo e(isset($editData->discount_end_date)? $editData->discount_end_date: date('m/d/Y')); ?>">
                                        <span class="focus-border"></span>
                                        <label>Discount End Date<span>*</span> </label>
                                        <?php if($errors->has('discount_end_date')): ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('discount_end_date')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="" type="button">
                                        <i class="ti-calendar" id="discount_end_date"></i>
                                    </button>
                                </div>
                            </div>
                             </div>
                        </div>          
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="">
                                            <span class="ti-check"></span>
                                            <?php echo e(isset($editData)? 'update':'save'); ?> Discount
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Discount List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Discount Title</th>
                                    <th>Discount Type</th>
                                    <th>Amount</th>
                                    <th>Note</th>
                                    <th>Discount Start date</th>
                                    <th>Discount end date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $count=1; ?> 
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $editData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($count++); ?></td>
                                    <td><?php echo e($editData->title); ?></td>
                                    <td>
                                         <?php if($editData->type=='F'): ?>
                                            Fixed Price
                                               
                                         <?php elseif($editData->type=='P'): ?>
                                            Percentage
                                         <?php endif; ?>
                                   </td>
                                    <td><?php echo e(number_format(@$editData->amount,2,'.', '')); ?></td>
                                    <td><?php echo e($editData->note); ?></td>
                                    <td><?php echo e($editData->discount_start_date); ?></td>
                                    <td><?php echo e($editData->discount_end_date); ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                Select
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                               
                                               <a class="dropdown-item" href="<?php echo e(url('edit-discount', [$editData->id])); ?>">Edit</a>
                                             
                                               <a class="dropdown-item" data-toggle="modal" data-target="#DeleteService<?php echo e($editData->id); ?>"
                                                    href="#">Delete </a>
                                            
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteService<?php echo e($editData->id); ?>" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete  Item</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to delete ? </h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal"><?php echo app('translator')->getFromJson('lang.cancel'); ?></button>
                                                     <?php echo e(Form::open(['url' => 'discount-delete/'.$editData->id, 'method' => 'GET', 'enctype' => 'multipart/form-data'])); ?>

                                                    <button class="primary-btn fix-gr-bg" type="submit">DELETE</button>
                                                     <?php echo e(Form::close()); ?>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>