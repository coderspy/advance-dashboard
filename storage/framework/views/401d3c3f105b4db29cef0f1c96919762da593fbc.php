<?php $__env->startSection('mainContent'); ?> 
<link rel="stylesheet" href="<?php echo e(asset('public/backEnd/css/sla.css')); ?>">
<?php
 
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; }
    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo;
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> Create SLA</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->getFromJson('lang.dashboard'); ?></a>
                <a href="<?php echo e(url('tender')); ?>">SLA</a>
                <a href="<?php echo e(url('tender/create')); ?>" class="active">create SLA</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30"><?php if(isset($edit)): ?>
                                   Edit
                                <?php else: ?>
                                   Add
                                <?php endif; ?>
                                  SLA
                            </h3>
                        </div>
                  
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => 'true', 'url' => 'sla-store', 'method' => 'POST', 'id'=>'tender-create-form','enctype' => 'multipart/form-data' ])); ?>                   
                        <div class="white-box">   
                                <div class="text-center">

                                        <?php if($errors->any()): ?>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            
                                                <div class="alert alert-danger">
                                                    
                                                    <?php echo e($error); ?>.
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                </div>

                            <div class="add-visitor">
                                <div class="row ">
                                    <div class="col-lg-3">
                                        <div class="invoice-details-left">
                                            <div class="mb-20">  <img src="<?php echo e(asset($logo)); ?>" class="tender-create-logo w-25" > </div>
                                            <div class="business-info">
                                                <h3><?php echo e($generalSetting->company_name); ?></h3>
                                                <p class="textWrap"><?php echo e($generalSetting->address); ?></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="row">
        
                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('days') ? ' is-invalid' : ''); ?>" type="number" min="1" max="365" name="days" autocomplete="off" id="days"
                                                    value="<?php echo e(isset($edit)? !empty($edit->days)? $edit->days : old('days'):''); ?>">
                                                    <label>Service Durration Days<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    
                                                </div>
                                            </div> 

                                            
                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('number') ? ' is-invalid' : ''); ?>" type="text" name="number" autocomplete="off" id="number" value="<?php echo e(isset($edit)? !empty($edit->number)? $edit->number : old('number'): Auth::user()->id.''.time()); ?>">
                                                    <label>SlA NUMBER<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    <?php if($errors->has('number')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('number')); ?></strong>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="input-effect">
                                                            <?php
                                                            $value = date('m/d/Y');
                                                            if(isset($edit) && !empty($edit->date) ){  $value = date('m/d/Y', strtotime($edit->date));   }
                                                            else{ if(!empty(old('date'))){ $value = old('date');   }else{  $value = date('m/d/Y');   } }
                                                            ?>
                                                            <input class="primary-input date" id="date" type="text" name="date" value="<?php echo e($value); ?>">
                                                            <label>SLA EXPIRE DATE(Alert)</label>
                                                            <span class="focus-border"></span>
                                                            <?php if($errors->has('date')): ?>
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong><?php echo e($errors->first('date')); ?></strong>
                                                                </span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="" type="button">
                                                            <i class="ti-calendar" id="end-date-icon"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('reference') ? ' is-invalid' : ''); ?>" type="text" name="reference" autocomplete="off"     value="<?php echo e(isset($edit)? !empty($edit->reference)? $edit->reference : old('reference'): old('reference')); ?>" id="reference">
                                                    <label><?php echo app('translator')->getFromJson('lang.reference'); ?> <span></span></label>
                                                    <span class="focus-border"></span>
                                                    <?php if($errors->has('reference')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('reference')); ?></strong>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            <div class="col-lg-3 mt-20 mb-10">
                                                <div class="col-lg-12 d-flex mt-10">
                                                    <p class="text-uppercase fw-500 mb-10">Type *</p>
                                                    <div class="d-flex radio-btn-flex ml-40">
                                                        <div class="mr-30">
                                                            <input type="radio" name="customer_type" id="relationFather" value="new" class="common-radio relationButton newc" <?php echo e(old('customer_type') == "new"? 'checked': ''); ?>>
                                                            <label for="relationFather">New</label>
                                                        </div>
                                                        <div class="mr-30">
                                                            <input type="radio" name="customer_type" id="relationMother" value="old" class="common-radio relationButton oldc" <?php echo e(old('customer_type') == "old"? 'checked': ''); ?>>
                                                            <label for="relationMother">Old</label>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                            <div class="exCustomer row" style="display:none">
                                                <div class="col-lg-4 mt-20 mb-10 ">
                                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('customer') ? ' is-invalid' : ''); ?>" name="customer_id" id="SelectCustomerID">
                                                        <option data-display="<?php echo app('translator')->getFromJson('lang.select'); ?> <?php echo app('translator')->getFromJson('lang.customer'); ?> *" value="" ><?php echo app('translator')->getFromJson('lang.select'); ?> <?php echo app('translator')->getFromJson('lang.customer'); ?> *</option>
                                                        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                             <option value="<?php echo e($value->id); ?>"  <?php echo e(isset($edit)? !empty($edit->customer_id)? $edit->customer_id==$value->id ? 'selected':'':'':''); ?> >
                                                                <?php echo e($value->full_name); ?>

                                                                - [<?php echo e(@$value->nid_number); ?>]
                                                            </option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                    <?php if($errors->has('customer_id')): ?>
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong><?php echo e($errors->first('customer')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>  
                                                
                                                <div class="col-lg-8"> 
                                                    <div class="paper_list" id="paper_list">
                                                        <h4 class="mt-20" id="avaliable_papers"></h4>
                                                        <ul>    
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                                                          
                                        <div class="row" id="newCustomer" style="display:none">
                                            <div class="col-lg-12 mt-10">
                                                <h4 class="text-center">New Customer </h4>
                                            </div>
                                            
 
                                            <div class="col-lg-4 mt-20">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control <?php echo e($errors->has('first_name') ? 'is-invalid' : ' '); ?>" type="text"  name="first_name" value="<?php echo e(old('first_name')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>First Name <span>*</span> </label>
                                                    <?php if($errors->has('first_name')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div> 
                                            <div class="col-lg-4 mt-20">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('last_name') ? ' is-invalid' : ''); ?>" type="text"  name="last_name" value="<?php echo e(old('last_name')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>Last Name <span>*</span> </label>
                                                    <?php if($errors->has('last_name')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" type="email"  name="email" value="<?php echo e(old('email')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>Email <span>*</span> </label>
                                                    <?php if($errors->has('email')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('mobile') ? ' is-invalid' : ''); ?>" type="number"  name="mobile" value="<?php echo e(old('mobile')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>Mobile <span>*</span> </label>
                                                    <?php if($errors->has('mobile')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('mobile')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('passport_number') ? ' is-invalid' : ''); ?>" type="text"  name="passport_number" value="<?php echo e(old('passport_number')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>Passport Number <span>*</span> </label>
                                                    <?php if($errors->has('passport_number')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('passport_number')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-4 mt-20"> 
                                                <div class="input-effect">
                                                    <input class="primary-input form-control<?php echo e($errors->has('nid_number') ? ' is-invalid' : ''); ?>" type="text"  name="nid_number" value="<?php echo e(old('nid_number')); ?>">
                                                    <span class="focus-border"></span>
                                                    <label>NID Number <span>*</span> </label>
                                                    <?php if($errors->has('nid_number')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('nid_number')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>
 
                               
 

                             
                                <!-- ***************** equipment-table ************************ ******** -->
                                <div class="equipment comon-status row mt-25 d-block">
                                    <div class="col-lg-12 text-right">
                                        <button type="button" class="primary-btn small fix-gr-bg" id="addRowEquipment">
                                            <span class="ti-plus pr-2"></span> Add New Service
                                        </button>
                                    </div>
                                    <input type="hidden" id="add_product_counter" >
                                    <table class="display school-table school-table-style without-box-shadow" cellspacing="0" width="100%" id="equipment-table">
                                        <thead>
                                            <tr>
                                                <th class="item_name">Service Name</th>   
                                                <th>Papers</th>
                                                <th>Price (<?php echo e($currency_symbol); ?>)</th>
                                                <th>Subtotal  (<?php echo e($currency_symbol); ?>)</th>
                                                <th>Discount</th>
                                                <th>Total (<?php echo e($currency_symbol); ?>)</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="input-effect">
                                                        <select class="niceSelect w-100 bb form-control" name="Eproducts[]" id="Ereceived_product">
                                                            <option data-display="Select Service *" value="none">Select Service *</option>
                                                            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                        <span class="focus-border"></span>
                                                </div>
                                                </td>  
                                                <td> 
                                                    <div class="sla_service_papers_list" id="sla_service_papers_list"></div>
                                                </td>
                                                <td> 
                                                    <div class="sla_service_price_list text-right" id="sla_service_price_list"></div>
                                                </td> 
                                                <td>
                                                    <div class="total_price text-center" id="total_price"><h4 class="text-center" id="setTotalPrice"></h4></div> 
                                                </td>
                                                <td>
                                                    <div class="input-effect" id="CustomerDiscountDiv">
                                                        <select class="niceSelect w-72 bb form-control" name="discount" id="CustomerDiscount">
                                                            <option data-display="Select Discount" value="0">Select Discount</option>
                                                           
                                                        </select>
                                                        <span class="focus-border"></span>
                                                </div>
                                                </td>
                                                <td>
                                                    <input type="text"  class="service_payable_price" name="service_payable_price[]" id="service_total_price" style="border:none; background-color:transparent">
                                                </td>
                                                <td>
                                                        
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr> 
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><input class="primary-input form-control d-none"
                                                        type="number" step="any" id="Etotal" name="Etotal" autocomplete="off" readonly="true" value="0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr> 
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                               
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <tr> 
                                                
                                                <td></td>
                                                <td colspan="4" style="text-align:right">Grand Total:</td>
                                                <td>
                                                    <input class="primary-input form-control" type="number" step="any" id="Ebid_amount" name="Ebid_amount" autocomplete="off" readonly="true"> 
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td colspan="4" style="text-align:right"></td>
                                                <td>
                                                    <input class="primary-input form-control " type="number" step="any" name="paid_amount"  id="paid_amount">
                                                </td>    <td>
                                                    <div class="d-flex radio-btn-flex ml-40 ">
                                                        <div class="mr-30 ">
                                                            <input type="radio" name="pament_type" id="full_paid" value="F" class="common-radio relationButton" >
                                                            <label for="full_paid" class="pl-4">Full</label>
                                                        </div>
                                                        <div class="mr-30 ">
                                                            <input type="radio" name="pament_type" id="Partial" value="P" class="common-radio relationButton">
                                                            <label for="Partial" class="pl-4">Partial</label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        </tfoot>
                                    </table>

                                </div> 
                                
                                    
                                <div class="row mt-40">
                                    <div class="col-lg-12">
                                        <div class="student-details invoice-student-details">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item text-center">
                                                        <a class="nav-link active" href="#publicNotes" role="tab" data-toggle="tab"><?php echo app('translator')->getFromJson('lang.Public'); ?> <?php echo app('translator')->getFromJson('lang.notes'); ?></a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#privateNotes" role="tab" data-toggle="tab"><?php echo app('translator')->getFromJson('lang.Private'); ?> <?php echo app('translator')->getFromJson('lang.notes'); ?></a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#terms" role="tab" data-toggle="tab"><?php echo app('translator')->getFromJson('lang.Terms'); ?></a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#footer" role="tab" data-toggle="tab"><?php echo app('translator')->getFromJson('lang.Footer'); ?></a>
                                                    </li>
                                                    <li class="nav-item text-center">
                                                        <a class="nav-link" href="#signature" role="tab" data-toggle="tab"><?php echo app('translator')->getFromJson('lang.signature'); ?></a>
                                                    </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content invoice-tab-content">
                                                <!-- Start Profile Tab -->
                                                <div role="tabpanel" class="tab-pane fade  show active" id="publicNotes">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="public_note"></textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="privateNotes">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="private_note"></textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="terms">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="terms_note"></textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="footer">
                                                    <textarea class="primary-input form-control" cols="0" rows="4" name="footer_note"></textarea>
                                                </div>
                                                <!-- End Profile Tab -->
                                                <!-- Start Fees Tab -->
                                                <div role="tabpanel" class="tab-pane fade" id="signature">
                                                    <input type="text" name="signature_person" class="primary-input form-control" placeholder="person name">
                                                    <input type="text" name="signature_company" class="primary-input form-control" placeholder="company name">
                                                </div>
                                                <!-- End Profile Tab -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-40">
                                    <div class="col-lg-12 text-right">
                                        <button type="submit" class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            <?php if(isset($edit)): ?>
                                                <?php echo app('translator')->getFromJson('lang.update'); ?>
                                            <?php else: ?>
                                                <?php echo app('translator')->getFromJson('lang.save'); ?>
                                            <?php endif; ?>
                                             SLA
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>