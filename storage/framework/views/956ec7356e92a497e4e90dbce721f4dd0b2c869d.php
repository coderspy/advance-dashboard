<?php $__env->startSection('mainContent'); ?>
<?php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;

?> 
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> Assign Discount</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>">Dashboard</a>
                <a href="#">Assign Discount</a>
                <a href="#">Assign Discount List</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor up_st_admin_visitor pl_22">
    <div class="container-fluid p-0">
        <?php if(isset($editData)): ?> 
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="<?php echo e(url('assign-discount')); ?>" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    <?php echo app('translator')->getFromJson('lang.add'); ?>
                </a>
            </div>
        </div> 
        <?php endif; ?>
        <div class="row"> 
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30"><?php if(isset($editData)): ?>
                                    edit
                                <?php else: ?>
                                    Assign
                                <?php endif; ?>
                                    Discount
                            </h3>
                        </div>

                      <?php if(isset($editData)): ?>
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'assign-discount-update', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                        <input type="hidden" name="id" value="<?php echo e($editData->id); ?>">
                        <?php else: ?> 
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'store-assign-discount', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?> 
                        <?php endif; ?>
                     <div class="white-box">
                            <div class="add-visitor">
                                


                                 <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('service') ? ' is-invalid' : ''); ?>" name="service">
                                                <option data-display="Select Service *" value="">Select Service*</option>
                                                <?php $__currentLoopData = $service_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                
                                                <option value="<?php echo e($value->id); ?>" <?php echo e(@$editData->service_id==$value->id ?  'selected' :''); ?> ><?php echo e($value->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('service')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong><?php echo e($errors->first('service')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>  
                               
                                <div class="row mt-40">
                                    <div class="col-lg-12"> 
                                        <div class="input-effect">
                                            <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('discount') ? ' is-invalid' : ''); ?>" name="discount" id="">
                                                <option data-display="Select Discount*" value="">Select Discount *</option>
                                                <?php $__currentLoopData = $discount_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                
                                                <option value="<?php echo e($value->id); ?>" <?php echo e(@$editData->discount_id==$value->id ?  'selected' :''); ?>><?php echo e($value->title); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('discount')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong><?php echo e($errors->first('discount')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                     
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                         <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="">
                                            <span class="ti-check"></span>
                                            <?php echo e(isset($editData)? 'update':'save'); ?> Discount
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Discount List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead> 
                                <tr>
                                    <th>SL</th>
                                    <th>Discount Item</th>
                                    <th>Amount (<?php echo e($currency_symbol); ?>)</th>
                                    <th>Type</th>
                                    <th>Services</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $count=1; ?> 
                                <?php $__currentLoopData = $assign_discount_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $editData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($count++); ?></td>

                                   
                                    <td>
                                        <?php
                                        $discount = DB::table('discounts')->select('*')
                                                ->where('id', $editData->discount_id)
                                                ->first();
                                        if (!empty($discount)) {
                                            echo @$discount->title;
                                        }
                                        ?> 
                                         
                                   </td>
                                   <td><?php echo e(number_format(@$discount->amount, 2, '.', '')); ?></td>
                                   <td><?php if(@$discount->type=="F"): ?> Fixed <?php else: ?> Percentage <?php endif; ?></td>
                                    <td>
                                            <?php
                                        $service_name = DB::table('services')->select('name')
                                                ->where('id', $editData->service_id)
                                                ->first();
                                        if (!empty($service_name)) {
                                            echo $service_name->name;
                                        }
                                        ?> 


                                        
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                Select
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                               
                                               <a class="dropdown-item" href="<?php echo e(url('edit-assign-discount', [$editData->id])); ?>">Edit</a>
                                             
                                               <a class="dropdown-item" data-toggle="modal" data-target="#DeleteService<?php echo e($editData->id); ?>"
                                                    href="#">Delete </a>
                                            
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteService<?php echo e($editData->id); ?>" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete  Item</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to delete ? </h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal"><?php echo app('translator')->getFromJson('lang.cancel'); ?></button>
                                                     <?php echo e(Form::open(['url' => 'assign-discount-delete/'.$editData->id, 'method' => 'GET', 'enctype' => 'multipart/form-data'])); ?>

                                                    <button class="primary-btn fix-gr-bg" type="submit">DELETE</button>
                                                     <?php echo e(Form::close()); ?>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>