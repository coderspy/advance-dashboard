<?php $__env->startSection('mainContent'); ?> 

<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Reports/Pro Daily Services</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->getFromJson('lang.dashboard'); ?></a>
                <a href="<?php echo e(url('quotations')); ?>" class="active">Pro Daily Services</a> 
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
                      <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'search-services-pending-done', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                        <div class="row"> 

                  <div class="col-lg-4">
                        <div class="no-gutters input-right-icon">
                            <div class="col">
                                <div class="input-effect">
                                    <input class="primary-input date form-control<?php echo e($errors->has('from_date') ? ' is-invalid' : ''); ?>" id="from_date" type="text"
                                     name="from_date" value="<?php echo e(date('m/d/Y')); ?>">
                                    <span class="focus-border"></span>
                                    <label>From Date<span style="color:red;">*</span> </label>
                                    <?php if($errors->has('from_date')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('from_date')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button class="" type="button">
                                    <i class="ti-calendar" id="from_date"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="no-gutters input-right-icon">
                            <div class="col">
                                <div class="input-effect">
                                    <input class="primary-input date form-control<?php echo e($errors->has('to_date') ? ' is-invalid' : ''); ?>" id="to_date" type="text"
                                     name="to_date" value="<?php echo e(date('m/d/Y')); ?>">
                                    <span class="focus-border"></span>
                                    <label>To Date<span style="color:red;">*</span> </label>
                                    <?php if($errors->has('to_date')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('to_date')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button class="" type="button">
                                    <i class="ti-calendar" id="to_date"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg">
                                <span class="ti-search pr-2"></span>
                                Search
                            </button>
                        </div>
                    </div>
            <?php echo e(Form::close()); ?>

            </div>
        </div>
    </div>
        <div class="row">
         <div class="col-lg-12">
              
                <div class="row">
                    <div class="col-lg-12">
                    <div class="" style="margin-left: 22px; margin-top: 7px;">
                    <a href="<?php echo e(url('/services-pending-done-pdf/')); ?>" class="btn btn-info">Pdf</a>
                    <a class="btn btn-info" href="<?php echo e(URL::previous()); ?>">back</a>
                     </div>
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                               <?php if(session()->has('message-success') != "" ||
                                session()->get('message-danger') != ""): ?>
                                <tr>
                                    <td colspan="11">
                                         <?php if(session()->has('message-success')): ?>
                                          <div class="alert alert-success">
                                              <?php echo e(session()->get('message-success')); ?>

                                          </div>
                                        <?php elseif(session()->has('message-danger')): ?>
                                          <div class="alert alert-danger">
                                              <?php echo e(session()->get('message-danger')); ?>

                                          </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                 <?php endif; ?> 
                                <tr>
                                    <th>Sl </th>
                                    <th>Pro Id</th>
                                    <th>Pro user</th>
                                    <th>Date</th>
                                    <th>Services Approved</th>
                                    <th>Services Pending</th>
                                      
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count =1; ?>
                                <?php $__currentLoopData = $pro_daily_services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td ><?php echo e($count++); ?></td>
                                    <td>
                                         <?php
                                            $pro= DB::table('sm_staffs')->where('user_id',$value->created_by)->first();
                                            echo $pro->staff_no;
                                          ?>
                                    </td>
                                    <td >
                                       <?php
                                            $pro= DB::table('sm_staffs')->where('user_id',$value->created_by)->first();
                                            echo $pro->full_name;
                                          ?>

                                    </td>
                                    <td ><?php echo e(date('jS M, Y', strtotime($value->date))); ?></td>
 
                                    <td> 
                                        <?php if($value->is_approved == 1): ?> 
                                        <a style="color: darkgreen;">Approved</a>
                                        <?php else: ?>
                                         <a style="color: blue;">Null</a>
                                        <?php endif; ?>
                                    
                                    </td> 
                                      <td> 
                                        <?php if($value->is_approved == 0): ?> 
                                        <a style="color:red ;">Pending</a>
                                         <?php else: ?>
                                         <p style="color: blue;">Null</p>
                                        <?php endif; ?>
                                    
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>