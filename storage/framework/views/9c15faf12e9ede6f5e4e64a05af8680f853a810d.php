<?php $__env->startSection('mainContent'); ?> 
<style>
   table,
   thead,
   th,
   tr,
   td {
       color: #415094 !important;
       -webkit-print-color-adjust: exact;
   }

   @media  print {
       body {
           -webkit-print-color-adjust: exact;
       }
   }

   .purchaseInvoice {
       padding: 0px 25px !important;
   }

   .TotalAmount {
       background: linear-gradient(90deg, #7c32ff 0%, #c738d8 51%, #7c32ff 100%);
       padding: 5px 0px;
       color: white;
       letter-spacing: 2px;
       margin-top: 10px;
   }

   .quotation_view_table {
       width: 100%;
   }

   .quotation_view_50 {
       width: 50%;
   }

   .quotation_view_60 {
       width: 50%;
   }

   .quotation_view_table_tr_td {
       width: 50%;
       vertical-align: top;
   }

   .quotation_view_table_tr_td_h2 {
       background: #e9ecef !important;
       padding: 5px;
   }

   .quotation_view_table_tr_img {
       min-width: 160px;
       max-width: 160px;
       height: auto;
   }

   .shipment_qorkorder {
       width: 60%;
       padding-right: 10%;
       vertical-align: top;
   }

   .shipment_qorkorder_td {
       width: 20%;
       vertical-align: top;
   }
   </style>
<?php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
   
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; } 

    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo; 
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
?> 
<link rel="stylesheet" href="<?php echo e(asset('/public/css/quotationView.css')); ?>">
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>SLA <?php echo app('translator')->getFromJson('lang.details'); ?></h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->getFromJson('lang.dashboard'); ?></a>
                <a href="#">SLA </a>
                <a href="#">SLA <?php echo app('translator')->getFromJson('lang.details'); ?></a>
            </div>
        </div>
    </div>
</section>


<section class="admin-visitor-area">
<div class="container-fluid p-0">
    <div class="row">
            <div class="offset-lg-2 col-lg-8">
                <div class="white-box">
                   <div class="row mt-40">
                        <div class="col-lg-12"> 

                            <div class="row" id="purchaseInvoice">
                                <div class="container-fluid">
                                    <div class="row mb-20">
                                        <div class="col-lg-12">
                                            <table class="quotation_view_table"  style="width:100%;">
                                                <tr>
                                                    <td class="quotation_view_table_tr_td"> 
                                                        <div class="col-lg-12 ">
                                                            <img src="<?php echo e(asset($logo)); ?>"  class="quotation_view_table_tr_img" style="width:200px !important">
                                                            <div class="business-info text-left">
                                                                <h3 class="mt-10 primary-color"><?php echo e($generalSetting->company_name); ?></h3>
                                                                <p class="mt-0 primary-color" class="quotation_view_50"><?php echo e($generalSetting->address); ?></p>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="quotation_view_50 p-0" class="primary-color"> 
                                                        <div class="col-lg-12 ">
                                                            <div class="invoice-details-right">
                                                                <h2 class="text-uppercase text-center quotation_view_table_tr_td_h2" >SLA <?php echo app('translator')->getFromJson('lang.details'); ?></h2>                                                
                                                               
                                                                <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">SLA <?php echo app('translator')->getFromJson('lang.no_'); ?>:</p>
                                                                    <p class="text-left  primary-color"><?php echo e(@$sla->number); ?></p>
                                                                </div>
                                                                <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">SLA <?php echo app('translator')->getFromJson('lang.date'); ?>:</p>
                                                                    <p class="text-left  primary-color"><?php echo e(date('jS M, Y', strtotime(@$sla->created_at))); ?></p>
                                                                </div>
                                                                <div class="d-flex  invoice-details-content">
                                                                    <p class="fw-500 primary-color">SLA <?php echo app('translator')->getFromJson('lang.reference'); ?></p>
                                                                    <p class="text-left  primary-color"><?php echo e(@$sla->reference); ?></p>
                                                                </div> 
                                                                <h2 class="text-uppercase text-center TotalAmount" ><?php echo app('translator')->getFromJson('lang.total'); ?> <?php echo app('translator')->getFromJson('lang.amount'); ?> <?php echo e($currency_symbol); ?><?php echo e(@$sla->paybale_amount); ?> </h2> 
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>  


                                    <hr>
             

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="quotation_view_table">
                                                <tr>
                                                    <td class="quotation_view_50">
                                                                    
                                                         <?php if(!empty($sla->customer )): ?>  
                                                        <div class="col-lg-12 ">
                                                            <div class=" primary-color">
                                                                <h5 class="primary-color"><?php echo app('translator')->getFromJson('lang.Bill_To'); ?>:</h5>
                                                            </div>

                                                            <div class=" primary-color">
                                                                <h5 class="primary-color"><?php echo e($sla->customer != ""? $sla->customer->full_name : ''); ?></h5>
                                                                <p class="primary-color quotation_view_60" ><?php echo e($sla->customer != ""? $sla->customer->current_address : ''); ?></p>
                                                            </div>
                                                        </div>
                                                        <?php endif; ?>

                                                    </td>
                                                    <td class="quotation_view_50">
                                                         <?php if(!empty($sla->vendor_id )): ?>     
                                                        <div class="col-lg-12 ">
                                                            <div class=" primary-color">
                                                                <h5 class="primary-color"><?php echo app('translator')->getFromJson('lang.vendor'); ?>:</h5>
                                                            </div>

                                                            <div class=" primary-color"> 
                                                                <h5 class="primary-color"> Vendor Name</h5>
                                                                <p class="primary-color quotation_view_60" > Vendor Address</p> 
                                                            </div>
                                                        </div>
                                                        <?php endif; ?>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="col-lg-12 ">
                                                            <?php $tem=App\SlaTemplate::where('active_status',1)->first(); ?>
                                                            <p class="primary-color mt-40"><?=$tem->message;?></p>
                                                            <p class="primary-color mt-40"> <?php echo e($sla->description); ?>   </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <?php echo e(@$value->title); ?>



                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row mt-30 mb-50">
                                        <div class="col-lg-12">
                                            <table class="d-table table-responsive custom-table" cellspacing="0" width="100%" >
                                                <thead>
                                                    <tr>
                                                        <th class="primary-color text-left"><?php echo app('translator')->getFromJson('lang.sl'); ?></th>  
                                                        <th class="primary-color text-left">Service <?php echo app('translator')->getFromJson('lang.name'); ?> </th>   
                                                        <th class="primary-color text-right"><?php echo app('translator')->getFromJson('lang.sale'); ?> <?php echo app('translator')->getFromJson('lang.price'); ?> (<?php echo e($currency_symbol); ?>)</th> 
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php $grand_total = 0; $sub_total = 0; $total_subtotal=0; $count=1; 
                                                
                                                
                                                $AllServices= DB::table('sla_services')->where('sla_id', $sla->id )
                                                ->join('services', 'services.id', 'sla_services.service_id')
                                                // ->where('services.id', $sla->id)
                                                ->get();
                                             
                                                ?>

                                                <?php $__currentLoopData = $AllServices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <?php     

                                                $AllPrice= DB::table('service_assign_prices')
                                                ->join('service_prices', 'service_prices.id', 'service_assign_prices.service_price_id')
                                                ->where('service_assign_prices.service_id',$value->service_id)
                                                ->sum('price');
                                                

                                                    $total_subtotal = $total_subtotal+ $AllPrice;

                                                ?>

                                                <tr>
                                                  <td><?php echo e($count++); ?></td>
                                                  <td class="primary-color text-left">
                                                    
                                                      <?php echo e(@$value->name); ?>

                                                      <?php
                                                          $service_price_info=DB::table('service_assign_prices')
                                                          ->where('service_assign_prices.service_id',$value->id)
                                                          ->join('service_prices','service_prices.id','=','service_assign_prices.service_price_id')
                                                          ->get();
                                                         
                                                      ?>
                                                      <ul>
                                                            <?php $__currentLoopData = $service_price_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sla_service_price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            
                                                                <li><?php echo e(@$sla_service_price->title); ?> [<?php echo e(@$sla_service_price->price); ?>]</li>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </ul>
                                                     
                                                    </td>  
                                                <td class="primary-color text-right"><?php echo e($currency_symbol); ?><?php echo e(@$sla->paybale_amount); ?></td>  
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      <tr>   
                                                        <td></td>
                                                        <td class="fw-600 primary-color text-right"><?php echo app('translator')->getFromJson('lang.sub'); ?> <?php echo app('translator')->getFromJson('lang.total'); ?> (<?php echo e($currency_symbol); ?>) </td>
                                                        <td class="fw-600 primary-color text-right">
                                                            <?php echo e(App\User::NumberToBangladeshiTakaFormat( $sla->paybale_amount)); ?>

                                                        </td>
                                                    </tr>
                                                    <tr>   
                                                        <td></td>
                                                        
                                                        <td class="fw-600 primary-color text-right">Paid  (<?php echo e($currency_symbol); ?>)</td>
                                                        <td class="fw-600 primary-color text-right">
                                                        <?php echo e($sla->paid_amount != 0?  App\User::NumberToBangladeshiTakaFormat($sla->paid_amount): "0.00"); ?>  
                                                        </td>
                                                    </tr>
                                                    <tr>   
                                                        <td></td>
                                                        <td class="fw-600 primary-color text-right">Due  (<?php echo e($currency_symbol); ?>)</td>
                                                        <td class="fw-600 primary-color text-right">
                                                            <?php echo e(App\User::NumberToBangladeshiTakaFormat($sla->paybale_amount-$sla->paid_amount)); ?> 
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                                     <div class="row mb-20">
                                        <div class="col-lg-12">
                                            <?php if(!empty($sla->note)): ?>
                                            <p class="primary-color"><?php echo e($sla->note); ?></p>
                                            <hr>
                                            <?php endif; ?>
                                        </div>
                                    </div>
 
                                    <div class="row mt-40">
                                        <div class="col-lg-12 text-center">
                                            <button class="primary-btn fix-gr-bg" onclick="javascript:printDiv('purchaseInvoice')" id="printButton"><?php echo app('translator')->getFromJson('lang.print'); ?></button>

                                             <a class=" btn btn-danger"  href="<?php echo e(URL::previous()); ?>">Back</a>
                                        </div>
                                      </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>








  










<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>