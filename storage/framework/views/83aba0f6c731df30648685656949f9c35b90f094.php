<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1><?php echo app('translator')->getFromJson('lang.user_log'); ?> Report</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->getFromJson('lang.dashboard'); ?></a>
                <a href="#"><?php echo app('translator')->getFromJson('lang.reports'); ?></a>
                <a href="#"><?php echo app('translator')->getFromJson('lang.user_log'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
                              <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'search-user-log', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                        <div class="row"> 

                  <div class="col-lg-4">
                        <div class="no-gutters input-right-icon">
                            <div class="col">
                                <div class="input-effect">
                                    <input class="primary-input date form-control<?php echo e($errors->has('from_date') ? ' is-invalid' : ''); ?>" id="from_date" type="text"
                                     name="from_date" value="<?php echo e(date('m/d/Y')); ?>">
                                    <span class="focus-border"></span>
                                    <label>From Date<span style="color:red;">*</span> </label>
                                    <?php if($errors->has('from_date')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('from_date')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button class="" type="button">
                                    <i class="ti-calendar" id="from_date"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="no-gutters input-right-icon">
                            <div class="col">
                                <div class="input-effect">
                                    <input class="primary-input date form-control<?php echo e($errors->has('to_date') ? ' is-invalid' : ''); ?>" id="to_date" type="text"
                                     name="to_date" value="<?php echo e(date('m/d/Y')); ?>">
                                    <span class="focus-border"></span>
                                    <label>To Date<span style="color:red;">*</span> </label>
                                    <?php if($errors->has('to_date')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('to_date')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button class="" type="button">
                                    <i class="ti-calendar" id="to_date"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg">
                                <span class="ti-search pr-2"></span>
                                Search
                            </button>
                        </div>
                    </div>
            <?php echo e(Form::close()); ?>

            </div>
        </div>
    </div>
            

            <div class="row mt-40">
                <div class="col-lg-12">
                    
                   
                    <div class="row">
                         <div class="pull-right" style="margin-left: 22px; margin-top: 7px;">
                                    <a href="<?php echo e(url('/user-log-pdf/')); ?>" class="btn btn-info">Pdf</a>
                                    <a class="btn btn-info" href="<?php echo e(URL::previous()); ?>">back</a>
                                </div>
                        <div class="col-lg-12">
                            <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Role</th>
                                        <th>Ip Address</th>
                                        <th>Mac Address</th>
                                        <th>Login Time</th>
                                        <th>User Agent</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $__currentLoopData = $user_logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($user_log->user!=""?$user_log->user->username:""); ?></td>
                                        <td><?php echo e($user_log->role!=""?$user_log->role->name:""); ?></td>
                                        <td><?php echo e($user_log->ip_address); ?></td>
                                        <td><?php echo e($user_log->mac_address); ?></td>
                                        <td  data-sort="<?php echo e(strtotime($user_log->created_at)); ?>" >
                                            <?php echo e($user_log->created_at != ""? App\SmGeneralSettings::DateConvater($user_log->created_at):''); ?>


                                        </td>
                                        <td><?php echo e($user_log->user_agent); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


    </div>
</section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>