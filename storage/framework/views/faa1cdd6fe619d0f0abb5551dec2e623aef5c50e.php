<?php $__env->startSection('mainContent'); ?> 
<?php   
    $generalSetting=App\SmGeneralSettings::where('id',1)->first();
    $currency_symbol = $generalSetting->currency_symbol;
    if(isset($generalSetting->logo)){  $logo = $generalSetting->logo;  }
    else{ $logo = 'public/uploads/settings/logo.png'; } 

    $sm_staff= App\SmStaff::where('user_id',Auth::user()->id)->first();
   
    if(!empty($sm_staff)){
        $profile_image = $sm_staff->staff_photo; 
        if(empty($profile_image)){
            $profile_image ='public/uploads/staff/staff1.jpg';
        }
    }
?> 
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>SLA List</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->getFromJson('lang.dashboard'); ?></a>
                <a href="<?php echo e(url('quotations')); ?>" class="active">SLA List</a> 
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area">
    <div class="container-fluid p-0">
         
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="<?php echo e(url('create-sla')); ?>" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    <?php echo app('translator')->getFromJson('lang.add'); ?>  <?php echo app('translator')->getFromJson('lang.new'); ?>
                </a>
            </div>
        </div> 
        
        <div class="row">

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">SLA List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                               <?php if(session()->has('message-success') != "" ||
                                session()->get('message-danger') != ""): ?>
                                <tr>
                                    <td colspan="11">
                                         <?php if(session()->has('message-success')): ?>
                                          <div class="alert alert-success">
                                              <?php echo e(session()->get('message-success')); ?>

                                          </div>
                                        <?php elseif(session()->has('message-danger')): ?>
                                          <div class="alert alert-danger">
                                              <?php echo e(session()->get('message-danger')); ?>

                                          </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                 <?php endif; ?> 
                                <tr>
                                    <th>Sl</th>
                                    <th>Number</th>
                                    <th>Reference</th>
                                    <th>Created Date</th>
                                    <th>Expire Alert Date</th>
                                    <th>Customer No</th>
                                    <th>Customer name</th>
                                    <th>Service</th>
                                    <th><?php echo app('translator')->getFromJson('lang.payment'); ?> <?php echo app('translator')->getFromJson('lang.status'); ?></th>
                                    <th>Paybale amount (<?php echo e($currency_symbol); ?>)</th> 
                                    <th>Paid amount (<?php echo e($currency_symbol); ?>)</th> 

                                    <th>status</th>
                                    
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count =1; ?>
                                <?php $__currentLoopData = $slas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <tr>
                                    <td ><?php echo e($count++); ?></td>
                                    <td ><?php echo e($value->number); ?></td>
                                    <td ><?php echo e($value->reference); ?></td> 
                                    <td ><?php echo e(date('jS M, Y', strtotime($value->created_at))); ?></td>
                                    <td ><?php echo e(date('jS M, Y', strtotime($value->date))); ?></td>
                                    <td ><?php echo e($value->customer->staff_no); ?></td> 
                                    <td ><?php echo e($value->customer->first_name); ?></td>

                                    <td >

                                     <?php 
                                        $sla_services= DB::table('sla_services')->where('sla_id',$value->id)->get();
                                        foreach($sla_services as $services){
                                            $service_details=DB::table('services')->where('id',$services->service_id)->first();
                                          
                                            echo $service_details->name .'<br>';
    
                                           }
                                        
                                        ?>

                                    </td> 
                                    <td ><?php echo e($value->payment_status); ?></td> 
                                    <td ><?php echo e(App\User::NumberToBangladeshiTakaFormat($value->paybale_amount)); ?></td>  
                                    <td ><?php echo e($value->paid_amount); ?></td>  

                                  <?php if(Auth::user()->role_id==1): ?>   

                                    

                                    <td> 
                                        <?php if($value->is_approved==0): ?> 
                                    <a data-toggle="modal"   href="#" data-target="#approveStatus<?php echo e($value->id); ?>" class="primary-btn small tr-bg">Pending</a>
                                        <?php else: ?>
                                    <a data-toggle="modal"   href="#" data-target="#approveStatus<?php echo e($value->id); ?>" class="primary-btn fix-gr-bg small">Approved</a>
                                        <?php endif; ?>
                                    
                                    </td>

                                <?php else: ?>
                                    <td> 
                                        <?php if($value->is_approved==0): ?> 
                                          <a style="color:red;">Pending</a>
                                        <?php else: ?>
                                         <a style="color: darkgreen;">Approved</a>
                                        <?php endif; ?>
                                    
                                    </td>
                                <?php endif; ?>

                                    <td >
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                <?php echo app('translator')->getFromJson('lang.select'); ?>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">  
                                                <a class="dropdown-item" href="<?php echo e(url('sla-list-view', [$value->id])); ?>"><?php echo app('translator')->getFromJson('lang.view'); ?></a> 
                                                <a class="dropdown-item" href="<?php echo e(url('sla-edit', [$value->id])); ?>"><?php echo app('translator')->getFromJson('lang.edit'); ?></a> 

                                            <?php
                                                $due=$value->paybale_amount-$value->paid_amount;
                                            ?>
                                              <?php if($value->is_approved==1): ?> 
                                                <?php if($due>0): ?>
                                                    <a class="dropdown-item" href="<?php echo e(url('sla-payment', [$value->id])); ?>">Payment</a> 

                                                <?php endif; ?>
                                                <?php endif; ?>

                                                <a class="dropdown-item" data-toggle="modal" data-target="#deletequotations<?php echo e($value->id); ?>"  href="#"><?php echo app('translator')->getFromJson('lang.delete'); ?> </a> 
                                           
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                  <div class="modal fade admin-query" id="deletequotations<?php echo e($value->id); ?>" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title"><?php echo app('translator')->getFromJson('lang.delete'); ?> SLA</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4><?php echo app('translator')->getFromJson('lang.are_you_sure_to_delete'); ?></h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal"><?php echo app('translator')->getFromJson('lang.cancel'); ?>
                                                        </button>

                                                        <a href="<?php echo e(url('sla-list-delete', [$value->id])); ?>"
                                                           class="primary-btn fix-gr-bg"><?php echo app('translator')->getFromJson('lang.delete'); ?></a>

                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="modal fade admin-query" id="approveStatus<?php echo e($value->id); ?>" >
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    <?php if(@$value->is_approved==0): ?>
                                                        Make Approve
                                                    <?php else: ?>
                                                        Make Pending
                                                    <?php endif; ?>
                                                    
                                                  SLA</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>Are you sure to 
                                                        <?php if(@$value->is_approved==0): ?>
                                                        Make Approve
                                                    <?php else: ?>
                                                        Make Pending
                                                    <?php endif; ?></h4>
                                                </div>

                                                <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal"><?php echo app('translator')->getFromJson('lang.cancel'); ?>
                                                        </button>
                                                            <?php
                                                            if (@$value->is_approved==0) {
                                                                $status=' Make Approve';
                                                            } else {
                                                            $status='Make Pending';
                                                            }

                                                                
                                                            ?>
                                                        <a href="<?php echo e(url('sla/approve', [$value->id])); ?>"
                                                        class="primary-btn fix-gr-bg"><?php echo e($status); ?></a>

                                                    </div>
                                                    
                                                     
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
     $(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    });</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>