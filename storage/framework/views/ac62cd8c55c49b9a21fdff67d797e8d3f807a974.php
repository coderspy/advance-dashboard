<?php
    if (Auth::user() == "") {
        header('location:' . url('/login'));
        exit();
    }

    $staffs = App\SmStaff::where('user_id', Auth::user()->id )->first();
        $modules = [];
        $module_links = [];

        $permissions = App\SmRolePermission::where('staff_id',$staffs->id)->get();
        foreach ($permissions as $permission) {
            $module_links[] = $permission->module_link_id;
            $modules[] = $permission->moduleLink->module_id;
        }
?>



<input type="hidden" name="url" id="url" value="<?php echo e(url('/')); ?>">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="<?php echo e(url('/login')); ?>">
            <img src="<?php echo e(asset($generalSetting->logo)); ?>" alt="">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>

    <ul class="list-unstyled components"> 
        
        <?php if(Auth::user()->role_id==1): ?> 
            <li>
                <a href="<?php echo e(url('/admin-dashboard')); ?>" id="admin-dashboard">

                    <span class="flaticon-speedometer"></span>
                    dashboard
                </a>
            </li> 
            <li>
                <a href="#services" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Services
                </a>
                <ul class="collapse list-unstyled" id="services"> 
                    <li>
                        <a href="<?php echo e(route('categoryView')); ?>">Category</a>
                    </li> 
                       <li>
                        <a href="<?php echo e(url('services/papers-list')); ?>">Papers List</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('services/prices-list')); ?>">Price List</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('service-list')); ?>">Create Services</a>
                    </li>
                 
                </ul>
            </li>

            <li>
                <a href="#subMenuHumanResource" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Human Resource
                </a>
                <ul class="collapse list-unstyled" id="subMenuHumanResource"> 
                        
                        <li><a href="<?php echo e(url('department')); ?>">Department</a></li>
                        <li><a href="<?php echo e(url('designation')); ?>">Designation</a></li>  
                        <li><a href="<?php echo e(route('staff_directory')); ?>">Users </a></li>   
                        <li><a href="<?php echo e(route('role')); ?>">role</a></li>
                        <li><a href="<?php echo e(url('customer-directory')); ?>">Customers</a></li>    
                               
                </ul>
            </li> 
            
            <li>
                <a href="#subMenuDiscount" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle"> <span class="flaticon-consultation"></span>  Discount
                </a>
                <ul class="collapse list-unstyled" id="subMenuDiscount"> 
                    <li><a href="<?php echo e(url('discount')); ?>"> Create Discount</a></li> 
                    <li><a href="<?php echo e(url('assign-discount')); ?>"> Assign Discounts</a></li>  
                    
                </ul>
            </li> 
            
            <li>
                <a href="#subMenuSla" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle"> <span class="flaticon-consultation"></span>  SLA
                </a>
                <ul class="collapse list-unstyled" id="subMenuSla"> 
                    <li><a href="<?php echo e(url('create-sla-template')); ?>">SLA & Quotation Template</a></li> 
                    <li><a href="<?php echo e(url('create-sla')); ?>">Create SLA</a></li> 
                    <li><a href="<?php echo e(url('sla-list')); ?>"> SLA List</a></li>  
                    
                </ul>
            </li> 

            <li>
                <a href="#subMenuQuotation" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-book"></span> Quotations  </a>
                <ul class="collapse list-unstyled" id="subMenuQuotation"> 
                    <li><a href="<?php echo e(url('quotations/create')); ?>">New Quotations</a></li> 
                    <li><a href="<?php echo e(url('quotations')); ?>">Quotations List</a></li> 
                </ul>
            </li>
  
            <li>
                <a href="#subMenuAccount" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-accounting"></span>
                    <?php echo app('translator')->getFromJson('lang.accounts'); ?>
                </a>
                <ul class="collapse list-unstyled" id="subMenuAccount"> 
                        <li>
                            <a href="<?php echo e(url('chart-of-account')); ?>"> <?php echo app('translator')->getFromJson('lang.chart_of_account'); ?></a>
                        </li> 
                        <li>
                            <a href="<?php echo e(route('payment_method')); ?>"> <?php echo app('translator')->getFromJson('lang.payment_method'); ?></a>
                        </li> 
                        <li>
                            <a href="<?php echo e(url('bank-account')); ?>"> <?php echo app('translator')->getFromJson('lang.bank_account'); ?></a>
                        </li> 
                        <li>
                            <a href="<?php echo e(route('add_income')); ?>"> <?php echo app('translator')->getFromJson('lang.income'); ?></a>
                        </li> 
                        <li>
                            <a href="<?php echo e(route('profit')); ?>"> <?php echo app('translator')->getFromJson('lang.profit'); ?></a>
                        </li> 
                        <li>
                            <a href="<?php echo e(url('add-expense')); ?>"> <?php echo app('translator')->getFromJson('lang.expense'); ?></a>
                        </li> 
                        <li>
                            <a href="<?php echo e(route('search_account')); ?>"> <?php echo app('translator')->getFromJson('lang.search'); ?></a>
                        </li> 
                    
                </ul>
            </li> 




            
            <li>
                <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-email"></span>
                    Communication
                </a>
                <ul class="collapse list-unstyled" id="subMenuCommunicate"> 
                    
                    <li><a href="<?php echo e(url('notice-list')); ?>">Notice Board</a></li> 
                    <li><a href="<?php echo e(url('send-email-sms-view')); ?>">Send Email</a></li> 
                    <li><a href="<?php echo e(url('email-sms-log')); ?>">Email Logs</a></li>  
                    <li> 
                        <a href="<?php echo e(route('user.chats')); ?>">
                            <span class="ti-user"></span>Chatting
                        </a> 
                    </li>

                </ul>
            </li>  
            




            <li>
                <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-analysis"></span> Reports
                </a>
                <ul class="collapse list-unstyled" id="subMenusystemReports"> 
                    <li><a href="<?php echo e(url('reports/pro-user-list')); ?>">PRO Users</a></li>  
                 
                    <li><a href="<?php echo e(url('reports/services-list')); ?>">Services</a></li>  
                    <li><a href="<?php echo e(url('reports/daily-services-report')); ?>">Daily Services Report</a></li>      
                    <li><a href="<?php echo e(url('reports/customer-monthly-services')); ?>">All Services Report</a></li> 
                    <li><a href="<?php echo e(url('reports/services-pending-done')); ?>">services pending/done</a></li>  
                    <li><a href="<?php echo e(route('user_log')); ?>">user log</a></li>
                </ul>
            </li>
            
            
            <li>
                <a href="#subMenusystemSettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-settings"></span> System Settings
                </a>
                <ul class="collapse list-unstyled" id="subMenusystemSettings">
                    <li><a href="<?php echo e(url('general-settings')); ?>"> General Settings</a></li>
                    <li><a href="<?php echo e(url('email-settings')); ?>">Email Settings</a></li> 
                  
                    <li> <a href="<?php echo e(route('base_group')); ?>">Base Group</a></li>
                    <li><a href="<?php echo e(route('base_setup')); ?>">Base Setup</a></li>  
                    <li><a href="<?php echo e(url('backup-settings')); ?>">Backup Settings</a></li>
                     <li>
                        <a href="<?php echo e(url('sms-settings')); ?>"><?php echo app('translator')->getFromJson('lang.sms_settings'); ?></a>
                    </li>
                </ul>
            </li> 

            
            <li>
                <a href="#subMenufrontEndSettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-software"></span> Web Settings
                </a>
                <ul class="collapse list-unstyled" id="subMenufrontEndSettings">
                    <li><a href="<?php echo e(url('admin-home-page')); ?>"> Home Page</a></li>
                    <li><a href="<?php echo e(url('news')); ?>">News list</a></li>
                    <li><a href="<?php echo e(url('news-category')); ?>">News Category</a></li>
                    <li><a href="<?php echo e(url('testimonial')); ?>">Testimonial</a></li> 
                    <li><a href="<?php echo e(url('contact-page')); ?>">Contact</a></li>
                    <li><a href="<?php echo e(url('contact-message')); ?>">contact message</a></li>
                    <li><a href="<?php echo e(url('about-page')); ?>"> about us</a></li>
                    <li><a href="<?php echo e(url('custom-links')); ?>"> custom links</a></li>
                </ul>
            </li>
            
 

 
            <?php endif; ?>

            <?php if(Auth::user()->role_id==2): ?> 
            <li>
                <a href="<?php echo e(url('/admin-dashboard')); ?>" id="admin-dashboard">

                    <span class="flaticon-speedometer"></span>
                    dashboard 
                </a>
            </li> 
            <?php endif; ?> 

            
            <?php if(Auth::user()->role_id==3): ?>
              <?php if(in_array(1, $modules)): ?> 
                <?php if(in_array(1, $module_links)): ?> 

            <li>
                <a href="<?php echo e(url('/admin-dashboard')); ?>" id="admin-dashboard">

                    <span class="flaticon-speedometer"></span>
                    dashboard 
                </a>
            </li> 
             <?php endif; ?>  
            <li>
                <a href="#subMenuHumanResource" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Human Resource
                </a>
                <ul class="collapse list-unstyled" id="subMenuHumanResource">  
                <?php if(in_array(2, $module_links)): ?>      
                    <li>
                        <a href="<?php echo e(url('add-customer')); ?>"> Add Customer</a>
                    </li> 
                <?php endif; ?>    
                </ul>
            </li> 
            <?php endif; ?>
          <?php if(in_array(2, $modules)): ?>
            <li>
                <a href="#ServicesMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Services
                </a>
                <ul class="collapse list-unstyled" id="ServicesMenu"> 
             
                    <?php if(in_array(3, $module_links)): ?>
                        <li><a href="<?php echo e(url('create-sla-template')); ?>">SLA Template</a></li> 
                    <?php endif; ?> 
                    
                      <?php if(in_array(3, $module_links)): ?> <li><a href="<?php echo e(url('create-sla')); ?>">Create SLA</a></li>  <?php endif; ?> 

                      <?php if(in_array(4, $module_links)): ?> <li><a href="<?php echo e(url('sla-list')); ?>"> SLA List</a></li>  <?php endif; ?>   
                   
                     <?php if(in_array(5, $module_links)): ?> <li><a href="<?php echo e(url('quotations/create')); ?>">New Quotations</a></li>  <?php endif; ?> 
                       <?php if(in_array(6, $module_links)): ?> <li><a href="<?php echo e(url('quotations')); ?>">Quotations List</a></li>   <?php endif; ?> 
                </ul>
            </li> 
             <?php endif; ?>

             <?php if(in_array(3, $modules)): ?>
             <li>
                <a href="#NotificationMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Notifications
                </a>
                <ul class="collapse list-unstyled" id="NotificationMenu"> 
               
                  <?php if(in_array(7, $module_links)): ?> 
                     <li>
                        <a href="<?php echo e(url('sms-settings')); ?>">Sms Notification</a>
                    </li>  
                  <?php endif; ?>      
                </ul>
            </li> 
          <?php endif; ?>
            
            <?php if(in_array(4, $modules)): ?>
              <li>
                <a href="#ReportMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Report
                </a>
                <ul class="collapse list-unstyled" id="ReportMenu">  
                    <?php if(in_array(8, $module_links)): ?>
                        <li>
                            <a href="<?php echo e(url('reports/daily-services-report')); ?>">Daily Services Report</a>
                        </li> 
                    <?php endif; ?>
                    <?php if(in_array(9, $module_links)): ?>
                     <li>
                        <a href="<?php echo e(url('reports/services-pending-done')); ?>">Services Pending/Done</a>
                    </li>  
                    <?php endif; ?>
                    <?php if(in_array(10, $module_links)): ?>
                     <li>
                        <a href="<?php echo e(url('reports/customer-monthly-services')); ?>">Montly Services Report</a>
                    </li>  
                    <?php endif; ?>  
                </ul>
            </li> 
            <?php endif; ?>

            <?php if(in_array(5, $modules)): ?>

             <li>
                <a href="#ServicesMenu" data-toggle="collapse" aria-expanded="false"
                    class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    Communication
                </a>
                <ul class="collapse list-unstyled" id="ServicesMenu">  
                    <?php if(in_array(11, $module_links)): ?>


                    <li> 
                        <a href="<?php echo e(route('user.chats')); ?>">
                            <span class="ti-user"></span>Chatting
                        </a> 
                    </li> 

                    <?php endif; ?> 

                </ul>
            </li> 
            <?php endif; ?> 


            <?php endif; ?> 
    </ul>
</nav>
