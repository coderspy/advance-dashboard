<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmPaymentGatewaySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_payment_gateway_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gateway_name')      ->nullable();
            $table->string('gateway_username')  ->nullable();
            $table->string('gateway_password')  ->nullable();
            $table->string('gateway_signature') ->nullable();
            $table->string('gateway_client_id') ->nullable();
            $table->string('gateway_mode')      ->nullable(); 
            $table->string('gateway_secret_key')->nullable();
            $table->string('gateway_secret_word')->nullable();
            $table->string('gateway_publisher_key')->nullable();
            $table->string('gateway_private_key')->nullable();
            $table->tinyInteger('active_status')->default(0); 
            $table->timestamps();
            $table->integer('created_by')->nullable()->default(1)->unsigned();
            $table->integer('updated_by')->nullable()->default(1)->unsigned();
        });

        DB::table('sm_payment_gateway_settings')->insert([
            [
                'gateway_name'          => 'Stripe',
                'gateway_username'      => 'demo@strip.com',
                'gateway_password'      => '12334589',
                'gateway_client_id'     => '',
                'gateway_secret_key'    => 'AVZdghanegaOjiL6DPXd0XwjMGEQ2aXc58z1-isWmBFnw1h2j',
                'gateway_secret_word'   => 'AVZdghanegaOjiL6DPXd0XwjMGEQ2aXc58z1',
                'created_at' => date('Y-m-d h:i:s'),
                
            ]

        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_payment_gateway_settings');
    }
}
