<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->string('type')->default('System');
            $table->tinyInteger('user_id')->default();
            $table->tinyInteger('active_status')->default(1);
            $table->string('created_by')->nullable()->default(1);
            $table->string('updated_by')->nullable()->default(1);
            $table->timestamps();
        }); 

        
        DB::table('roles')->insert([
            [
                'name' => 'Super admin',   
                'type' => 'System', 
                'created_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Admin',   
                'type' => 'System', 
                'created_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'PRO User',  
                'type' => 'System', 
                'created_at' => date('Y-m-d h:i:s')
            ],
            [
                'name' => 'Customers',  
                'type' => 'System', 
                'created_at' => date('Y-m-d h:i:s')
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
