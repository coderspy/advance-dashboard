<?php

use App\SlaServices;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlaServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('sla_services', function (Blueprint $table) {
            $table->increments('id');
            $table->double('govt_price', 15, 2)->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();


            $table->integer('sla_id')->nullable()->unsigned();
            $table->foreign('sla_id')->references('id')->on('customer_slas')->onDelete('restrict');

            $table->integer('service_id')->nullable()->unsigned();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('restrict');

            $table->integer('discount_id')->nullable()->unsigned();
            $table->foreign('discount_id')->references('id')->on('discounts')->onDelete('restrict');
        });

 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sla_services');
    }
}
