<?php

use App\SmStaff;
use App\CustomerSla;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerSlasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_slas', function (Blueprint $table) {
            $table->increments('id');
            
            
            $table->string('sla_type',255)->nullable();
            $table->string('title',255)->nullable();
            $table->string('customer_type',255)->nullable(); 
            $table->string('number',255)->nullable(); 
            $table->date('date')->nullable();
            $table->integer('days')->nullable();
            $table->string('reference',255)->nullable();


            // $table->string('service_id');
            $table->string('customer_id')->nullable();
            $table->string('customer_name',255)->nullable();
 

            $table->integer('discount_id')->nullable()->unsigned();
            $table->foreign('discount_id')->references('id')->on('discounts')->onDelete('restrict');

            // $table->double('amount', 15, 2);
            // $table->double('discount_amount', 15, 2)->nullable();
            // $table->enum('discount_type', ['P', 'A'])->nullable()->comment('P = percentage, A= amount'); 
            $table->double('tax_amount', 15, 2)->nullable(); 

            $table->double('total_amount', 15, 2)->nullable(); 
            $table->double('paybale_amount', 15, 2)->nullable(); 
            $table->double('paid_amount', 15, 2)->nullable();
 


            $table->enum('payment_status',  ['UP', 'P', 'PP', 'PR'] )->comment('UP= UNPAID , P= PAID , PP= PARTIALLY PAID, PR= PROFORMA');
            $table->double('partial_paymemt', 15, 2)->nullable();


            $table->text('note')->nullable();
            $table->text('description')->nullable();

            $table->text('private_note')->nullable();
            $table->text('public_note')->nullable();
            $table->text('terms_note')->nullable();
            $table->text('footer_note')->nullable();
            $table->string('signature_person',255)->nullable();
            $table->string('signature_company',255)->nullable(); 
            $table->tinyInteger('is_approved')->default(0)->comment('0 = no, 1= yes');
            $table->tinyInteger('active_status')->default(1);
            $table->string('created_by')->nullable()->default(1);
            $table->string('updated_by')->nullable()->default(1);
            $table->timestamps();
        });


        

        // $sla = new CustomerSla();
            
        // $sla->title               = 'Sample Data for SLA ';
        // $sla->number              =234;
        // $sla->date                = date('Y-m-d');
        // $sla->reference           = 'reference'; 
        // $sla->category_id          = 1;
        // $sla->service_id          = 1;
        // $sla->customer_id         = 2;
        // $customer_details         = SmStaff::find(4);
        // $sla->customer_name       = $customer_details->full_name;  
        // $sla->private_note        = 'For test';
        // $sla->public_note         = 'For Public note test';
        // $sla->terms_note          = 'For terms note';
        // $sla->footer_note         = 'For footer note';
        // $sla->signature_person    ='customer sign' ;
        // $sla->signature_company   = 'company signature'; 
        // $sla->discount_amount    =  0;
        // $sla->discount_type      = 'P';
        // $sla->amount             = 1500; 
        // $sla->created_by    =     1;
        // $sla->save();
        
        
        DB::statement("INSERT INTO customer_slas (id,sla_type,title,customer_type,number,date,days,reference,customer_id,customer_name,discount_id,tax_amount,total_amount,paybale_amount,paid_amount,payment_status,partial_paymemt,note,description,private_note,public_note,terms_note,footer_note,signature_person,signature_company,is_approved,active_status,created_by,updated_by,created_at, updated_at) VALUES
(1, NULL, NULL, 'old', '11586325716', '2020-04-09', 20, '4234234', '3', NULL, 1, NULL, 0.00, 1275.50, 1275.50, 'P', NULL, 'wqdasdasdas', 'sdasdasdasd', 'sdasdasd', 'dasdasd', 'sdadasd', 'sadasd', NULL, 'sffsd', 0, 1, '1', '1', '2020-04-08 06:05:22', '2020-04-08 12:42:54')");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_slas');
    }
}
