<?php

use App\SmStaff;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_no')->nullable();
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('full_name', 200)->nullable();
            $table->string('fathers_name', 100)->nullable();
            $table->string('mothers_name', 100)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->date('date_of_joining')->nullable();
            $table->string('nid_number', 50)->nullable();
            $table->string('birth_number', 50)->nullable();
            $table->string('passport_number', 50)->nullable();
            $table->string('emirate_id', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('mobile', 50)->nullable();
            $table->string('emergency_mobile', 50)->nullable();
            $table->string('marital_status', 30)->nullable();
            $table->string('merital_status', 30)->nullable();
            $table->string('staff_photo')->nullable();
            $table->string('current_address', 500)->nullable();
            $table->string('permanent_address', 500)->nullable();
            $table->string('qualification', 200)->nullable();
            $table->string('experience', 200)->nullable();
            $table->string('job_type', 20)->nullable();
            $table->string('basic_salary', 200)->nullable();
            $table->string('contract_type', 200)->nullable();
            $table->string('location', 50)->nullable();
            $table->string('casual_leave', 15)->nullable();
            $table->string('medical_leave', 15)->nullable();
            $table->string('metarnity_leave', 15)->nullable();
            $table->string('bank_account_name', 50)->nullable();
            $table->string('bank_account_no', 50)->nullable();
            $table->string('bank_name', 20)->nullable();
            $table->string('bank_brach', 30)->nullable();
            $table->string('facebook_url', 100)->nullable();
            $table->string('twiteer_url', 100)->nullable();
            $table->string('linkedin_url', 100)->nullable();
            $table->string('instragram_url', 100)->nullable();
            $table->string('joining_letter', 500)->nullable();
            $table->string('resume', 500)->nullable();
            $table->string('other_document', 500)->nullable();
            $table->string('job_description', 500)->nullable();
            $table->string('notes', 500)->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->string('driving_license', 255)->nullable();
            $table->date('driving_license_ex_date')->nullable();
            $table->date('contract_start_date')->nullable();
            $table->date('contract_end_date')->nullable();
            $table->timestamps();
            $table->integer('designation_id')->nullable()->unsigned();
            $table->integer('department_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('role_id')->nullable()->unsigned();
            $table->integer('gender_id')->nullable()->unsigned();
            $table->integer('created_by')->nullable()->default(1)->unsigned();
            $table->integer('updated_by')->nullable()->default(1)->unsigned();
        });


        $faker = Faker\Factory::create();
        $users  = User::all();
        foreach($users as $user){
            $s = new SmStaff();
            $s->user_id = $user->id;
            $s->role_id = $user->role_id;
            $s->staff_no = $user->id;
            $s->designation_id = $user->id;
            $s->department_id = $user->id;
            $s->first_name = $user->full_name;
            $s->full_name = $user->full_name;
            $s->last_name = $user->id;
            $s->email = $user->email;
            $s->nid_number = time();
            $s->gender_id = 1;
            $s->mobile = '+88017'.$user->id.time();
            $s->staff_photo = 'public/uploads/staff/'.$user->id.'.png';
            $s->save();
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_staffs');
    }
}
