<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmHumanDepartmentsTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('sm_human_departments', function (Blueprint $table) {
         $table->increments('id');
         $table->string('name')->nullable();
         $table->tinyInteger('active_status')->default(1);
         $table->timestamps();
         $table->integer('created_by')->nullable()->default(1)->unsigned();
         $table->integer('updated_by')->nullable()->default(1)->unsigned();
      });

      DB::table('sm_human_departments')->insert([
         
         [
            'name' => 'Admin',
            'created_at' => date('Y-m-d h:i:s')
         ],
         [
            'name' => 'Finance',
            'created_at' => date('Y-m-d h:i:s')
         ],
         [
            'name' => 'Health',
            'created_at' => date('Y-m-d h:i:s')
         ],
         [
            'name' => 'Technology',
            'created_at' => date('Y-m-d h:i:s')
         ],
         [
            'name' => 'Music and Theater',
            'created_at' => date('Y-m-d h:i:s')
         ]
      ]);
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::dropIfExists('sm_human_departments');
   }
}
