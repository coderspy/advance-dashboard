<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_title',100)->nullable();
            $table->string('event_location',200)->nullable();
            $table->string('event_des',500)->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->string('uplad_image_file',200)->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();



            $table->integer('created_by')->nullable()->default(1)->unsigned();
            $table->integer('updated_by')->nullable()->default(1)->unsigned();
        });
        DB::table('sm_events')->insert([
            [
                'event_title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',    //      1
                'event_location' => 'Dhaka, Bangladesh',
                'event_des' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'from_date' => '2019-06-12',
                'to_date' => '2019-06-21',
                'uplad_image_file' => 'public/uploads/events/event1.jpg',
            ],
            [
                'event_title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',    //      1
                'event_location' => 'Dhaka, Bangladesh',
                'event_des' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'from_date' => '2019-06-12',
                'to_date' => '2019-06-21',
                'uplad_image_file' => 'public/uploads/events/event2.jpg',
            ],
            [
                'event_title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',    //      1
                'event_location' => 'Dhaka, Bangladesh',
                'event_des' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'from_date' => '2019-06-12',
                'to_date' => '2019-06-21',
                'uplad_image_file' => 'public/uploads/events/event3.jpg',
            ],
            [
                'event_title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',    //      1
                'event_location' => 'Dhaka, Bangladesh',
                'event_des' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'from_date' => '2019-06-12',
                'to_date' => '2019-06-21',
                'uplad_image_file' => 'public/uploads/events/event4.jpg',
            ],
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_events');
    }
}
