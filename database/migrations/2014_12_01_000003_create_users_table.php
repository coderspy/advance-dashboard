<?php

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 250)->nullable();
            $table->string('username', 250)->nullable();
            $table->string('email', 250)->nullable();
            $table->string('password', 100)->nullable();
            $table->string('usertype', 210)->nullable();
            $table->tinyInteger('access_status')->default(1)->comment('0 = off, 1 = on');
            $table->tinyInteger('active_status')->default(1);
            $table->text('random_code')->nullable();
            $table->text('notificationToken')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->integer('created_by')->nullable()->default(1);
            $table->integer('updated_by')->nullable()->default(1);
            $table->integer('role_id')->nullable()->unsigned(); 
        }); 
        
        $user               = new User();
        $user->access_status=  1;
        $user->created_by   =  1;
        $user->updated_by   =  1; 
        $user->role_id      =  1;
        $user->full_name    =  'Super Admin';
        $user->email        =  'admin@demo.com';
        $user->username     =  'admin';
        $user->password     =  Hash::make('123456');
        $user->created_at   =  date('Y-m-d h:i:s');
        $user->save(); 
 
        $user               = new User();
        $user->access_status=  1;
        $user->created_by   =  1;
        $user->updated_by   =  1; 
        $user->role_id      =  3;
        $user->full_name    =  'Pro User';
        $user->email        =  'prouser@demo.com';
        $user->username     =  'prouser';
        $user->password     =  Hash::make('123456');
        $user->created_at   =  date('Y-m-d h:i:s');
        $user->save(); 
 
        $user               = new User();
        $user->access_status=  1;
        $user->created_by   =  1;
        $user->updated_by   =  1; 
        $user->role_id      =  4;
        $user->full_name    =  'Customer 01';
        $user->email        =  'customer1@demo.com';
        $user->username     =  'customer1';
        $user->password     =  Hash::make('123456');
        $user->created_at   =  date('Y-m-d h:i:s');
        $user->save(); 
 
        $user               = new User();
        $user->access_status=  1;
        $user->created_by   =  1;
        $user->updated_by   =  1; 
        $user->role_id      =  4;
        $user->full_name    =  'Customer 02';
        $user->email        =  'customer2@demo.com';
        $user->username     =  'customer2';
        $user->password     =  Hash::make('123456');
        $user->created_at   =  date('Y-m-d h:i:s');
        $user->save(); 
        
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
