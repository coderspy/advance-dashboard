<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Discount;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->enum('type', ['P', 'F'])->comment('P=Percentage, F=Fixed Amount');
            $table->double('amount', 16, 2)->default(0);
            $table->string('note', 255)->nullable();
            $table->string('discount_start_date')->nullable();
            $table->string('discount_end_date')->nullable();
            $table->timestamps();
        });

        $s = new Discount();
        $s->title = 'Pohela Boishak';
        $s->type = "P";
        $s->amount = 5;
        $s->discount_start_date = '2020-01-31';
        $s->discount_end_date = '2020-06-30';
        $s->note = "This is a test discount for percentage";
        $s->save();


        $s = new Discount();
        $s->title = "Valentaine Day";
        $s->type = "F";
        $s->amount = 500;
        $s->discount_start_date = '2020-01-31';
        $s->discount_end_date = '2020-06-30';
        $s->note = "This is a test discount for Fixed";
        $s->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
