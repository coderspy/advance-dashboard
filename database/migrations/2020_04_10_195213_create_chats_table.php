<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('message');
            $table->integer('sender_id')->nullable()->unsigned()->index('chats_o1');
            $table->foreign('sender_id','chats_o1')->references('id')->on('users')->onDelete('cascade');

            $table->integer('receiver_id')->nullable()->unsigned()->index('chats_02');
            $table->foreign('receiver_id','chats_02')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
