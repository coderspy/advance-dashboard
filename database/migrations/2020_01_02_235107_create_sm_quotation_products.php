<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SmQuotationProducts;

class CreateSmQuotationProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_quotation_services', function (Blueprint $table) {
            $table->increments('id');
            $table->double('govt_price', 16, 2)->nullable();
            $table->double('sale_unit_price', 16, 2)->nullable();

            $table->timestamps();

            $table->integer('quotation_id')->nullable()->unsigned();
            $table->foreign('quotation_id')->references('id')->on('sm_quotations')->onDelete('restrict');

            $table->integer('discount_id')->nullable()->unsigned();
            $table->foreign('discount_id')->references('id')->on('discounts')->onDelete('restrict');

            $table->integer('service_id')->nullable()->unsigned();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('restrict');
        });

 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_quotation_products');
    }
}
