<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SlaTemplate;
class CreateSlaTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sla_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });

        $s = new SlaTemplate();
        $s->message = "Dear [name],
            It is our pleasure to serve you, and thank you for choosing [system_name]. You selected a service of emigration works. This service duration is [durration] year. 
            To process above-mentioned work, you are willing submit the below documents and information, which is mandate to proceed your work. If you have any queries, feel free to ask me.
            We need 45 days to need to accomplish the assigned works. If we need additional time, we shall ask you over the phone, or it will be our pleasure to response your call or email at [email].";
        $s->active_status = 1;
        $s->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sla_templates');
    }
}
