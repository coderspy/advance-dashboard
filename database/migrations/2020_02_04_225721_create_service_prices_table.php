<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ServicePrice;

class CreateServicePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->double('price')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable(); 
            $table->timestamps();
        });


        $s= new ServicePrice();
        $s->title ="Govt. Price";
        $s->price = 1275.50;
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();

        $s= new ServicePrice();
        $s->title ="3rd Party Price";
        $s->price = 575.50;
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();


        $s= new ServicePrice();
        $s->title ="Service charge";
        $s->price = 1570;
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();


        $s= new ServicePrice();
        $s->title ="Extend Lisence";
        $s->price = 575.50;
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_prices');
    }
}
