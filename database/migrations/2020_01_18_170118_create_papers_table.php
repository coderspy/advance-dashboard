<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Papers;
class CreatePapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papers', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('title', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable(); 
            $table->timestamps();
        });


        $s = new Papers();
        $s->title = "VISA";
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();
        $s = new Papers();
        $s->title = "EID";
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();
        $s = new Papers();
        $s->title = "PASSPORT";
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();
        $s = new Papers();
        $s->title = "NID";
        $s->created_by =1;
        $s->updated_by =1;
        $s->save();



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papers');
    }
}
