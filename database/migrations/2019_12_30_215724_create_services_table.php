<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Service;
class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();  
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable(); 
            $table->timestamps();

            $table->integer('category_id')->nullable()->unsigned();
            $table->foreign('category_id')->references('id')->on('service_categories')->onDelete('restrict');
        });

        DB::table('services')->insert([
            [
                'name' => 'Boishakhi service',
                'created_by' => 1,
                'updated_by' => 1,
                'category_id' => 1,
            ]
        ]);

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
