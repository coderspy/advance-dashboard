<?php

use App\SmStaff;
use App\SmQuotation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmQuotations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_quotations', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('quotation_type',255)->nullable();
            $table->string('title',255)->nullable();
            $table->string('number',255)->nullable(); 
            $table->integer('days')->nullable(); 
            $table->date('date')->nullable();
            $table->string('reference',255)->nullable();


            $table->integer('customer_id')->unsigned();
            $table->string('customer_name',255)->nullable();
 

            $table->integer('discount_id')->nullable()->unsigned();
            $table->foreign('discount_id')->references('id')->on('discounts')->onDelete('restrict');

            // $table->double('amount', 15, 2);
            // $table->double('discount_amount', 15, 2)->nullable();
            // $table->enum('discount_type', ['P', 'A'])->nullable()->comment('P = percentage, A= amount'); 
            $table->double('tax_amount', 15, 2)->nullable(); 

            $table->double('total_amount', 15, 2)->nullable(); 
            $table->double('paybale_amount', 15, 2)->nullable(); 
            $table->double('paid_amount', 15, 2)->nullable(); 
 


            $table->enum('payment_status',  ['UP', 'P', 'PP', 'PR'] )->comment('UP= UNPAID , P= PAID , PP= PARTIALLY PAID, PR= PROFORMA');
            // $table->double('partial_paymemt', 15, 2)->nullable();


            $table->text('note')->nullable();
            $table->text('description')->nullable();

            $table->text('private_note')->nullable();
            $table->text('public_note')->nullable();
            $table->text('terms_note')->nullable();
            $table->text('footer_note')->nullable();
            $table->string('signature_person',255)->nullable();
            $table->string('signature_company',255)->nullable(); 
            $table->tinyInteger('is_approved')->default(0)->comment('0 = no, 1= yes');
            $table->tinyInteger('active_status')->default(1);
            $table->string('created_by')->nullable()->default(1);
            $table->string('updated_by')->nullable()->default(1);
            $table->timestamps();
        });






        $quotation = new SmQuotation();
            
        $quotation->title               = 'Sample Data for Quoatation ';
        $quotation->number              =234;
        $quotation->date                = date('Y-m-d');
        $quotation->reference           = 'reference'; 
        $quotation->customer_id         = 4;
        $customer_details               = SmStaff::find(4);
        $quotation->customer_name       = $customer_details->full_name; 
        //footer note 
        $quotation->private_note        = 'For test';
        $quotation->public_note         = 'For Public note test';
        $quotation->terms_note          = 'For terms note';
        $quotation->footer_note         = 'For footer note';
        $quotation->signature_person    ='customer sign' ;
        $quotation->signature_company   = 'company signature'; 
        $quotation->discount_id    =  null;
        $quotation->paid_amount        = 1500; 
        $quotation->created_by    =     1;
        $quotation->save(); 
 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_quotations');
    }
}
