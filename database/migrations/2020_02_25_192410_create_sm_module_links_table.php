<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmModuleLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_module_links', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('module_id')->nullable();
            $table->string('name')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
        DB::table('sm_module_links')->insert([
            [
                'name' => 'Dashboard', //ey gulo sub menu hobe ok??
                'module_id' => 1,
            ],
            [
                'name' => 'Customer',
                'module_id' => 1,
            ],
            [
                'name' => 'Create SLA',
                'module_id' => 2,
            ],
            [
                'name' => 'SLA List',
                'module_id' => 2,
            ],
             [
                'name' => 'New Quotations',
                'module_id' => 2,
            ],
             [
                'name' => 'Quotations List',
                'module_id' => 2,
            ],
          
            [
                'name' => 'Sms Notification',
                'module_id' => 3,
            ],
             [
                'name' => 'Daily Services Report',
                'module_id' => 4,
            ],
                 [
                'name' => 'Services Pending/Done',
                'module_id' => 4,
            ],
                 [
                'name' => 'Montly Services Report',
                'module_id' => 4,
            ],
                 [
                'name' => 'Communication',
                'module_id' => 5,
            ],
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_module_links');
    }
}
