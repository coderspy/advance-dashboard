<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_role_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('active_status')->default(1);  
            $table->integer('module_id')->nullable()->unsigned();
            $table->integer('module_link_id')->nullable()->unsigned();
            $table->integer('staff_id')->nullable()->unsigned();
            $table->integer('created_by')->nullable()->default(1)->unsigned();
            $table->integer('updated_by')->nullable()->default(1)->unsigned(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_role_permissions');
    }
}
