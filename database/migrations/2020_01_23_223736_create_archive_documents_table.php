<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ArchiveDocuments;
class CreateArchiveDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('file_title',255)->nullable();
            $table->string('file',255)->nullable();
            $table->integer('created_by')->nullable()->default(1);
            $table->integer('updated_by')->nullable()->default(1);
            $table->timestamps();

            
            $table->integer('paper_id')->nullable()->unsigned();
            $table->foreign('paper_id')->references('id')->on('papers')->onDelete('restrict');

            
            $table->integer('sla_id')->nullable()->unsigned();
            $table->foreign('sla_id')->references('id')->on('customer_slas')->onDelete('restrict');

            
            $table->integer('quotation_id')->nullable()->unsigned();
            $table->foreign('quotation_id')->references('id')->on('sm_quotations')->onDelete('restrict');

            
        });

        $s = new ArchiveDocuments();
        $s->customer_id =3;
        $s->paper_id =1; 
        $s->file_title ='NID';
        $s->file ='public/uploads/file/file.png';
        $s->save();

        $s = new ArchiveDocuments();
        $s->customer_id =3;
        $s->paper_id =2; 
        $s->file_title ='VISA File';
        $s->file ='public/uploads/file/file.pdf';
        $s->save();

        $s = new ArchiveDocuments();
        $s->customer_id =4;
        $s->paper_id =3; 
        $s->file_title ='VISA';
        $s->file ='public/uploads/file/file.pdf';
        $s->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_documents');
    }
}
