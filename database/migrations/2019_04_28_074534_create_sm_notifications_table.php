<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SmNotification;
class CreateSmNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->string('message')->nullable();
            $table->string('url')->nullable();
            $table->tinyInteger('is_read')->default(0);
            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();

            $table->integer('user_id')->default(1)->nullable()->unsigned();
            $table->integer('role_id')->default(1)->unsigned();
            $table->integer('created_by')->default(1)->unsigned();
            $table->integer('updated_by')->default(1)->unsigned();

        });

        $s= new SmNotification();
        $s->date = date('Y-m-d');
        $s->message= "Create a SLA";
        $s->url= url('services/papers-list');
        $s->is_read =0;
        $s->user_id= 1; //Auth::user()->id
        $s->role_id =3; //Auth::user()->role_id
        $s->save();



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_notifications');
    }
}
