<?php

use App\SmAcademicYear;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmGeneralSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_general_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('system_name')->nullable();
            $table->string('system_title')->nullable(); 
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('currency')->nullable()->default('USD');
            $table->string('currency_symbol')->nullable()->default('$');
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->integer('active_status')->nullable()->default(1);
            $table->string('currency_code')->nullable()->default('USD');
            $table->string('language_name')->nullable()->default('en');
            $table->string('system_domain')->nullable();
            $table->string('copyright_text')->nullable();
            $table->integer('api_url')->default(1);
            $table->integer('website_btn')->default(1);
            $table->integer('dashboard_btn')->default(1);
            $table->integer('report_btn')->default(1);
            $table->integer('ttl_rtl')->default(2);
            $table->timestamps();


            $table->integer('time_zone_id')->nullable();
            $table->integer('language_id')->nullable()->default(1)->unsigned();
            $table->integer('date_format_id')->nullable()->default(1)->unsigned();
        });
      

        DB::table('sm_general_settings')->insert([
            [
                'copyright_text' => 'Copyright &copy; 2019 All rights reserved By CoderSpy',
                'logo' => 'public/uploads/settings/logo.png',
                'favicon' => 'public/uploads/settings/favicon.png', 
                'email' => 'jmrashed@gmail.com',
                'currency' => 'USD',
                'currency_symbol' => '$',
                'system_name' => 'Advance Dashboard',
                'system_title' => 'Advance Dashboard',
                'time_zone_id' => 51,
                'address'=>'Dhaka-1215, Bangladesh'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_general_settings');
    }
}
