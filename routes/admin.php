
<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;





    /* ******************* Dashboard Setting ***************************** */
    Route::get('dashboard/display-setting', 'SmSystemSettingController@displaySetting');
    Route::post('dashboard/display-setting-update', 'SmSystemSettingController@displaySettingUpdate');


    /* ******************* Dashboard Setting ***************************** */
    Route::get('api/permission', 'SmSystemSettingController@apiPermission');
    Route::get('api-permission-update', 'SmSystemSettingController@apiPermissionUpdate');
    /* ******************* Dashboard Setting ***************************** */

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::view('/admin-setup', 'frontEnd.admin_setup');
    Route::view('/general-setting', 'frontEnd.general_setting');


    //Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('add-toDo', 'HomeController@addToDo');
    Route::post('saveToDoData', 'HomeController@saveToDoData');
    Route::get('view-toDo/{id}', 'HomeController@viewToDo')->where('id', '[0-9]+');
    Route::get('edit-toDo/{id}', 'HomeController@editToDo')->where('id', '[0-9]+');
    Route::post('update-to-do', 'HomeController@updateToDo');
    Route::get('remove-to-do', 'HomeController@removeToDo');
    Route::get('get-to-do-list', 'HomeController@getToDoList');


    Route::get('admin-dashboard', 'HomeController@index')->name('admin-dashboard');


    //Role Setup
    Route::get('role', ['as' => 'role', 'uses' => 'RoleController@index']);
    Route::post('role-store', ['as' => 'role_store', 'uses' => 'RoleController@store']);
    Route::get('role-edit/{id}', ['as' => 'role_edit', 'uses' => 'RoleController@edit'])->where('id', '[0-9]+');
    Route::post('role-update', ['as' => 'role_update', 'uses' => 'RoleController@update']);
    Route::post('role-delete', ['as' => 'role_delete', 'uses' => 'RoleController@delete']);


    // Role Permission
    Route::get('assign-permission/{id}', ['as' => 'assign_permission', 'uses' => 'SmRolePermissionController@assignPermission']);
    Route::post('role-permission-store', ['as' => 'role_permission_store', 'uses' => 'SmRolePermissionController@rolePermissionStore']);


    // Module Permission

    Route::get('module-permission', 'RoleController@modulePermission');


    Route::get('assign-module-permission/{id}', 'RoleController@assignModulePermission');
    Route::post('module-permission-store', 'RoleController@assignModulePermissionStore');

    Route::get('pro-user-access', 'RoleController@pro_user_access');

  

    //User Route
    Route::get('user', ['as' => 'user', 'uses' => 'UserController@index']);
    Route::get('user-create', ['as' => 'user_create', 'uses' => 'UserController@create']);

    // Base group
    Route::get('base-group', ['as' => 'base_group', 'uses' => 'SmBaseGroupController@index']);
    Route::post('base-group-store', ['as' => 'base_group_store', 'uses' => 'SmBaseGroupController@store']);
    Route::get('base-group-edit/{id}', ['as' => 'base_group_edit', 'uses' => 'SmBaseGroupController@edit']);
    Route::post('base-group-update', ['as' => 'base_group_update', 'uses' => 'SmBaseGroupController@update']);
    Route::get('base-group-delete/{id}', ['as' => 'base_group_delete', 'uses' => 'SmBaseGroupController@delete']);

    // Base setup
    Route::get('base-setup', ['as' => 'base_setup', 'uses' => 'SmBaseSetupController@index']);
    Route::post('base-setup-store', ['as' => 'base_setup_store', 'uses' => 'SmBaseSetupController@store']);
    Route::get('base-setup-edit/{id}', ['as' => 'base_setup_edit', 'uses' => 'SmBaseSetupController@edit']);
    Route::post('base-setup-update', ['as' => 'base_setup_update', 'uses' => 'SmBaseSetupController@update']);
    Route::post('base-setup-delete', ['as' => 'base_setup_delete', 'uses' => 'SmBaseSetupController@delete']);



    


    //user log
    Route::get('user-log', ['as' => 'user_log', 'uses' => 'UserController@userLog']);
    Route::get('user-log-pdf','UserController@user_log_pdf');


    // income head routes
    Route::get('income-head', ['as' => 'income_head', 'uses' => 'SmIncomeHeadController@index']);
    Route::post('income-head-store', ['as' => 'income_head_store', 'uses' => 'SmIncomeHeadController@store']);
    Route::get('income-head-edit/{id}', ['as' => 'income_head_edit', 'uses' => 'SmIncomeHeadController@edit']);
    Route::post('income-head-update', ['as' => 'income_head_update', 'uses' => 'SmIncomeHeadController@update']);
    Route::get('income-head-delete/{id}', ['as' => 'income_head_delete', 'uses' => 'SmIncomeHeadController@delete']);

    // Search account
    Route::get('search-account', ['as' => 'search_account', 'uses' => 'SmAccountsController@searchAccount']);
    Route::post('search-account', ['as' => 'search_account', 'uses' => 'SmAccountsController@searchAccountReportByDate']);

    //--------------------------------------------------------------------



    Route::get('download-resume/{file_name}', function ($file_name = null) {
        $file = public_path() . '/uploads/resume/' . $file_name;
        if (file_exists($file)) {
            return Response::download($file);
        }
    });

    Route::get('download-other-document/{file_name}', function ($file_name = null) {
        $file = public_path() . '/uploads/others_documents/' . $file_name;
        if (file_exists($file)) {
            return Response::download($file);
        }
    });

    Route::get('download-staff-timeline-doc/{file_name}', function ($file_name = null) {
        $file = public_path() . '/uploads/staff/timeline/' . $file_name;
        if (file_exists($file)) {
            return Response::download($file);
        }
    });

    Route::get('delete-staff-document-view/{id}', 'SmStaffController@deleteStaffDocumentView');
    Route::get('delete-staff-document/{id}', 'SmStaffController@deleteStaffDocument');

    // staff timeline
    Route::get('add-staff-timeline/{id}', 'SmStaffController@addStaffTimeline');
    Route::post('staff_timeline_store', 'SmStaffController@storeStaffTimeline');
    Route::get('delete-staff-timeline-view/{id}', 'SmStaffController@deleteStaffTimelineView');
    Route::get('delete-staff-timeline/{id}', 'SmStaffController@deleteStaffTimeline');


    //Staff Attendance
    Route::get('staff-attendance', ['as' => 'staff_attendance', 'uses' => 'SmStaffAttendanceController@staffAttendance']);
    Route::post('staff-attendance-search', 'SmStaffAttendanceController@staffAttendanceSearch');
    Route::post('staff-attendance-store', 'SmStaffAttendanceController@staffAttendanceStore');

    Route::get('staff-attendance-report', ['as' => 'staff_attendance_report', 'uses' => 'SmStaffAttendanceController@staffAttendanceReport']);
    Route::post('staff-attendance-report-search', ['as' => 'staff_attendance_report_search', 'uses' => 'SmStaffAttendanceController@staffAttendanceReportSearch']);

    Route::get('staff-attendance/print/{role_id}/{month}/{year}/', 'SmStaffAttendanceController@staffAttendancePrint');

    Route::get('view-payslip/{id}', 'SmPayrollController@viewPayslip');

    //payroll Report
    Route::get('payroll-report', 'SmPayrollController@payrollReport');
    Route::post('search-payroll-report', ['as' => 'searchPayrollReport', 'uses' => 'SmPayrollController@searchPayrollReport']);
    Route::get('search-payroll-report', 'SmPayrollController@searchPayrollReport');

    // pro report
      Route::get('pro/daily-services-report', 'SmPayrollController@daily_services_report');
      Route::get('pro/montly-service', 'SmPayrollController@montly_service');
     

    //Homework
    Route::get('homework-list', ['as' => 'homework-list', 'uses' => 'SmHomeworkController@homeworkList']);

    Route::post('homework-list', ['as' => 'homework-list', 'uses' => 'SmHomeworkController@searchHomework']);
    Route::get('homework-edit/{id}', ['as' => 'homework_edit', 'uses' => 'SmHomeworkController@homeworkEdit']);
    Route::post('homework-update', ['as' => 'homework_update', 'uses' => 'SmHomeworkController@homeworkUpdate']);
    Route::get('homework-delete/{id}', ['as' => 'homework_delete', 'uses' => 'SmHomeworkController@homeworkDelete']);

    Route::get('add-homeworks', ['as' => 'add-homeworks', 'uses' => 'SmHomeworkController@addHomework']);
    Route::post('save-homework-data', ['as' => 'saveHomeworkData', 'uses' => 'SmHomeworkController@saveHomeworkData']);

    //Route::get('evaluation-homework/{class_id}/{section_id}', 'SmHomeworkController@evaluationHomework');

    Route::get('evaluation-homework/{class_id}/{section_id}/{homework_id}', 'SmHomeworkController@evaluationHomework');
    Route::post('save-homework-evaluation-data', ['as' => 'save-homework-evaluation-data', 'uses' => 'SmHomeworkController@saveHomeworkEvaluationData']);
    Route::get('evaluation-report', 'SmHomeworkController@EvaluationReport');

    Route::get('evaluation-document/{file_name}', function ($file_name = null) {

        $file = public_path() . '/uploads/homework/' . $file_name;
        if (file_exists($file)) {
            return Response::download($file);
        }
    });

 

    Route::get('assignment-list', 'SmTeacherController@assignmentList');
    Route::get('study-metarial-list', 'SmTeacherController@studyMetarialList');
    Route::get('syllabus-list', 'SmTeacherController@syllabusList');
    Route::get('other-download-list', 'SmTeacherController@otherDownloadList');

    // Communicate
    Route::get('notice-list', 'SmCommunicateController@noticeList');
    Route::get('add-notice', 'SmCommunicateController@sendMessage');
    Route::post('save-notice-data', 'SmCommunicateController@saveNoticeData');
    Route::get('edit-notice/{id}', 'SmCommunicateController@editNotice');
    Route::post('update-notice-data', 'SmCommunicateController@updateNoticeData');
    Route::get('delete-notice-view/{id}', 'SmCommunicateController@deleteNoticeView');
    Route::get('send-email-sms-view', 'SmCommunicateController@sendEmailSmsView');
    Route::post('send-email-sms', 'SmCommunicateController@sendEmailSms');
    Route::get('email-sms-log', 'SmCommunicateController@emailSmsLog');
    Route::get('delete-notice/{id}', 'SmCommunicateController@deleteNotice');

    Route::get('studStaffByRole', 'SmCommunicateController@studStaffByRole');





    //Language Setting
    Route::get('language-setup/{id}', 'SmSystemSettingController@languageSetup');
    Route::get('language-settings', 'SmSystemSettingController@languageSettings');
    Route::post('language-add', 'SmSystemSettingController@languageAdd');

    Route::get('language-edit/{id}', 'SmSystemSettingController@languageEdit');
    Route::post('language-update', 'SmSystemSettingController@languageUpdate');

    Route::post('language-delete', 'SmSystemSettingController@languageDelete');

    Route::get('get-translation-terms', 'SmSystemSettingController@getTranslationTerms');
    Route::post('translation-term-update', 'SmSystemSettingController@translationTermUpdate');


    //Backup Setting
    Route::post('backup-store', 'SmSystemSettingController@BackupStore');
    Route::get('backup-settings', 'SmSystemSettingController@backupSettings');
    Route::post('backup-verify', 'SmSystemSettingController@VerifyBackupPost')->name('VerifyBackup_post');
    Route::get('backup-verify/{id}', 'SmSystemSettingController@VerifyBackup')->name('VerifyBackup');
    Route::get('get-backup-files/{id}', 'SmSystemSettingController@getfilesBackup');
    Route::get('get-backup-db/{id}', 'SmSystemSettingController@getDatabaseBackup');
    Route::get('download-database/{id}', 'SmSystemSettingController@downloadDatabase');
    Route::get('download-files/{id}', 'SmSystemSettingController@downloadFiles');
    Route::get('restore-database/{id}', 'SmSystemSettingController@restoreDatabase');
    Route::get('delete-database/{id}', 'SmSystemSettingController@deleteDatabase')->name('delete_database');

    //Update System
    Route::get('update-system', 'SmSystemSettingController@UpdateSystem');
    Route::post('admin/update-system', 'SmSystemSettingController@admin_UpdateSystem');
    Route::any('upgrade-settings', 'SmSystemSettingController@UpgradeSettings');


    //Settings
    Route::get('general-settings', 'SmSystemSettingController@generalSettingsView');
    Route::get('update-general-settings', 'SmSystemSettingController@updateGeneralSettings');
    Route::post('update-general-settings-data', 'SmSystemSettingController@updateGeneralSettingsData');
    Route::post('update-school-logo', 'SmSystemSettingController@updateSchoolLogo');


    //Email Settings
    Route::get('email-settings', 'SmSystemSettingController@emailSettings');
    Route::post('update-email-settings-data', 'SmSystemSettingController@updateEmailSettingsData');

    // payment Method Settings
    Route::get('payment-method-settings', 'SmSystemSettingController@paymentMethodSettings');
    Route::post('update-paypal-data', 'SmSystemSettingController@updatePaypalData');
    Route::post('update-stripe-data', 'SmSystemSettingController@updateStripeData');
    Route::post('update-payumoney-data', 'SmSystemSettingController@updatePayumoneyData');
    Route::post('active-payment-gateway', 'SmSystemSettingController@activePaymentGateway');

    //Email Settings
    Route::get('email-settings', 'SmSystemSettingController@emailSettings');
    Route::post('update-email-settings-data', 'SmSystemSettingController@updateEmailSettingsData');

    // payment Method Settings
    Route::get('payment-method-settings', 'SmSystemSettingController@paymentMethodSettings');
    Route::post('update-payment-gateway', 'SmSystemSettingController@updatePaymentGateway');
    Route::post('is-active-payment', 'SmSystemSettingController@isActivePayment');
  
    // background setting
    Route::get('background-setting', 'SmBackgroundController@index');
    Route::post('background-settings-update', 'SmBackgroundController@backgroundSettingsUpdate');
    Route::post('background-settings-store', 'SmBackgroundController@backgroundSettingsStore');
    Route::get('background-setting-delete/{id}', 'SmBackgroundController@backgroundSettingsDelete');
    Route::get('background_setting-status/{id}', 'SmBackgroundController@backgroundSettingsStatus');

    // login access control
    Route::get('login-access-control', 'SmLoginAccessControlController@loginAccessControl');
    Route::post('login-access-control', 'SmLoginAccessControlController@searchUser');
    Route::get('login-access-permission', 'SmLoginAccessControlController@loginAccessPermission');


//////////////////////////////////////////Services/////////////////////////////////////////////////////////



    Route::get('services-category', 'ServiceController@categoryView')->name('categoryView');
    Route::get('services/category-edit/{id}', 'ServiceController@categoryEdit');
    Route::get('services/category-delete/{id}', 'ServiceController@CategoryDelete');
    Route::post('services/category-store', 'ServiceController@CategoryStore');
    Route::post('services/category-update', 'ServiceController@CategoryUpdate');
    
    Route::get('services/prices-list', 'ServiceController@PriceList');
    Route::post('services/Price-store', 'ServiceController@PriceListStore');
    Route::get('services/price-list-edit/{id}', 'ServiceController@PriceListEdit');
    Route::post('services/price-update', 'ServiceController@PriceListUpdate');
    Route::get('services/price-delete/{id}', 'ServiceController@price_delete');

    Route::get('service-list', 'ServiceController@ServiceView');
    Route::get('service-list-edit/{id}', 'ServiceController@ServiceEdit');
    Route::get('service-list-delete/{id}', 'ServiceController@ServiceDelete');
    Route::post('service-list-store', 'ServiceController@ServiceStore');
    Route::get('service-list-notification/{id}', 'ServiceController@ServiceStoreNotification')->name('ServiceStoreNotification');
    Route::post('service-list-update', 'ServiceController@ServiceUpdate');

    // Route::get('service-list', 'ServiceController@ServiceView');
    // Route::get('service-list-edit/{id}', 'ServiceController@ServiceEdit');
    // Route::get('service-list-delete/{id}', 'ServiceController@ServiceDelete');
    // Route::post('service-list-store', 'ServiceController@ServiceStore');
    // Route::post('service-list-update', 'ServiceController@ServiceUpdate');

  //////////////////////////////////////Discount///////////////////////////////////////////////////////  
    Route::get('discount', 'DiscountController@index');
    Route::get('assign-discount', 'DiscountController@assign_discount');
    Route::get('edit-discount/{id}', 'DiscountController@Edit_Discount');
    Route::get('discount-delete/{id}', 'DiscountController@discount_delete');
    Route::post('store-discount', 'DiscountController@store_discount');
    Route::post('discount-update', 'DiscountController@discount_update');
    Route::get('assign-discount', 'DiscountAssignController@assign_discount');
    Route::get('assign-discount-list/{id}', 'DiscountAssignController@assign_discount_list')->name('assign_discount_list');
    Route::post('store-assign-discount', 'DiscountAssignController@store_assign_discount');
    Route::get('edit-assign-discount/{id}', 'DiscountAssignController@edit_assign_discount');
    Route::post('assign-discount-update', 'DiscountAssignController@assign_discount_update');
    Route::get('assign-discount-delete/{id}', 'DiscountAssignController@assign_discount_delete');

   
///////////////////////report////////////////////////////////////////////////////////////////////////////

    Route::get('reports/pro-user-list', 'ServiceController@pro_user_list');
    Route::get('reports/user-list', 'ServiceController@user_list');
    Route::get('reports/services-list', 'ServiceController@services_list')->name('services_list');
    Route::get('reports/services-pending-done', 'ServiceController@services_pending_done');
    Route::post('search-services-pending-done', 'ServiceController@search_services_pending_done');
    Route::post('search-user-log', 'UserController@search_user_log');
    Route::get('services-list-pdf', 'ServiceController@services_list_pdf');
    Route::get('services-pending-done-pdf', 'ServiceController@services_pending_done_pdf');
    Route::get('cutomer-services-list-pdf', 'SmStaffController@cutomer_services_list_pdf');
    Route::get('reports/daily-services-report', 'SmStaffController@daily_services_report');
    Route::get('daily-services-report-pdf', 'SmStaffController@daily_services_report_pdf');
    Route::get('reports/customer-monthly-services', 'SmStaffController@customer_archive_list');

    // create 
    Route::resource('services', 'SmHumanDepartmentController');
    Route::get('services-charges', 'SmHumanDepartmentController@services_charges');
    Route::get('user-archive-document', 'SmHumanDepartmentController@user_archive_document');
    Route::get('user-discount-approve', 'SmHumanDepartmentController@user_discount_approve');

    /////////////////////Customer///////////////////////////
    
   
    Route::get('customer/archive/{id}', 'SmStaffController@customer_archive');
    Route::get('edit-Archive/{id}', 'SmStaffController@edit_Archive');
    Route::get('view-Archive/{id}', 'SmStaffController@view_Archive');


    
    Route::post('customer-archive-store', 'SmStaffController@customer_archive_store');
    Route::post('archive-update', 'SmStaffController@Archive_update');
    Route::get('archive-delete/{id}',  'SmStaffController@Archive_delete');

    Route::get('customer-directory', 'SmStaffController@customer_directory');
    Route::get('add-customer', 'SmStaffController@add_customer');
    Route::get('view-customer/{id}', 'SmStaffController@viewCustomer');
    Route::get('edit-customer/{id}', 'SmStaffController@edit_customer');
    Route::get('delete-Customer/{id}', 'SmStaffController@delete_Customer_View');
    Route::get('deleteCustomer/{id}', 'SmStaffController@deleteCustomer');   
    Route::post('customer-update', 'SmStaffController@customer_Update');
    Route::post('customer-store', 'SmStaffController@customerStore'); 
    Route::post('search-customer', 'SmStaffController@searchCustomer');   
    Route::post('search-customer-services', 'SmStaffController@search_customer_services');   

    Route::get('create-sla-template',               'CustomerSlaController@createSlaTemplate');
    Route::post('store-sla-template',               'CustomerSlaController@storeSlaTemplate');
    Route::post('sla-template-update',               'CustomerSlaController@SlaTemplateUpdate');
    Route::get('create-sla',               'CustomerSlaController@createSla');
    // Route::get('create-sla',               'CustomerSlaController@index');
    Route::get('sla-list',                  'CustomerSlaController@sla_list');
    Route::get('sla-payment/{id}',    'CustomerSlaController@slaPayment');
    Route::post('sla-payment',    'CustomerSlaController@slaPaymentReceive');
    Route::get('sla-list-view/{id}',    'CustomerSlaController@show');
    Route::get('sla-list-edit/{id}',    'CustomerSlaController@slaEdit');
    Route::get('sla-template-edit/{id}',    'CustomerSlaController@SlaTemplateEdit');
    Route::get('sla-list-delete/{id}',  'CustomerSlaController@slaDelete');
    Route::get('sla-template-delete/{id}',  'CustomerSlaController@SlaTemplateDelete');
    Route::get('sla/approve/{id}', 'CustomerSlaController@changeStatus');
    
    Route::post('sla-store',       'CustomerSlaController@storeSla');
    Route::get('created-sla-list/{id}',       'CustomerSlaController@created_sla_list')->name('created_sla_list');
    Route::post('sla-update',      'CustomerSlaController@slaUpdate');

    // new 
    Route::get('sla-edit/{id}',       'CustomerSlaController@edit');
